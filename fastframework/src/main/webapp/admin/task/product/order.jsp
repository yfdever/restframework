<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class='form-group' data-action="${APP.admin.base }/product/task">
	<input type="hidden" name="id" class="form-control" value = '${product.id }'/>
	<input type="hidden" name="processId" class="form-control" value = '${processId }'/>
	<div class="form-group">
		<label>*商品名称 </label>
		<input type="text" name="productName" class="form-control" value='${product.productName }'/>
	</div>
	<div class="form-group">
		<label>*商品数量 </label>
		<input type="text" name="productSize" class="form-control" value='${product.productSize }'/>
	</div>
	<div class="form-group">
		<label>*审批人 </label>
		<select name="actorId" class="form-control">
			<option>请选择审批人</option>
			<c:forEach var="user" items="${users}">
				<option value='${user.username }' <c:if test="${user.username eq product.actorId}">selected</c:if>>${user.username }</option>
			</c:forEach>
		</select>
	</div>
</div>

<script type="text/javascript">
	var isread = ('${isread}' != '')?true:false;
	if(isread){
		$('input,select').attr("disabled",isread);
	}
</script>