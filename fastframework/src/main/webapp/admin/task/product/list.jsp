<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="/fast" prefix="fast" %> 
<%@include file="../../basic/header.jsp"%>
        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">商品列表</h1>
			</div>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body table-grid" id="usergrid">
						<div class="toolbar">
							<div class="btn-group" role="group" aria-label="...">
								<button type="button" data-toggle="modal" data-target="#product-dialog" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> 新建</button>
							</div>
						</div>
						<table data-toggle="table" class="datagrid"
						data-url="${APP.admin.base }/product/list"  
						data-show-refresh="true" data-search="true" 
						data-select-item-name="id[]" data-pagination="true" 
						data-side-pagination="server"
						data-click-to-select="true"
						data-page-list="[10]"
						data-toolbar="#productgrid > .toolbar"
						data-sort-name="createTime" data-sort-order="desc">
						    <thead>
						    <tr>
						    	<th data-checkbox="true" >ID</th>
						        <th data-field="id" data-visible="false">ID</th>
						        <th data-field="productName" >商品名称</th>
						        <th data-field="productSize" >商品数量</th>
						        <th data-field="status" data-formatter="formatter.appover">审核状态</th>
						        <th data-field="creator" >创建者</th>
						        <th data-field="createTime" data-formatter="formatter.datetime">创建时间</th>
						        <th data-formatter="operate">操作</th>
						    </tr>
						    </thead>
						</table>
					</div>
				</div>
			</div>
		</div><!--/.row-->	
	</div><!--/.main-->
	
<!-- 商品信息对话框 START-->
<div class="modal" id="product-dialog" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
     <form action="#" method="post" role="form" class="modal-form edit-form" callback="cb1">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="dialog-title"><span class="glyphicon glyphicon-edit"></span> 商品信息</h4>
      </div>
      <div class="modal-body"></div>
      <div class='modal-footer'>
      	<button type="submit" class="btn btn-primary">确认</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- 商品信息对话框 END-->

<!-- 任务对话框 START-->
<div class="modal" id="agency-dialog" tabindex="-1" role="dialog" aria-hidden="true" >
  <div class="modal-dialog">
    <div class="modal-content">
    	<form action="#" method="post" role="form" class="modal-form agency-form" callback="cb2">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="dialog-title"><span class="glyphicon glyphicon-edit"></span> 查看审批</h4>
      </div>
      <div class="modal-body">
        <ul id="myTab" class="nav nav-tabs"></ul>
		<div id="myTabContent" class="tab-content"></div>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- 任务对话框 END-->

<fast:script>
	<script type="text/javascript">

		$('#agency-dialog').on('show.bs.modal', function(event){
			var button = $(event.relatedTarget);
			var index = button.attr("data-index");
			var params = '';
			if(index != undefined){
				params = "-" + data[index]['id'];
			}
			$.ajax({ 
				url:  "${APP.admin.base}/product/all/" + data[index]['orderId'], 
				type:"POST",
				dataType:'json',
				success: function(result){
					if(result != null && result.length > 0){
						$('#myTab > li').remove();
						$('#myTabContent > .tab-pane').remove();
						for(var i=0; i<result.length; i++){
							var task = result[i];
							$('#myTab').append("<li><a href='#" + task.taskName + "' data-toggle='tab'>" + task.displayName + "</a></li>");
							$('#myTabContent').append("<div class='tab-pane fade' id='" + task.taskName + "'></div>");
							$('#myTab > li:first').addClass('active');
							$('#myTabContent > .tab-pane:first').addClass('in active');
						}
						
						var ajax =  function(i){
							var task = result[i];
							var params = "";
							if(task.variableMap != null)
								params = task.variableMap.id;
							$.ajax({ 
								url:  task.actionUrl + "-" + params + "-" + task.taskId,
								type:"POST",
								success: function(html){
									$($('#myTabContent > .tab-pane').get(i)).append(html);
									$editForm = $("form.edit-form");
									$editForm.attr('action', $('div',$($('#myTabContent > .tab-pane').get(i))).attr('data-action'));
								}
							});
						}
						
						ajax(0);
						
						$('#myTab > li').each(function(i){
							$(this).click(function(){
								if($($('#myTabContent > .tab-pane').get(i)).html()=='')
									ajax(i);
							});
						});
						
					}
				}
			});
		});
	
		var cb2 = function(rst) {
			$agencyForm = $("form.agency-form");
			$agencyForm.resetForm();
			$("#agency-dialog").modal('hide');
		};
		
		$('#product-dialog').on('show.bs.modal', function(event) {
			if($('#product-dialog .modal-body').html() != "")
				$('#product-dialog .modal-body').html("");
			$.ajax({ 
				url:  "${APP.admin.base}/product/form/order", 
				type:"POST",
				success: function(result){
					if(result != null){
						$('#product-dialog .modal-body').append(result);
						$editForm = $("form.edit-form");
						$editForm.attr('action', $('#product-dialog .modal-body > div').attr('data-action'));
					}
				}
			});
		});
		
		var $datagrid = $("#productgrid").datagrid();
	
		var cb1 = function(rst) {
			$editForm = $("form.edit-form");
			$editForm.resetForm();
			$("#product-dialog").modal('hide');
			$datagrid.bootstrapTable('refresh');
		};
		var data = {};
		//输出操作的html代码
		var operate = function(value,row,index){
			data[index] = row;
			var html = '<button data-toggle="modal" data-index="'+index+'" data-target="#agency-dialog" class="btn btn-success btn-sm"> 查看审批</button>';
			
			//var params = ((row.taskVariableMap.isLogistics)?'isLogistics':'') + '&' + ((row.taskVariableMap.isQuality)?'isQuality':'');
			//return '<a href="${APP.admin.base }/task/start/'+row.orderId+'-'+row.taskId+'?'+ params +'" class="btn btn-success btn-sm"> 我抢单</a>';
			return html;
		}
	</script>
</fast:script>
<%@include file="../../basic/footer.jsp"%>
