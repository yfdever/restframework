<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class='form-group' data-action="${APP.admin.base }/product/task">
	<div class="form-group">
		<input type="hidden" name="id" class="form-control" value = '${id }'/>
		<input type="hidden" name='taskId' value='${taskId }' />
		<label>商品审核 </label>
		<input type="radio" name='status' <c:if test="${product.status==1}">checked</c:if> value='1' /> 同意
		<input type="radio" name='status' <c:if test="${product.status==-1}">checked</c:if> value='-1' /> 不同意
	</div>
</div>
<script type="text/javascript">
	var isread = ('${isread}' != '')?true:false;
	if(isread){
		$('input,select').attr("disabled",isread);
	}
</script>