<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="/fast" prefix="fast" %> 
<%@include file="basic/header.jsp"%>
        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">消息提醒 
					<small><button id="toggleButton" class="btn btn-sm btn-primary btn-ajax" callback="nodify_togglePlugin" ajax-url="${APP.admin.base}/togglePlugin/EMAIL">Active</button></small>
				</h1>
			</div>
		</div><!--/.row-->
				
		
		<div class="row">
			<div class="col-lg-12">
				<div class="alert bg-primary" role="alert">
					<span class="glyphicon glyphicon-exclamation-sign"></span> 设置系统邮箱发送的参数
				</div>
			</div>
		</div><!--/.row-->	
		<div class="row">
			<div class="col-lg-8">
				<div class="panel panel-default">
					<div class="panel-body">
						<form role="form" class="ajax-form" method="post" action="${APP.admin.base}/notify/confEmail">
							<div class="form-group">
								<label>SMTP 服务器</label>
								<input name="host" class="form-control" value="${emailConfig.host }" placeholder="如：smtp.163.com">
							</div>
															
							<div class="form-group">
								<label>邮箱地址</label>
								<input name="user" type="text" class="form-control" value="${emailConfig.user }" placeholder="如：server@163.com">
							</div>
							
							<div class="form-group">
								<label>发送名称</label>
								<input name="name" type="text" class="form-control" value="${emailConfig.name }" placeholder="如：江阴煤炭交易市场">
							</div>
							
							<div class="form-group">
								<label>邮箱密码</label>
								<input name="password" type="text" class="form-control" value="${emailConfig.password }" placeholder="登录邮箱时使用的密码">
							</div>
							<div class="form-group checkbox">
								<label> <input type="checkbox"> 使用SSL 建议不用开启
								</label>
							</div>
							<div class="form-group">
								<label>邮箱发送模板</label>
								<textarea name="template" class="form-control" rows="10" >${emailConfig.template }</textarea>
							</div>
							<button type="submit" class="btn btn-primary">保存设置</button>
							<button type="reset" class="btn btn-default">重置</button>
						</form>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="panel panel-warning">
					<div class="panel-heading">
						提示
					</div>
					<div class="panel-body">
						<p>尽量使用126、163、qq等邮箱设置，Gmail和yahoo等邮箱可能会导致发送邮件被拦截的情况。</p>
					</div>
				</div>
			</div>
		</div>	
		
	</div><!--/.main-->

<fast:script>
	//此处放置需要导入或者执行的script
	<script type="text/javascript">
	$("#notifyBanner").addClass("active");
	var nodify_togglePlugin = function(rsp){
		$("#toggleButton").toggleClass("btn-primary");
	};
	</script>
</fast:script>    
<%@include file="basic/footer.jsp"%>
