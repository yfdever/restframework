<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="/fast" prefix="fast" %> 
<%@include file="basic/header.jsp"%>
        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">系统日志</h1>
			</div>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body table-grid" id="loggrid">
					<div class="toolbar">
							<div class="btn-group" role="group" aria-label="...">
							  <button type="button" data-toggle="modal" data-target="#clear-dialog" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> 删除</button>
							</div>
						</div>
						<table data-toggle="table" class="datagrid"
						data-url="${APP.admin.base }/log/ajaxList"
						data-show-refresh="true" data-search="true" 
						data-pagination="true" 
						data-side-pagination="server"
						data-page-list="[10]"
						data-toolbar="#loggrid > .toolbar"
						data-sort-name="createtime" data-sort-order="desc">
						    <thead>
						    <tr>
						    	<th data-field="id" data-visible="false">ID</th>
						        <th data-field="ip">IP</th>
						        <th data-field="action">Behavior</th>
						        <th data-field="login_name">用户</th>
						        <th data-field="createtime" data-formatter="formatter.datetime">时间</th>
						    </tr>
						    </thead>
						</table>
					</div>
				</div>
			</div>
		</div><!--/.row-->		
		
		
	</div><!--/.main-->
	
<!--  -->
<div class="modal" id="clear-dialog" tabindex="-1" role="dialog" aria-hidden="true" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="dialog-title"><span class="glyphicon glyphicon-trash"></span> </h4>
      </div>
			<div class="modal-body">
			<form action="${APP.admin.base }/log/clear" method="post" role="form" class="modal-form edit-form" callback="clearCB">
				Clear All?
				<hr/>
				<button type="submit" class="btn btn-primary">YES</button>
			</form>
			</div>
		</div>
  </div>
</div>
<fast:script>
	<script type="text/javascript">
	$("#logBanner").addClass("active");
	var $datagrid = $("#loggrid").datagrid();
	function clearCB(){
		$("#clear-dialog").modal('hide');
		$datagrid.bootstrapTable('refresh');
	}
	</script>
</fast:script>
    
<%@include file="basic/footer.jsp"%>
