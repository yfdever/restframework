<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="/fast" prefix="fast" %> 
<%@include file="basic/header.jsp"%>
        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">内容管理</h1>
			</div>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="alert bg-danger" role="alert">
							<span class="glyphicon glyphicon-exclamation-sign"></span> 该功能尚未开放！
						</div>
					</div>
				</div>
			</div>
		</div>	
		
	</div><!--/.main-->

<fast:script>
	//此处放置需要导入或者执行的script
	<script type="text/javascript">
	
	$("#documentBanner").addClass("active");
	</script>
</fast:script>    
<%@include file="basic/footer.jsp"%>
