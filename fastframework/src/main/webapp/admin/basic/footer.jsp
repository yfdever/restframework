<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <script src="${APP.jquery }/jquery.js"></script>
    <script src="${APP.jquery }/jquery-ui.min.js" type="text/javascript"></script>
    <script src="${APP.jquery }/plugins/jquery.form.js"></script>
    <script src="${APP.jquery }/plugins/validate/jquery.validate.min.js"></script>
    <script src="${APP.jquery }/plugins/validate/messages_zh.min.js"></script>
	<script src="${APP.bootstrap }/bootstrap.min.js"></script>
	<script src="${APP.bootstrap }/plugins/bootstrap-table.js"></script>
	<script src="${APP.bootstrap }/plugins/bootstrap-table-zh-CN.js"></script>
	<!-- [if lt IE 10]>
	<script src="${APP.leancloud }/swfobject.js"></script>
	<script src="${APP.leancloud }/web_socket.js"></script>
	<script type="text/javascript">
		WEB_SOCKET_SWF_LOCATION = '${APP.leancloud }/WebSocketMain.swf';
	</script>
	<![endif]-->
	<script src="${APP.leancloud }/Map.js"></script>
<%-- 	<script src="${APP.leancloud }/AV.push.js"></script>
	<script src="${APP.leancloud }/AV.realtime.js"></script>
	<script src="${APP.leancloud }/IM.js"></script> --%>
	<script src="${APP.admin.js }/app.js"></script>
	<script>
		!function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){		  
				$(this).find('em:first').toggleClass("glyphicon-minus");	  
			}); 
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
		
		//需要验证的表单加入表单验证的功能
		$(".validate-form").each(function(){
			$this = $(this);
			$error = $this.find(".error-msg");
			$this.validate({
				errorLabelContainer: $error
			});
		});
		
	</script>
	${script }
	<% pageContext.removeAttribute("script"); %>
</body>

</html>
