<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<ul class="nav menu">
			<li id="dashboardBanner"><a href="${APP.admin.base }/dashboard"><span class="glyphicon glyphicon-dashboard"></span> 仪表板</a></li>
			<li class="parent ">
				<a>
					<span class="glyphicon glyphicon-user"></span> 用户管理 <span id="user-collapse" data-toggle="collapse" data-toggle="collapse" href="#sub-item-user" class="icon pull-right"><em class="glyphicon glyphicon-s glyphicon-plus"></em></span> 
				</a>
				<ul class="children collapse" id="sub-item-user">
					<li>
						<a id="userlistBanner" class="" href="${APP.admin.base }/user">
							<span class="glyphicon glyphicon-share-alt"></span> 用户列表
						</a>
					</li>
					<li>
						<a id="userroleBanner" class="" href="${APP.admin.base }/user/role">
							<span class="glyphicon glyphicon-share-alt"></span> 用户角色
						</a>
					</li>
					<li>
						<a id="permissionBanner" class="" href="${APP.admin.base }/permission">
							<span class="glyphicon glyphicon-share-alt"></span> 用户权限
						</a>
					</li>
				</ul>
			</li>
			<%-- <li id="documentBanner"><a href="${APP.admin.base }/document"><span class="glyphicon glyphicon-file"></span> 内容管理</a></li> --%>
			<li id="attachmentBanner"><a href="${APP.admin.base }/attachment"><span class="glyphicon glyphicon-paperclip"></span> 附件管理</a></li>
			<li role="presentation" class="divider"></li>
			<li id="logBanner"><a href="${APP.admin.base }/log"><span class="glyphicon glyphicon-info-sign"></span> 系统日志</a></li>
			<li id="apiBanner"><a href="${APP.admin.base }/api"><span class="glyphicon glyphicon-link"></span> API设置</a></li>
			<li class="parent">
				<a>
					<span class="glyphicon glyphicon-cog"></span> 系统设置 <span id="system-collapse" data-toggle="collapse" href="#sub-item-setting" class="icon pull-right"><em class="glyphicon glyphicon-s glyphicon-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-setting">
					<li>
						<a id="behaviorBanner" class="" href="${APP.admin.base }/system/behavior">
							<span class="glyphicon glyphicon-share-alt"></span> 用户行为
						</a>
					</li>
					<li>
						<a id="pluginBanner" class="" href="${APP.admin.base }/plugin">
							<span class="glyphicon glyphicon-share-alt"></span> 插件设置
						</a>
					</li>
					<%--
					<li>
						<a class="" href="#">
							<span class="glyphicon glyphicon-share-alt"></span> 菜单设置
						</a>
					</li>
					<li>
						<a class="" href="#">
							<span class="glyphicon glyphicon-share-alt"></span> 用户设置
						</a>
					</li>
					 --%>
					<%-- <li>
						<a id="processBanner" href="${APP.admin.base }/process/list">
							<span class="glyphicon glyphicon-share-alt"></span> 流程定义
						</a>
					</li> --%>
				</ul>
			</li>
			<li role="presentation" class="divider"></li>
			<%-- <li id="modelBanner"><a href="${APP.admin.base }/product/page"><span class="glyphicon glyphicon-th-list"></span> 我的商品</a></li>
			<li id="modelBanner"><a href="${APP.admin.base }/judge/page"><span class="glyphicon glyphicon-th-list"></span> 我的代办</a></li>
			<li id="modelBanner"><a href="${APP.admin.base }/model"><span class="glyphicon glyphicon-th-list"></span> 模型配置</a></li> --%>
			<li id="settingBanner"><a href="${APP.admin.base }/setting"><span class="glyphicon glyphicon-wrench"></span> 配置设置</a></li>
			<%-- <li id="notifyBanner"><a href="${APP.admin.base }/notify"><span class="glyphicon glyphicon-bell"></span> 消息提醒</a></li>--%>
			<li class="parent">
				<a>
					<span class="glyphicon glyphicon-transfer"></span> 数据管理 <span id="data-collapse" data-toggle="collapse" href="#sub-item-data" class="icon pull-right"><em class="glyphicon glyphicon-s glyphicon-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-data">
					<li>
						<a id="backupBanner"  href="${APP.admin.base }/data/backup">
							<span class="glyphicon glyphicon-share-alt"></span> 备份
						</a>
					</li>
					<li>
						<a id="storeBanner" href="${APP.admin.base }/data/restore">
							<span class="glyphicon glyphicon-share-alt"></span> 还原
						</a>
					</li>
				</ul>
			</li>
		</ul>
	</div><!--/.sidebar-->