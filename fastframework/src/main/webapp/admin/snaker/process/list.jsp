<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="/fast" prefix="fast" %> 
<%@include file="../../basic/header.jsp"%>
        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">工作流列表</h1>
			</div>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body table-grid" id="usergrid">
						<div class="toolbar">
							<div class="btn-group" role="group" aria-label="...">
							  <a href="${APP.admin.base }/process/save" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> 新建</a>
							  <button target-url="${APP.admin.base }/snaker/process/del" type="button" class="btn btn-warning remove-bar"><span class="glyphicon glyphicon-remove"></span> 删除</button>
							</div>
						</div>
						<table data-toggle="table" class="datagrid"
						data-url="${APP.admin.base }/process/ajax"  
						data-show-refresh="true" data-search="true" 
						data-select-item-name="id[]" data-pagination="true" 
						data-side-pagination="server"
						data-click-to-select="true"
						data-page-list="[10]"
						data-toolbar="#processgrid > .toolbar"
						data-sort-name="create_Time" data-sort-order="desc">
						    <thead>
						    <tr>
						    	<th data-checkbox="true" >ID</th>
						        <th data-field="id" data-visible="false">ID</th>
						        <th data-field="name" >流程名称</th>
						        <th data-field="displayName">显示名称</th>
						        <th data-field="state" data-formatter="formatter.state">是否可用</th>
						        <th data-field="createTime" data-formatter="formatter.datetime">创建时间</th>
						        <th data-field="creator">创建者</th>
						        <th data-formatter="operate">操作</th>
						    </tr>
						    </thead>
						</table>
					</div>
				</div>
			</div>
		</div><!--/.row-->	
	</div><!--/.main-->
	
<!-- 编辑数据的表单对话框 END -->
<fast:script>
	<script type="text/javascript">	
		var $datagrid = $("#processgrid").datagrid();
	
		var data = {};
		//输出操作的html代码
		var operate = function(value,row,index){
			data[index] = row;
			return '<a href="${APP.admin.base }/process/save/'+row.id+'" class="btn btn-success btn-sm">编辑</a>';
		}
		
		$("#system-collapse").click();
		$("#processBanner").addClass("active");
	</script>
</fast:script>
<%@include file="../../basic/footer.jsp"%>
