<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="/fast" prefix="fast" %> 
<%@include file="../../basic/header.jsp"%>
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">工作流编辑</h1>
			</div>
			<div class="col-lg-12">
				<div class="panel panel-default">
					<iframe src="${APP.admin.base }/process/designer/${processId}" frameborder=0 scrolling=no style='width:100%; height:700px;'></iframe>
				</div>
			</div>
		</div><!--/.row-->
	</div><!--/.main-->
<%@include file="../../basic/footer.jsp"%>
