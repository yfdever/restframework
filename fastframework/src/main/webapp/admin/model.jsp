<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="/fast" prefix="fast" %> 
<%@include file="basic/header.jsp"%>
        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">系统模型管理</h1>
			</div>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<table data-toggle="table" 
						data-search="true" 
						data-pagination="true" 
						data-page-list="[10]"
						>
						    <thead>
						    <tr>
						    	<th data-checkbox="true" >ID</th>
						        <th>名称</th>
						        <th>标识</th>
						        <th>操作</th>
						    </tr>
						    </thead>
						    <tbody>
						    	<tr>
						    		<td></td>
						    		<td>设置</td>
						    		<td>Setting</td>
						    		<td><a class="btn" href="">修改</a><a href="">删除</a></td>
						    	</tr>
						    	<tr>
						    		<td></td>
						    		<td>设置</td>
						    		<td>Setting</td>
						    		<td><a href="">修改</a></td>
						    	</tr>
						    </tbody>
						</table>
					</div>
				</div>
			</div>
		</div><!--/.row-->		
		
		
	</div><!--/.main-->

<fast:script>
	<script type="text/javascript">
	$("#modelBanner").addClass("active");
	</script>
</fast:script>
    
<%@include file="basic/footer.jsp"%>
