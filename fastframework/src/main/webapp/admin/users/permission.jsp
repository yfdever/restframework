<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="/fast" prefix="fast" %> 
<%@include file="../basic/header.jsp"%>
        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">用户权限</h1>
			</div>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body table-grid" id="permissiongrid">
						<div class="toolbar">
							<div class="btn-group" role="group" aria-label="...">
							  <button type="button" data-toggle="modal" data-target="#form-dialog" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> 新建</button>
							  <button target-url="${APP.admin.base }/permission/del" type="button" class="btn btn-warning remove-bar"><span class="glyphicon glyphicon-remove"></span> 删除</button>
							</div>
						</div>
						<table data-toggle="table" class="datagrid"
						data-url="${APP.admin.base }/permission/ajaxList"  
						data-show-refresh="true" data-search="true" 
						data-select-item-name="id[]" data-pagination="true" 
						data-side-pagination="server"
						data-page-list="[10]"
						data-toolbar="#permissiongrid > .toolbar"
						data-sort-name="name" data-sort-order="desc">
						    <thead>
						    <tr>
						    	<th data-checkbox="true" >ID</th>
						        <th data-field="id" data-visible="false">ID</th>
						        <th data-field="name" >权限</th>
						        <th data-field="title">权限标题</th>
						        <th data-field="scope">权限分组</th>
						        <th data-field="createtime" data-formatter="formatter.datetime">创建时间</th>
						    </tr>
						    </thead>
						</table>
					</div>
				</div>
			</div>
		</div><!--/.row-->		
		
		
	</div><!--/.main-->

<!-- 新增数据的表单对话框 -->
<div class="modal" id="form-dialog" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="dialog-title"><span class="glyphicon glyphicon-edit"></span> 新建权限</h4>
      </div>
      <div class="modal-body">
        <form action="${APP.admin.base }/permission/add" method="post" role="form" class="modal-form" callback="cb1">
			<div class="form-group">
				<label>* 权限名称</label>
				<input name="name" class="form-control" placeholder="" />
			</div>
			<div class="form-group">
				<label>* 权限标题</label>
				<input name="title" class="form-control" placeholder="" />
			</div>
			<div class="form-group">
				<label>* 权限分组</label>
				<input name="scope" class="form-control" placeholder="" />
			</div>
			<hr/>
			<button type="submit" class="btn btn-primary">保存设置</button>
			<button type="reset" class="btn btn-default">重置</button>
		</form>
      </div>
    </div>
  </div>
</div>
<!-- 新增数据的表单对话框 END -->

<!-- 编辑数据的表单对话框 -->
<div class="modal" id="edit-form-dialog" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="dialog-title"><span class="glyphicon glyphicon-edit"></span> 修改信息</h4>
      </div>
      <div class="modal-body">
        <form action="${APP.admin.base }/permission/edit" method="post" role="form" class="modal-form edit-form" callback="cb2">
        	<input name="id" type="hidden" id="rowid" />
			<div class="form-group">
				<label>* 权限名称</label>
				<input name="name" class="form-control" placeholder="" />
			</div>
			<div class="form-group">
				<label>* 权限标题</label>
				<input name="title" class="form-control" placeholder="" />
			</div>
			<div class="form-group">
				<label>* 权限分组</label>
				<input name="scope" class="form-control" placeholder="" />
			</div>
			<hr/>
			<button type="submit" class="btn btn-primary">保存修改</button>
		</form>
      </div>
    </div>
  </div>
</div>
<!-- 编辑数据的表单对话框 END -->

<fast:script>
	<script type="text/javascript">
	
		var $datagrid = $("#permissiongrid").datagrid();

		var data = {};

		var cb1 = function(rst) {
			$("#form-dialog").modal('hide');
			$datagrid.bootstrapTable('refresh');
		};

		var cb2 = function(rst) {
			$editForm = $("form.edit-form");
			$editForm.resetForm();
			$("#edit-form-dialog").modal('hide');
			$datagrid.bootstrapTable('refresh');
		};
		$("#user-collapse").click();
		$("#permissionBanner").addClass("active");
	</script>
</fast:script>
    
<%@include file="../basic/footer.jsp"%>
