<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="/fast" prefix="fast" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@include file="../basic/header.jsp"%>
    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">	
        <form role="form" class="ajax-form" method="post" action="${APP.admin.base}/permission/save" callback="cb1">
		<input type="hidden" name="roleid" value="${roleid }"/>	
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">用户角色 / <small>权限设置</small></h1>
			</div>
		</div><!--/.row-->
		<div class="row">
		
			<div class="col-md-6">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<label><input type="checkbox" scope="USER" class="checkAll" target="#user-security-panel"/>  基本用户权限</label>
					</div>
					<div class="panel-body" id="user-security-panel">
						<div class="row">
						<c:forEach var="p" items="${ permissionList.USER}" varStatus="vs">
							
							<div class="col-sm-3">
								<label><input type="checkbox" name="permissionid" value="${p.id}" /> <span>${p.title }</span></label>
							</div>
							<c:if test="${((vs.index+1)%4 == 0)}">
							</div>
							<hr/>
							<div class="row">
							</c:if>
						</c:forEach>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<label><input type="checkbox" scope="SYS" class="checkAll" target="#sys-security-panel"/>  后台用户权限</label>
					</div>
					<div class="panel-body" id="sys-security-panel">
						<div class="row">
						<c:forEach var="p" items="${ permissionList.SYS}" varStatus="vs">
							
							<div class="col-sm-3">
								<label><input type="checkbox" name="permissionid" value="${p.id}" /> <span>${p.title }</span></label>
							</div>
							<c:if test="${((vs.index+1)%4 == 0)}">
							</div>
							<hr/>
							<div class="row">
							</c:if>
						</c:forEach>
						</div>
					</div>
				</div>
			</div>
			
			<!-- <div class="col-md-6">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<label><input type="checkbox" />  后台用户权限</label>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-sm-3">
								<label><input type="checkbox" name="auth[]" value="1" /> <span>登录</span></label>
							</div>
							<div class="col-sm-3">
								<label><input type="checkbox" name="auth[]" value="1" /> <span>登录</span></label>
							</div>
							<div class="col-sm-3">
								<label><input type="checkbox" name="auth[]" value="1" /> <span>登录</span></label>
							</div>
							<div class="col-sm-3">
								<label><input type="checkbox" name="auth[]" value="1" /> <span>登录</span></label>
							</div>
						</div>
						<hr/>
						<div class="row">
							<div class="col-sm-3">
								<label><input type="checkbox" name="auth[]" value="1" /> <span>登录</span></label>
							</div>
							<div class="col-sm-3">
								<label><input type="checkbox" name="auth[]" value="1" /> <span>登录</span></label>
							</div>
							<div class="col-sm-3">
								<label><input type="checkbox" name="auth[]" value="1" /> <span>登录</span></label>
							</div>
							<div class="col-sm-3">
								<label><input type="checkbox" name="auth[]" value="1" /> <span>登录</span></label>
							</div>
						</div>
						<hr/>
					</div>
				</div>
			</div> -->
			
		</div><!-- /.row -->

	<hr/>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<button type="submit" class="btn btn-success">保存</button>
		</div>
	</div>
	</form>
</div><!--/.main-->

<fast:script>
	<script type="text/javascript">
	$(function(){
		$.ajax({ 
			url:  "${APP.admin.base}/permission/ajax/${roleid }", 
			type:"GET",
			dataType:'json',
			success: function(result){
				var permissionids = result.permissionid.split(',');
				for ( var permissionid in permissionids) {
					$("input[value='" + permissionids[permissionid] + "']").click();
				}
			}
		});
	});
		
	var cb1 = function(rsp){
		alert('修改成功!');
	};
	$("#user-collapse").click();
	$("#userroleBanner").addClass("active");
	</script>
</fast:script>    
<%@include file="../basic/footer.jsp"%>
