<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="/fast" prefix="fast" %> 
<%@include file="../basic/header.jsp"%>
        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">用户列表</h1>
			</div>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body table-grid" id="usergrid">
						<div class="toolbar">
							<div class="btn-group" role="group" aria-label="...">
							  <button type="button" data-toggle="modal" data-target="#form-dialog" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> 新建</button>
							  <button target-url="${APP.admin.base }/user/del" type="button" class="btn btn-warning remove-bar"><span class="glyphicon glyphicon-remove"></span> 删除</button>
							</div>
						</div>
						<table data-toggle="table" class="datagrid"
						data-url="${APP.admin.base }/user/ajaxList"  
						data-show-refresh="true" data-search="true" 
						data-select-item-name="id[]" data-pagination="true" 
						data-side-pagination="server"
						data-click-to-select="false"
						data-page-list="[10]"
						data-toolbar="#usergrid > .toolbar"
						data-sort-name="login_name" data-sort-order="desc">
						    <thead>
						    <tr>
						    	<th data-checkbox="true" >ID</th>
						        <th data-field="id" data-visible="false">ID</th>
						        <th data-field="login_name" >登录名</th>
						        <th data-field="role"  data-sortable="true">角色</th>
						        <th data-field="is_admin" data-formatter="formatter.yon">管理员</th>
						        <th data-field="createtime" data-sortable="true" data-formatter="formatter.datetime">创建时间</th>
						        <th data-formatter="operate">操作</th>
						        
						    </tr>
						    </thead>
						</table>
					</div>
				</div>
			</div>
		</div><!--/.row-->		
		
		
	</div><!--/.main-->

<!-- 新增数据的表单对话框 -->
<div class="modal" id="form-dialog" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="dialog-title"><span class="glyphicon glyphicon-edit"></span> 新建信息</h4>
      </div>
      <div class="modal-body">
        <form action="${APP.admin.base }/user/save" method="post" role="form" id="add-user-form" class="modal-form validate-form" callback="cb1">
        	<div class="alert alert-danger error-msg" role="alert">请按照提示输入完整的信息内容!<br/></div>
			<div class="form-group">
				<label>* 登录名 一般使用唯一的英文标示，需要保证唯一性</label>
				<input name="login_name" class="form-control" placeholder="" title="登录名不得为空" required/>
			</div>
			
			<div class="form-group">
				<label> 昵称</label>
				<input name="display_name" class="form-control" placeholder="" />
			</div>
			
			<div class="form-group">
				<label>* 邮箱，需要保证唯一性</label>
				<input type="email" name="email" class="form-control" />
			</div>
				
			<div class="form-group">
				<label>* 密码</label>
				<input name="login_pass" type="password" id="login_pass" class="form-control" placeholder="大于6位" title="密码不得小于6位" required minlength="6">
			</div>
			
			<div class="form-group">
				<label>* 确认密码</label>
				<input type="password" class="form-control" equalTo="#login_pass" placeholder="" title="两次密码不相等" required minlength="6">
			</div>	
			
			<div class="form-group">
				<label>* 密码加密方式</label>
				<select name="pass_encode" class="form-control">
					<option value="MD5">MD5</option>
					<option value="SHA-1">SHA-1</option>
					<option value="SHA-256">SHA-256</option>
					<option value="SHA-384">SHA-384</option>
					<option value="SHA-512">SHA-512</option>
				</select>
			</div>
			
			<div class="form-group">
				<label>* 设置为管理员 </label>
				<input name="is_admin" type="hidden" value="0" class="cbxYON" />
				
			</div>
			<div class="form-group">
				<label>* 选择角色 </label>
				<select name='roleid' class="select_role form-control">
					<option value='0'>请选择角色</option>
				</select>
			</div>
			<hr/>
			<button type="submit" class="btn btn-primary">保存设置</button>
			<button type="reset" class="btn btn-default">重置</button>
		</form>
      </div>
    </div>
  </div>
</div>
<!-- 新增数据的表单对话框 END -->

<!-- 编辑数据的表单对话框 -->
<!-- <div class="modal" id="edit-form-dialog" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="dialog-title"><span class="glyphicon glyphicon-edit"></span> 修改信息</h4>
      </div>
      <div class="modal-body">
        <form action="${APP.admin.base }/user/edit" method="post" role="form" class="modal-form edit-form" callback="cb2">
        	<input name="id" type="hidden" id="rowid" />
			<div class="form-group">
				<label>* 登录名 一般使用唯一的英文标示，需要保证唯一性</label>
				<input name="login_name" class="form-control" placeholder="" readonly="readonly" />
			</div>
			
			<div class="form-group">
				<label> 昵称</label>
				<input name="display_name" class="form-control" placeholder="" />
			</div>
			
			<div class="form-group">
				<label>* 邮箱，需要保证唯一性</label>
				<input type="email" name="email" class="form-control" />
			</div>
			<div class="form-group">
				<label>* 设置为管理员 </label>
				<input name="is_admin" type="hidden" value="1" class="cbxYON" />
			</div>
			<div class="form-group">
				<label>* 选择角色 </label>
				<select name='roleid' class="select_role form-control">
					<option>请选择角色</option>
				</select>
			</div>
			<hr/>
			<button type="submit" class="btn btn-primary">保存修改</button>
		</form>
      </div>
    </div>
  </div>
</div> -->
<!-- 编辑数据的表单对话框 END -->

<!-- 编辑数据的表单对话框 -->
<div class="modal" id="acl-dialog" tabindex="-1" role="dialog" aria-hidden="true" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="dialog-title"><span class="glyphicon glyphicon-edit"></span> 权限信息</h4>
      </div>
      <div class="modal-body">
        <form action="${APP.admin.base }/user/save" method="post" role="form" class="modal-form edit-form" callback="cb3">
        	<input name="id" type="hidden" id="rowid" />
			<div class="form-group">
				<label>* 登录名 一般使用唯一的英文标示，需要保证唯一性</label>
				<input name="login_name" class="form-control" placeholder="" readonly="readonly" />
			</div>
			
			<div class="form-group">
				<label> 昵称</label>
				<input name="display_name" class="form-control" placeholder="" />
			</div>
			
			<div class="form-group">
				<label>* 邮箱，需要保证唯一性</label>
				<input type="email" name="email" class="form-control" />
			</div>
			<div class="form-group">
				<label>* 设置为管理员 </label>
				<input name="is_admin" type="hidden" value="1" class="cbxYON" />
			</div>
			<div class="form-group">
				<label>* 选择角色 </label>
				<select name='roleid' class="select_role form-control">
					<option value='0'>请选择角色</option>
				</select>
			</div>
			<hr/>
			<button type="submit" class="btn btn-primary">保存修改</button>
		</form>
      </div>
    </div>
  </div>
</div>
<!-- 编辑数据的表单对话框 END -->
<fast:script>
	<script type="text/javascript">
	
		$(function(){
			$.ajax({ 
				url:  "${APP.admin.base}/user/role/ajaxList", 
				type:"GET",
				dataType:'json',
				success: function(result){
					for (var i = 0; i < result.rows.length; i++) {
						$(".select_role").append("<option value='"+result.rows[i].id+"'>" + result.rows[i].title +"</option>");
					}
				}
			});
		});
	
		var $datagrid = $("#usergrid").datagrid();

		var data = {};
		//输出操作的html代码
		var operate = function(value,row,index){
			data[index] = row;
			return '<button data-toggle="modal" data-index="'+index+'" data-target="#acl-dialog" class="btn btn-success btn-sm">设置</button>';
		}
		
		$('#acl-dialog').on('show.bs.modal', function(event) {
			var button = $(event.relatedTarget);
			var index = button.attr("data-index");
			var modal = $(this);
			for ( var f in data[index]) {
				var  o = $("input[name='" + f + "']", this);
				if(o != undefined)
					$(o).val(data[index][f]);
				if(f == 'is_admin')
					$('.cbx_YON',this).prop('checked',(data[index][f] == 1) ? true : false);
				else if(f == 'roleid'){
					var val = (data[index][f] != null)?data[index][f]:'0';
					$('.select_role',this).find("option[value='" + val + "']").attr("selected",true);
				}
			}
		});

		var cb1 = function(rst) {
			$("#form-dialog").modal('hide');
			$datagrid.bootstrapTable('refresh');
		};

		var cb2 = function(rst) {
			$editForm = $("form.edit-form");
			$editForm.resetForm();
			$("#edit-form-dialog").modal('hide');
			$datagrid.bootstrapTable('refresh');
		};
		
		var cb3 = function(rst) {
			$editForm = $("form.edit-form");
			$editForm.resetForm();
			$("#acl-dialog").modal('hide');
			$datagrid.bootstrapTable('refresh');
		};
		$("#user-collapse").click();
		$("#userlistBanner").addClass("active");
	</script>
</fast:script>

<%@include file="../basic/footer.jsp"%>
