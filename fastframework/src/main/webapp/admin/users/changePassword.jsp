<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="/fast" prefix="fast" %> 
<!DOCTYPE html>
<html>
		<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>欢迎使用FastFramework</title>
		<link rel="shortcut icon" href="${APP.admin.base}/favicon.ico" />
		<link href="${APP.bootstrap}/bootstrap.min.css" rel="stylesheet">
		<link href="${APP.admin.css}/styles.css" rel="stylesheet">
		<link href="${APP.admin.css}/theme.css" rel="stylesheet">
		<!--[if lt IE 9]>
		<script src="js/html5shiv.js"></script>
		<script src="js/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
	
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			<div class="change-password-panel panel panel-default">
				<div class="panel-heading"><span class="glyphicon glyphicon-lock"></span> 修改密码</div>
				<div class="panel-body">
					<form role="form" id="form" class="ajax-form" method="post" action="${APP.admin.base}/user/changePassword" callback="redirect">
						<fieldset>
							<div class="form-group">
								<input class="form-control" placeholder="旧口令" name="oldpassword" type="password" autofocus="" value="">
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="新口令" id="txt-old-pass" name="" type="password" value="">
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="确认" id="txt-cfm-pass" name="newpassword" type="password" value="">
							</div>
							<button type="submit" class="btn btn-primary">修  改</button>
						</fieldset>
					</form>
				</div>
			</div>
		</div><!-- /.col-->
	</div><!-- /.row -->	
	
	<!-- jQuery -->
    <script src="${APP.jquery }/jquery.js"></script>
	<script src="${APP.jquery }/plugins/jquery.form.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="${APP.bootstrap }/bootstrap.min.js"></script>
    
    <script src="${APP.admin.js }/app.js"></script>
    
    <script>
    $("#form").submit(function(){
    	var oldPass = $("#txt-old-pass").val();
    	var cfmPass = $("#txt-cfm-pass").val();
    	if(oldPass!=cfmPass){
    		alert("两次输入的密码不一致!");
    		return false;
    	}
    	if(oldPass.length<6){
    		alert("密码长度不能小于6位!");
    		return false;
    	}
    	return true;
    });
    function redirect(rsp){
    	if(rsp.msg){
    		alert(rsp.msg);
    		history.go(-1);
    	}
    }
    </script>
</body>

</html>
