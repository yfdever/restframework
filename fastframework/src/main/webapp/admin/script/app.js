/**
 * 
 */
(function($){
	$(document).delegate(".modal-form","submit",function(){
		var cb = $(this).attr("callback");
		var $form = $(this);
		var $submitBtn = $(this).find("button[type='submit']");
		$submitBtn.attr('disabled','disabled');
		$(this).ajaxSubmit(function(rsp){
			$submitBtn.removeAttr("disabled");
			if(rsp.code===0){
				//将表单信息重置
				$form[0].reset();
				if(cb){
					if(window[cb])
						window[cb](rsp);
				}
				return;
			}
			alert(rsp.msg||"操作失败!");
			
		});
		return false;
	});
	
	//全选的函数
	$(document).delegate(".checkAll","click",function(){
		var _f = $(this).prop('checked');
		var _t = $(this).attr('target');
		if(_t){
			$(_t+' input[type="checkbox"]').each(function(){
				$(this).prop('checked',_f);
			});
		}
	});	
	$(document).delegate(".panel-body input[type='checkbox']","click",function(){
		var _f = $(this).prop('checked');
		var _t = $($(this).parents('.panel-body'),this).attr('id');
		if(!_f){
			$("input[target='#" + _t + "']").prop('checked',_f);
			return;
		}
		var s = $("#" + _t+' input:checked').size();
		var size = $("#" + _t+' input[type="checkbox"]').size();
		if(s == size)
			$("input[target='#" + _t + "']").prop('checked',true);
	});	
	$(document).delegate(".ajax-form","submit",function(){
		var cb = $(this).attr("callback");
		var $submit = $(this).find("button[type='submit']");
		var text = $submit.text();
		$submit.text("Loading...");
		var $form = $(this);
		setTimeout(function(){
			$form.ajaxSubmit(function(rsp){
				$submit.text(text);
				if(rsp.code===0){
					if(cb){
						if(window[cb])
							window[cb](rsp);
					}else{
						if(rsp.msg){
							alert(rsp.msg);
						}
					}
					return;
				}
				alert(rsp.msg||"操作失败!");
				
			});
		},500);
		return false;
	});
	
	//所有的异步的按钮的单机事件
	$(document).delegate(".btn-ajax","click",function(){
		var url = $(this).attr("ajax-url");
		var cb = $(this).attr("callback");
		$.get(url,function(rsp){
			if(rsp.code===0){
				if(cb){
					window[cb](rsp);
				}else{
					alert("操作成功!");
				}
				return;
			}
			alert(rsp.msg||"操作失败!");
		});
		return false;
	});
	
	$(document).delegate(".cbx_YON","click",function(){
		$(this).prev().val($(this).prop('checked')?1:0);
	});
	
	$(".cbxYON").each(function(){
		var $parent = $(this).parent();
		var v = $(this).val();
		var $cbxYON = $('<input type="checkbox" class="cbx_YON" />');
		$cbxYON.prop('checkbox',v==1);
		$parent.append($cbxYON);
		
	});
	
	
	//初始化datagrid
	
	
})(jQuery);

//日期时间的扩展
Date.prototype.format = function(format) {
	var o = {
		"M+" : this.getMonth() + 1, // month
		"d+" : this.getDate(), // day
		"h+" : this.getHours(), // hour
		"m+" : this.getMinutes(), // minute
		"s+" : this.getSeconds(), // second
		"q+" : Math.floor((this.getMonth() + 3) / 3), // quarter
		"S" : this.getMilliseconds()
	// millisecond
	}
	if (/(y+)/.test(format))
		format = format.replace(RegExp.$1, (this.getFullYear() + "")
				.substr(4 - RegExp.$1.length));
	for ( var k in o)
		if (new RegExp("(" + k + ")").test(format))
			format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k]
					: ("00" + o[k]).substr(("" + o[k]).length));
	return format;
}

window.formatter = {
	yon : function(value){
		return value==1?"是":"否";
	}	,
	state: function(value){
		return value==1?"启动":"禁用";
	},
	appover: function(value){
		return value==0?"待审核":((value==1)?"已通过":"未通过");
	},
	datetime:function(value){
		return new Date(value).format('yyyy-MM-dd hh:mm:ss');
	}
	
};
//datagrid插件
(function($) {
	if ($ == undefined) {
		if (console.log) {
			console.log('jQuery Required!');
		}
		alert('jQuery Required!');
		return false;
	}

	var defaults = {};

	//上传图片的插件
	//this->指向需要打开上传对话况的按钮
	$.fn.datagrid = function(options) {
		var opts = $.extend(defaults, options);
		var $datagrid = $(this).find(".datagrid");
		$(this).find(".remove-bar").click(function() {
			var selects = $datagrid.bootstrapTable('getSelections');
			if (selects.length > 0) {
				if (!confirm("确认删除么?")) {
					return false;
				}
			}else{
				//尚未选择任何一条数据
				return false;
			}
			var ids = $.map(selects, function(row) {
				return row.id;
			});
			var url = $(this).attr("target-url");
			$.post(url + '?id[]=' + ids, function(rsp) {
				if(rsp.code===0){
					$datagrid.bootstrapTable('remove', {
						field : 'id',
						values : ids
					});
				}else{
					alert('删除失败!');
				}
				
			});
		});
		$datagrid.bootstrapTable({
			onDblClickRow : function(row) {
				//alert(JSON.stringify(row));
				//可以进行编辑
				$editForm = $("form.edit-form");
				$editForm.find("#rowid").val(row.id);
				for ( var k in row) {
					var $c = $editForm.find("*[name='" + k + "']");//获取相应的控件
					if (!$c) continue;
					$c.val(row[k]);
					//如果是是否单选框需要作js的操作
					if($c.hasClass("cbxYON")){
						$c.next().prop('checked',row[k]==1);
					}
				}
				$("#edit-form-dialog").modal('show');
			}
		});
		
		return $datagrid;
	};
})(jQuery);    


