<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="/fast" prefix="fast" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@include file="basic/header.jsp"%>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">数据还原</h1>
		</div>
		
	</div><!--/.row-->
			
	
	<div class="row">
		<div class="col-lg-12">
			<div class="alert bg-danger" role="alert">
				<span class="glyphicon glyphicon-exclamation-sign"></span> 还原操作会覆盖当前的数据，如果您需要当前的数据，请跳转至备份页面进行数据的备份！
			</div>
			<div class="col-md-6">
				<div class="panel panel-info">
					<div class="panel-heading">
						<label><span class="glyphicon glyphicon-align-justify"></span> 系统数据库</label>
					</div>
					<div class="panel-body table-grid" id="core-restore-grid">
						<table data-toggle="table" class="datagrid"
						data-sort-name="name" data-sort-order="desc">
						    <thead>
						    <tr>
						        <th data-field="name" data-sortable="true">备份名称</th>
						        <th data-field="createtime"  data-sortable="true" data-formatter="formatter.datetime">备份时间</th>
						        <th >操作</th>
						    </tr>
						    </thead>
						    <tbody>
						    	<c:forEach items="${backupList.core }" var="backup">
						    	<tr>
						    		<td>${backup.name }</td>
						    		<td>${backup.createtime }</td>
						    		<td>
										<button callback="restoreOK" ajax-url="${APP.admin.base }/data/doCoreDbRestore/${backup.id}" class="btn btn-success btn-sm btn-ajax">还原</button>
						    		</td>
						    	</tr>
						    	</c:forEach>
						    </tbody>
						</table>
					</div>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="panel panel-danger">
					<div class="panel-heading">
						<label><span class="glyphicon glyphicon-align-justify"></span> 业务数据库</label>
					</div>
					<div class="panel-body table-grid" id="core-restore-grid">
						<table data-toggle="table" class="datagrid"
						data-sort-name="name" data-sort-order="desc">
						    <thead>
						    <tr>
						        <th data-field="name" data-sortable="true">备份名称</th>
						        <th data-field="createtime"  data-sortable="true" data-formatter="formatter.datetime">备份时间</th>
						        <th >操作</th>
						    </tr>
						    </thead>
						    <tbody>
						    	<c:forEach items="${backupList.biz }" var="backup">
						    	<tr>
						    		<td>${backup.name }</td>
						    		<td>${backup.createtime }</td>
						    		<td>
										<button callback="restoreOK" ajax-url="${APP.admin.base }/data/doBizDbRestore/${backup.id}" class="btn btn-success btn-sm btn-ajax">还原</button>
						    		</td>
						    	</tr>
						    	</c:forEach>
						    </tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		
	</div><!--/.row-->		
	
</div><!--/.main-->
<fast:script>
	<script type="text/javascript">
		var $datagrid = $("#core-restore-grid").datagrid();
		$("#data-collapse").click();
		$("#storeBanner").addClass("active");
		function restoreOK(rsp){
			alert('还原成功');
		}
	</script>
</fast:script>
    
<%@include file="basic/footer.jsp"%>
