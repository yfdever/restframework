<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="/fast" prefix="fast" %> 
<%@include file="basic/header.jsp"%>
        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">数据备份</h1>
			</div>
		</div><!--/.row-->
				
		
		<div class="row">
			<div class="col-lg-12">
				<div class="alert bg-danger" role="alert">
					<span class="glyphicon glyphicon-exclamation-sign"></span> 备份功能需要您在系统配置之初将数据库安装文件的bin目录进行配置，首次安装会由配置人员进行配置，如果需要修改请联系供应商！
				</div>
				<div class="col-md-6">
					<div class="panel panel-info">
						<div class="panel-heading">
							<label><span class="glyphicon glyphicon-align-justify"></span> 系统数据库</label>
						</div>
						<div class="panel-body">
							<div class="alert bg-default" role="alert">
								<span class="glyphicon glyphicon-info-sign"></span> 系统会将当前的数据库中的数据信息进行导出，并将导出的数据保存到指定的磁盘目录下.可用于进行数据的还原.</a>
							</div>
							<div class="col-md-6">
								<button ajax-url="${APP.admin.base }/data/doCoreDbBackup" class="btn btn-success btn-ajax"><span class="glyphicon glyphicon-expand"></span> 点击备份</button>
							</div>
						</div>
					</div>
				</div>
				
				<%-- mongodb的导入导出 --%>
				<div class="col-md-6">
					<div class="panel panel-danger">
						<div class="panel-heading">
							<label><span class="glyphicon glyphicon-align-justify"></span> 业务数据库</label>
						</div>
						<div class="panel-body">
							<div class="alert bg-default" role="alert">
								<span class="glyphicon glyphicon-info-sign"></span> 通常是指对业务数据进行备份，一般是指 mongodb.备份功能可能会对当前系统产生一定的影响，所以请在管理员的协助下进行。</a>
							</div>
							<div class="col-md-6">
								<button ajax-url="${APP.admin.base }/data/doBizDbBackup" class="btn btn-success btn-ajax"><span class="glyphicon glyphicon-expand"></span> 点击备份</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!--/.row-->		
		
		
		<div class="row">
			
			
		</div><!-- /.row -->
	</div><!--/.main-->
<fast:script>
	<script type="text/javascript">
		$("#data-collapse").click();
		$("#backupBanner").addClass("active");
	</script>
</fast:script>
<%@include file="basic/footer.jsp"%>
