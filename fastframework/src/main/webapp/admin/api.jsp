<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="/fast" prefix="fast" %> 
<%@include file="basic/header.jsp"%>
        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">API设置</h1>
			</div>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<div class="alert bg-primary" role="alert">
					<span class="glyphicon glyphicon-exclamation-sign"></span> 设置系统提供的接口参数
				</div>
			</div>
		</div><!--/.row-->	
		<div class="row">
			<div class="col-lg-8">
				<div class="panel panel-default">
					<div class="panel-body">
						<form role="form">
							<div class="form-group">
								<label>App Key</label>
								<input class="form-control" placeholder="32位的md5值">
							</div>
															
							<div class="form-group">
								<label>token过期时间(单位：秒)</label>
								<input type="text" class="form-control" placeholder="600">
							</div>
							
							<div class="form-group">
								<label>仅允许如下IP访问,不填则默认全部</label>
								<input type="text" class="form-control" placeholder="192.168.1.10,192.168.1.*">
							</div>
							<button type="submit" class="btn btn-primary">保存设置</button>
							<button type="reset" class="btn btn-default">重置</button>
						</form>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="panel panel-warning">
					<div class="panel-heading">
						提示
					</div>
					<div class="panel-body">
						<p>非专业的技术人员请勿修改。修改会影响所有其他集成的模块，如：移动端，微信公众平台等</p>
					</div>
				</div>
			</div>
		</div>	
		
	</div><!--/.main-->

<fast:script>
	//此处放置需要导入或者执行的script
	<script type="text/javascript">
	$("#apiBanner").addClass("active");
	</script>
</fast:script>    
<%@include file="basic/footer.jsp"%>
