<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="/fast" prefix="fast" %> 
<%@include file="basic/header.jsp"%>
        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">附件列表</h1>
			</div>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body table-grid" id="attachement-grid">
						<div class="toolbar">
							<div class="btn-group" role="group" aria-label="...">
								<button type="button" data-toggle="modal" data-target="#upload-form-dialog" class="btn btn-info"><span class="glyphicon glyphicon-open"></span> 上传</button>
							  	<button target-url="${APP.admin.base }/attachment/del" type="button" class="btn btn-warning remove-bar"><span class="glyphicon glyphicon-remove"></span> 删除</button>
							</div>
						</div>
						<table data-toggle="table" class="datagrid"
						data-url="${APP.admin.base }/attachment/ajaxList"  
						data-show-refresh="true" data-search="true" 
						data-select-item-name="id[]" data-pagination="true" 
						data-side-pagination="server"
						data-page-list="[10]"
						data-toolbar="#attachement-grid > .toolbar"
						data-sort-name="createtime" data-sort-order="desc">
						    <thead>
						    <tr>
						    	<th data-checkbox="true" >ID</th>
						        <th data-field="id" data-visible="false">ID</th>
						        <th data-field="name" >附件名称</th>
						        <th data-field="type">附件类型</th>
						        <th data-field="folder">文件夹</th>
						        <th data-field="createtime" data-formatter="formatter.datetime">创建时间</th>
						        <th data-formatter="operate">操作</th>
						    </tr>
						    </thead>
						</table>
					</div>
				</div>
			</div>
		</div><!--/.row-->		
		
		
	</div><!--/.main-->
<!-- 新增数据的表单对话框 -->
<div class="modal" id="upload-form-dialog" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="dialog-title"><span class="glyphicon glyphicon-edit"></span> 附件上传</h4>
      </div>
      <div class="modal-body">
        <form action="${APP.admin.base }/attachment/upload" method="post" role="form" class="modal-form" callback="cb1" enctype="multipart/from-data">
			<div class="form-group">
				<label></label>
				<input name="file" type="file" class="form-control" placeholder="" />
			</div>
			
			<hr/>
			<button type="submit" class="btn btn-primary"> 上传</button>
		</form>
      </div>
    </div>
  </div>
</div>
<!-- 新增数据的表单对话框 END -->

<!-- 编辑数据的表单对话框 END -->
<fast:script>
	<script type="text/javascript">
	
		var $datagrid = $("#attachement-grid").datagrid();

		var data = {};
		//输出操作的html代码
		var operate = function(value,row,index){
			data[index] = row;
			return '<a href="${APP.admin.base }/attachment/download/'+row.id+'" class="btn btn-success btn-sm">下载</a>';
		}
		
		var cb1 = function(rst) {
			$("#upload-form-dialog").modal('hide');
			$datagrid.bootstrapTable('refresh');
		};

		$("#attachmentBanner").addClass("active");
	</script>
</fast:script>
    
<%@include file="basic/footer.jsp"%>
