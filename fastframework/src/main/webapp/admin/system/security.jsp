<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="/fast" prefix="fast" %> 
<%@include file="../basic/header.jsp"%>
        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">权限设置</h1>
			</div>
		</div><!--/.row-->
		<%--消息提醒
		<div class="row hidden">
			<div class="col-lg-12">
				<div class="alert bg-primary" role="alert">
					<span class="glyphicon glyphicon-exclamation-sign"></span> 消息
				</div>
			</div>
		</div><!--/.row-->	
		 --%>
		<div class="row">
		
			<div class="col-md-4">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<label><input type="checkbox" />  基本用户权限</label>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-sm-3">
								<label><input type="checkbox" name="auth[]" value="1" /> <span>登录</span></label>
							</div>
							<div class="col-sm-3">
								<label><input type="checkbox" name="auth[]" value="1" /> <span>登录</span></label>
							</div>
							<div class="col-sm-3">
								<label><input type="checkbox" name="auth[]" value="1" /> <span>登录</span></label>
							</div>
							<div class="col-sm-3">
								<label><input type="checkbox" name="auth[]" value="1" /> <span>登录</span></label>
							</div>
						</div>
						<hr/>
						<div class="row">
							<div class="col-sm-3">
								<label><input type="checkbox" name="auth[]" value="1" /> <span>登录</span></label>
							</div>
							<div class="col-sm-3">
								<label><input type="checkbox" name="auth[]" value="1" /> <span>登录</span></label>
							</div>
							<div class="col-sm-3">
								<label><input type="checkbox" name="auth[]" value="1" /> <span>登录</span></label>
							</div>
							<div class="col-sm-3">
								<label><input type="checkbox" name="auth[]" value="1" /> <span>登录</span></label>
							</div>
						</div>
						<hr/>
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<label><input type="checkbox" />  基本用户权限</label>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-sm-3">
								<label><input type="checkbox" name="auth[]" value="1" /> <span>登录</span></label>
							</div>
							<div class="col-sm-3">
								<label><input type="checkbox" name="auth[]" value="1" /> <span>登录</span></label>
							</div>
							<div class="col-sm-3">
								<label><input type="checkbox" name="auth[]" value="1" /> <span>登录</span></label>
							</div>
							<div class="col-sm-3">
								<label><input type="checkbox" name="auth[]" value="1" /> <span>登录</span></label>
							</div>
						</div>
						<hr/>
						<div class="row">
							<div class="col-sm-3">
								<label><input type="checkbox" name="auth[]" value="1" /> <span>登录</span></label>
							</div>
							<div class="col-sm-3">
								<label><input type="checkbox" name="auth[]" value="1" /> <span>登录</span></label>
							</div>
							<div class="col-sm-3">
								<label><input type="checkbox" name="auth[]" value="1" /> <span>登录</span></label>
							</div>
							<div class="col-sm-3">
								<label><input type="checkbox" name="auth[]" value="1" /> <span>登录</span></label>
							</div>
						</div>
						<hr/>
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<label><input type="checkbox" />  基本用户权限</label>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-sm-3">
								<label><input type="checkbox" name="auth[]" value="1" /> <span>登录</span></label>
							</div>
							<div class="col-sm-3">
								<label><input type="checkbox" name="auth[]" value="1" /> <span>登录</span></label>
							</div>
							<div class="col-sm-3">
								<label><input type="checkbox" name="auth[]" value="1" /> <span>登录</span></label>
							</div>
							<div class="col-sm-3">
								<label><input type="checkbox" name="auth[]" value="1" /> <span>登录</span></label>
							</div>
						</div>
						<hr/>
						<div class="row">
							<div class="col-sm-3">
								<label><input type="checkbox" name="auth[]" value="1" /> <span>登录</span></label>
							</div>
							<div class="col-sm-3">
								<label><input type="checkbox" name="auth[]" value="1" /> <span>登录</span></label>
							</div>
							<div class="col-sm-3">
								<label><input type="checkbox" name="auth[]" value="1" /> <span>登录</span></label>
							</div>
							<div class="col-sm-3">
								<label><input type="checkbox" name="auth[]" value="1" /> <span>登录</span></label>
							</div>
						</div>
						<hr/>
					</div>
				</div>
			</div>
			
		</div><!-- /.row -->
		
	</div><!--/.main-->

<fast:script>
	<script type="text/javascript">
	
	$("#user-collapse").click();
	$("#userroleBanner").addClass("active");
	</script>
</fast:script>    
<%@include file="../basic/footer.jsp"%>
