<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="/fast" prefix="fast" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@include file="../basic/header.jsp"%>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">插件管理</h1>
		</div>
		
	</div><!--/.row-->
			
	
	<div class="row">
		<div class="col-lg-12">
			<div class="alert bg-danger" role="alert">
				<span class="glyphicon glyphicon-exclamation-sign"></span> 插件的开启和关闭会影响系统的运行状态，请在管理员的协助下进行操作！
			</div>
			<div class="panel panel-default">
				<div class="panel-body table-grid" id="plugingrid">
					<div class="toolbar">
						<div class="btn-group" role="group" aria-label="...">
						  <button type="button" data-toggle="modal" data-target="#form-dialog" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> 新建</button>
						  <button target-url="${APP.admin.base }/plugin/del" type="button" class="btn btn-warning remove-bar"><span class="glyphicon glyphicon-remove"></span> 删除</button>
						</div>
					</div>
					<table data-toggle="table" class="datagrid"
					data-url="${APP.admin.base }/plugin/ajaxList"  
					data-show-refresh="true" data-search="true" 
					data-select-item-name="id[]" data-pagination="true" 
					data-side-pagination="server"
					data-click-to-select="false"
					data-page-list="[10]"
					data-toolbar="#plugingrid > .toolbar"
					data-sort-name="name" data-sort-order="desc">
					    <thead>
					    <tr>
					    	<th data-checkbox="true" >ID</th>
					        <th data-field="id" data-visible="false">ID</th>
					        <th data-field="name" >插件名称</th>
					        <th data-field="title">插件描述</th>
					        <th data-field="class">类名</th>
					        <th data-field="name" data-formatter="getStatus" >状态</th>
					        <th data-field="autoload" data-formatter="formatter.yon">自动加载</th>
					        <th data-field="name" data-formatter="operate">操作</th>
					    </tr>
					    </thead>
					</table>
				</div>
			</div>
		</div>
		
	</div><!--/.row-->		
	
</div><!--/.main-->

<!-- 新增数据的表单对话框 -->
<div class="modal" id="form-dialog" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="dialog-title"><span class="glyphicon glyphicon-edit"></span> 新建插件</h4>
      </div>
      <div class="modal-body">
        <form action="${APP.admin.base }/plugin/add" method="post" role="form" class="modal-form" callback="cb1">
			<div class="form-group">
				<label>* 插件名称</label>
				<input name="name" class="form-control" placeholder="" />
			</div>
			
			<div class="form-group">
				<label>* 插件描述</label>
				<textarea name="title" class="form-control" rows="3" placeholder=""></textarea>
			</div>
			<div class="form-group">
				<label>* Class</label>
				<textarea name="class" class="form-control" rows="3" placeholder=""></textarea>
			</div>
			<div class="form-group">
				<label> 配置参数，需保证为JSON格式</label>
				<textarea name="args" class="form-control" rows="3" placeholder=""></textarea>
			</div>
			<div class="form-group">
				<label> 配置路径，请确保该文件路径正确有效</label>
				<textarea name="filepath" class="form-control" rows="3" placeholder=""></textarea>
			</div>
			<div class="form-group">
				<label> 自动加载 </label>
				<input name="autoload" type="hidden" value="0" class="cbxYON" />
			</div>
			<hr/>
			<button type="submit" class="btn btn-primary">保存设置</button>
			<button type="reset" class="btn btn-default">重置</button>
		</form>
      </div>
    </div>
  </div>
</div>
<!-- 新增数据的表单对话框 END -->

<!-- 编辑数据的表单对话框 -->
<div class="modal" id="edit-form-dialog" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="dialog-title"><span class="glyphicon glyphicon-edit"></span> 修改信息</h4>
      </div>
      <div class="modal-body">
        <form action="${APP.admin.base }/plugin/edit" method="post" role="form" class="modal-form edit-form" callback="cb2">
        	<input name="id" type="hidden" id="rowid" />
			<div class="form-group">
				<label>* 插件名称</label>
				<input name="name" class="form-control" placeholder="" />
			</div>
			
			<div class="form-group">
				<label>* 插件描述</label>
				<textarea name="title" class="form-control" rows="3" placeholder=""></textarea>
			</div>
			<div class="form-group">
				<label>* Class</label>
				<textarea name="class" class="form-control" rows="3" placeholder=""></textarea>
			</div>
			<div class="form-group">
				<label> 配置参数，需保证为JSON格式</label>
				<textarea name="args" class="form-control" rows="3" placeholder=""></textarea>
			</div>
			<div class="form-group">
				<label> 配置路径，请确保该文件路径正确有效</label>
				<textarea name="filepath" class="form-control" rows="3" placeholder=""></textarea>
			</div>
			<div class="form-group">
				<label> 自动加载 </label>
				<input name="autoload" type="hidden" value="0" class="cbxYON" />
			</div>
			<hr/>
			<button type="submit" class="btn btn-primary">保存修改</button>
		</form>
      </div>
    </div>
  </div>
</div>
<!-- 编辑数据的表单对话框 END -->



<fast:script>
	<script type="text/javascript">
		var $datagrid = $("#plugingrid").datagrid();
		$("#system-collapse").click();
		$("#pluginBanner").addClass("active");
		
		var action = "${APP.admin.base }/plugin/";
		function operate(value,row,index){
			if(row.status == 'ACTIVED'){
				return '<button id="btn-'+value+'" class="btn btn-danger btn-xs" onclick="stopPlugin('+index+',\''+value+'\')"><span class="glyphicon glyphicon-pause"></span></button>';	
			}
			return '<button id="btn-'+value+'" class="btn btn-info btn-xs" onclick="startPlugin('+index+',\''+value+'\')"><span class="glyphicon glyphicon-play"></span></button>';
			
		}
		
		function startPlugin(index,name){
			$.getJSON(action+'startPlugin/'+name,function(res){
				if(res.code===0){
					//启动成功
					$datagrid.bootstrapTable('updateRow', {
	                    index: index,
	                    row: {
	                        name: name,
	                    }
	                });
				}else{
					alert('启动失败!\n'+res.msg);
				}
			});
		}
		
		function stopPlugin(index,name){
			$.getJSON(action+'stopPlugin/'+name,function(res){
				if(res.code===0){
					//停止成功
					$datagrid.bootstrapTable('updateRow', {
	                    index: index,
	                    row: {
	                        name: name,
	                    }
	                });
				}else{
					alert('停止失败!\n'+res.msg);
				}
			});
		}
		
		function getStatus(value,row,index){
			$.ajaxSettings.async = false; 
			$.getJSON(action+'getStatus/'+value,function(res){
				row.status = res.msg;
			});
			$.ajaxSettings.async = true;
			return row.status;
		}
		
		var cb1 = function(rst) {
			$("#form-dialog").modal('hide');
			$datagrid.bootstrapTable('refresh');
		};
		
		var cb2 = function(rst){
			$("#edit-form-dialog").modal('hide');
			$datagrid.bootstrapTable('refresh');
		}
		
		
	</script>
</fast:script>
    
<%@include file="../basic/footer.jsp"%>
