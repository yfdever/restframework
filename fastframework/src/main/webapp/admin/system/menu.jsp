<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="/fast" prefix="fast" %> 
<%@include file="../basic/header.jsp"%>
        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Menu列表</h1>
			</div>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body table-grid" id="menugrid">
						<div class="toolbar">
							<div class="btn-group" role="group" aria-label="...">
							  <button type="button" data-toggle="modal" data-target="#form-dialog" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> 新建</button>
							  <button target-url="${APP.admin.base }/user/del" type="button" class="btn btn-warning remove-bar"><span class="glyphicon glyphicon-remove"></span> 删除</button>
							</div>
						</div>
						<table data-toggle="table" class="datagrid"
						data-url="${APP.admin.base }/system/menu/ajaxList"  
						data-show-refresh="true" data-search="true" 
						data-select-item-name="id[]" data-pagination="true" 
						data-side-pagination="server"
						data-click-to-select="true"
						data-page-list="[10]"
						data-toolbar="#menugrid > .toolbar"
						data-sort-name="login_name" data-sort-order="desc">
						    <thead>
						    <tr>
						    	<th data-checkbox="true" >ID</th>
						        <th data-field="id" data-visible="false">ID</th>
						        <th data-field="login_name" >登录名</th>
						        <th data-field="email">email</th>
						        <th data-field="display_name"  data-sortable="true">昵称</th>
						        <th data-field="is_admin" data-formatter="formatter.yon">管理员</th>
						        <th data-field="createtime" data-formatter="formatter.datetime">创建时间</th>
						        <th data-formatter="operate">操作</th>
						        
						    </tr>
						    </thead>
						</table>
					</div>
				</div>
			</div>
		</div><!--/.row-->		
		
		
	</div><!--/.main-->

<!-- 新增数据的表单对话框 -->
<div class="modal" id="form-dialog" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="dialog-title"><span class="glyphicon glyphicon-edit"></span> 新建信息</h4>
      </div>
      <div class="modal-body">
        <form action="${APP.admin.base }/user/add" method="post" role="form" class="modal-form" callback="cb1">
			<div class="form-group">
				<label>* 登录名 一般使用唯一的英文标示，需要保证唯一性</label>
				<input name="login_name" class="form-control" placeholder="" />
			</div>
			
			<div class="form-group">
				<label> 昵称</label>
				<input name="display_name" class="form-control" placeholder="" />
			</div>
			
			<div class="form-group">
				<label>* 邮箱，需要保证唯一性</label>
				<input type="email" name="email" class="form-control" />
			</div>
				
			<div class="form-group">
				<label>* 密码</label>
				<input name="login_pass" type="password" class="form-control" placeholder="大于6位">
			</div>
			
			<div class="form-group">
				<label>* 确认密码</label>
				<input type="password" class="form-control" placeholder="">
			</div>	
			
			<div class="form-group">
				<label>* 密码加密方式</label>
				<select name="pass_encode" class="form-control">
					<option value="MD5">MD5</option>
					<option value="SHA-1">SHA-1</option>
					<option value="SHA-256">SHA-256</option>
					<option value="SHA-384">SHA-384</option>
					<option value="SHA-512">SHA-512</option>
				</select>
			</div>
			
			<div class="form-group">
				<label>* 设置为管理员 </label>
				<input name="is_admin" type="hidden" value="0" class="cbxYON" />
				
			</div>
			
			
			<hr/>
			<button type="submit" class="btn btn-primary">保存设置</button>
			<button type="reset" class="btn btn-default">重置</button>
		</form>
      </div>
    </div>
  </div>
</div>
<!-- 新增数据的表单对话框 END -->

<!-- 编辑数据的表单对话框 -->
<div class="modal" id="edit-form-dialog" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="dialog-title"><span class="glyphicon glyphicon-edit"></span> 修改信息</h4>
      </div>
      <div class="modal-body">
        <form action="${APP.admin.base }/user/edit" method="post" role="form" class="modal-form edit-form" callback="cb2">
        	<input name="id" type="hidden" id="rowid" />
			<div class="form-group">
				<label>* 登录名 一般使用唯一的英文标示，需要保证唯一性</label>
				<input name="login_name" class="form-control" placeholder="" readonly="readonly" />
			</div>
			
			<div class="form-group">
				<label> 昵称</label>
				<input name="display_name" class="form-control" placeholder="" />
			</div>
			
			<div class="form-group">
				<label>* 邮箱，需要保证唯一性</label>
				<input type="email" name="email" class="form-control" />
			</div>
			<div class="form-group">
				<label>* 设置为管理员 </label>
				<input name="is_admin" type="hidden" value="1" class="cbxYON" />
			</div>
			<hr/>
			<button type="submit" class="btn btn-primary">保存修改</button>
		</form>
      </div>
    </div>
  </div>
</div>
<!-- 编辑数据的表单对话框 END -->

<!-- 编辑数据的表单对话框 -->
<div class="modal" id="acl-dialog" tabindex="-1" role="dialog" aria-hidden="true" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="dialog-title"><span class="glyphicon glyphicon-edit"></span> 权限信息</h4>
      </div>
      <div class="modal-body">
        <form action="${APP.admin.base }/user/edit" method="post" role="form" class="modal-form edit-form" callback="cb2">
        	<input name="id" type="hidden" id="rowid" />
			<div class="form-group">
				<label>* 登录名 一般使用唯一的英文标示，需要保证唯一性</label>
				<input name="login_name" class="form-control" placeholder="" readonly="readonly" />
			</div>
			
			<div class="form-group">
				<label> 昵称</label>
				<input name="display_name" class="form-control" placeholder="" />
			</div>
			
			<div class="form-group">
				<label>* 邮箱，需要保证唯一性</label>
				<input type="email" name="email" class="form-control" />
			</div>
			<div class="form-group">
				<label>* 设置为管理员 </label>
				<input name="is_admin" type="hidden" value="1" class="cbxYON" />
			</div>
			<hr/>
			<button type="submit" class="btn btn-primary">保存修改</button>
		</form>
      </div>
    </div>
  </div>
</div>
<!-- 编辑数据的表单对话框 END -->
<fast:script>
	<script type="text/javascript">
	
		var $datagrid = $("#usergrid").datagrid();

		var data = {};
		//输出操作的html代码
		var operate = function(value,row,index){
			data[index] = row;
			return '<button data-toggle="modal" data-index="'+index+'" data-target="#acl-dialog" class="btn btn-success btn-sm">权限管理</button>';
		}
		
		$('#acl-dialog').on('show.bs.modal', function(event) {
			var button = $(event.relatedTarget);
			var index = button.attr("data-index");
			var modal = $(this);
			console.log(data[index]);
		})

		var cb1 = function(rst) {
			$("#form-dialog").modal('hide');
			$datagrid.bootstrapTable('refresh');
		};

		var cb2 = function(rst) {
			$editForm = $("form.edit-form");
			$editForm.resetForm();
			$("#edit-form-dialog").modal('hide');
			$datagrid.bootstrapTable('refresh');
		};
		$("#user-collapse").click();
		$("#userlistBanner").addClass("active");
	</script>
</fast:script>
    
<%@include file="../basic/footer.jsp"%>
