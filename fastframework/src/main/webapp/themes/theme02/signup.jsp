<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="/fast" prefix="fast" %> 
<!DOCTYPE html>
<html class="login-bg">
<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>欢迎使用</title>
	<link rel="shortcut icon" href="${APP.admin.base}/favicon.ico" />
		
    <!-- bootstrap -->
    <link href="${APP.theme.css}/bootstrap/bootstrap.css" rel="stylesheet" />
    <link href="${APP.theme.css}/bootstrap/bootstrap-responsive.css" rel="stylesheet" />
    <link href="${APP.theme.css}/bootstrap/bootstrap-overrides.css" type="text/css" rel="stylesheet" />

    <!-- global styles -->
    <link rel="stylesheet" type="text/css" href="${APP.theme.css}/layout.css" />
    <link rel="stylesheet" type="text/css" href="${APP.theme.css}/elements.css" />
    <link rel="stylesheet" type="text/css" href="${APP.theme.css}/icons.css" />

    <!-- libraries -->
    <link rel="stylesheet" type="text/css" href="${APP.theme.css}/lib/font-awesome.css" />
    
    <!-- this page specific styles -->
    <link rel="stylesheet" href="${APP.theme.css}/compiled/signup.css" type="text/css" media="screen" />
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
    <div class="header">
        <a href="index.html">
            <img src="${APP.theme.img}/logo.png" class="logo" />
        </a>
    </div>
    <div class="row-fluid login-wrapper">
        <div class="box">
            <div class="content-wrap">
                <h6>填写用户信息</h6>
                <input class="span12" type="text" placeholder="邮箱地址" />
                <input class="span12" type="password" placeholder="密码" />
                <input class="span12" type="password" placeholder="确认密码" />
                <div class="action">
                    <a class="btn-flat primary signup" href="index.html">注 册</a>
                </div>                
            </div>
        </div>
    </div>

	<!-- scripts -->
    <script src="${APP.theme.js}/jquery.js"></script>
    <script src="${APP.theme.js}/bootstrap.min.js"></script>
    <script src="${APP.theme.js}/theme.js"></script>
</body>
</html>