<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <script src="${APP.theme.js}/jquery.js"></script>
    <script src="${APP.theme.js }/bootstrap.min.js"></script>
    <script src="${APP.theme.js }/jquery-ui-1.10.2.custom.min.js"></script>
    <script src="${APP.theme.js }/theme.js"></script>
    
	<%= pageContext.getAttribute("script") %>
	<% pageContext.removeAttribute("script"); %>
</body>

</html>