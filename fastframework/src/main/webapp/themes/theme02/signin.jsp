<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="/fast" prefix="fast" %> 
<!DOCTYPE html>
<html class="login-bg">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>欢迎使用</title>
	<link rel="shortcut icon" href="${APP.admin.base}/favicon.ico" />
		
    <!-- bootstrap -->
    <link href="${APP.theme.css}/bootstrap/bootstrap.css" rel="stylesheet" />
    <link href="${APP.theme.css}/bootstrap/bootstrap-responsive.css" rel="stylesheet" />
    <link href="${APP.theme.css}/bootstrap/bootstrap-overrides.css" type="text/css" rel="stylesheet" />

    <!-- global styles -->
    <link rel="stylesheet" type="text/css" href="${APP.theme.css}/layout.css" />
    <link rel="stylesheet" type="text/css" href="${APP.theme.css}/elements.css" />
    <link rel="stylesheet" type="text/css" href="${APP.theme.css}/icons.css" />

    <!-- libraries -->
    <link rel="stylesheet" type="text/css" href="${APP.theme.css}/lib/font-awesome.css" />
    
    <!-- this page specific styles -->
    <link rel="stylesheet" href="${APP.theme.css}/compiled/signin.css" type="text/css" media="screen" />
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
    <div class="row-fluid login-wrapper">
        <a href="index.html">
            <img class="logo" src="${APP.theme.img}/logo-white.png" />
        </a>
        <div class="span4 box">
        	<form method="post" action="${APP.theme.base }/doLogin">
            <div class="content-wrap">
                <h6>登录系统</h6>
                <input class="span12" name="email" type="email" placeholder="邮箱" />
                <input class="span12" name="password" type="password" placeholder="密码" />
                <a href="#" class="forgot">忘记密码?</a>
                <button class="btn-flat primary login" href="index.html">登 录</button>
            </div>
            </form>
        </div>

        <div class="span4 no-account">
            <p>还没有账号?</p>
            <a href="signup">注册</a>
        </div>
    </div>

	<!-- scripts -->
    <script src="${APP.theme.js}/jquery.js"></script>
    <script src="${APP.theme.js}/bootstrap.min.js"></script>
    <script src="${APP.theme.js}/theme.js"></script>
</body>
</html>