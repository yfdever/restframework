package biz.yfsoft.app.fastframework.admin;


public class ApiController extends AdminController {
	
	@Override
	protected String getTable() {
		return "sys_api";
	}


	public void index() {
		render("/admin/api.jsp");
	}
	
}
