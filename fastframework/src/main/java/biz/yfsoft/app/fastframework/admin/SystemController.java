package biz.yfsoft.app.fastframework.admin;


public class SystemController extends AdminController {
	
	@Override
	protected String getTable() {
		return "sys_api";
	}

	@Override
	protected String getSearchFields(String searchs) {
		if(isSqlInject(searchs)){
			return " 1 = 2 ";
		}
		
		if(searchs == null){
			return " 1 = 1 ";
		}
		return " name like '%{key}%' or title like '%{key}%'".replace("{key}", searchs);
	}
	

	public void index() {
		render("/admin/api.jsp");
	}
	
	public void plugin() {
		render("/admin/system/plugin.jsp");
	}
	
}
