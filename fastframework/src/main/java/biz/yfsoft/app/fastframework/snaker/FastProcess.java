package biz.yfsoft.app.fastframework.snaker;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.sql.Blob;

import org.snaker.engine.SnakerException;
import org.snaker.engine.helper.StreamHelper;

public class FastProcess implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -975518365315145434L;

	private String id;
	
	private String name;
	
	private String displayName;
	
	private String type;
	
	private String instanceUrl;
	
	private String state;
	
	private Blob content;
	
	private String createTime;
	
	private String creator;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getInstanceUrl() {
		return instanceUrl;
	}

	public void setInstanceUrl(String instanceUrl) {
		this.instanceUrl = instanceUrl;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public byte[] getDBContent() {
		if(this.content != null) {
			try {
				return this.content.getBytes(1L, Long.valueOf(this.content.length()).intValue());
			} catch (Exception e) {
				try {
					InputStream is = content.getBinaryStream();
					return StreamHelper.readBytes(is);
				} catch (Exception e1) {
					throw new SnakerException("couldn't extract stream out of blob", e1);
				}
			}
		}
		return null;
	}
	
	public void setDBContent(byte[] bytes){
		try {
			content.setBytes(0, bytes, 0, bytes.length);
		} catch (Exception e1) {
			try {
				OutputStream out = content.setBinaryStream(0);
				out.write(bytes, 0, bytes.length);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public Blob getContent() {
		return content;
	}

	public void setContent(Blob content) {
		this.content = content;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}
	
	
}
