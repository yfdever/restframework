package biz.yfsoft.app.fastframework.core.hex;

public class UserExistsException extends Exception {

	private static final long serialVersionUID = -8846577867574263551L;

	public String getMessage(){
		return "用户已存在!";
	}
}
