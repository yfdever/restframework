package biz.yfsoft.app.fastframework.tag;

import javax.servlet.jsp.tagext.TagSupport;

public class TitleTag extends TagSupport {


	private static final long serialVersionUID = -6149887324815359562L;


	public void setTitle(String title) {
		this.pageContext.setAttribute("title", title);
	}


	public void setKeyword(String keyword) {
		this.pageContext.setAttribute("keyword", keyword);
	}


	public void setDescription(String description) {
		this.pageContext.setAttribute("description", description);
	}
	
}
