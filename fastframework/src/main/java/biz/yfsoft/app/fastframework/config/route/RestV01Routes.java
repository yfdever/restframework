package biz.yfsoft.app.fastframework.config.route;

import biz.yfsoft.app.fastframework.api.ShopController;
import biz.yfsoft.app.fastframework.api.RestController;

import com.jfinal.config.Routes;

public class RestV01Routes extends Routes {

	@Override
	public void config() {
		add("/rest", RestController.class);
		add("/api/v01/shop", ShopController.class);
	}

}
