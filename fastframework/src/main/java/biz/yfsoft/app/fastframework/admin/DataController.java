package biz.yfsoft.app.fastframework.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import biz.yfsoft.app.fastframework.kit.DataKit;
import biz.yfsoft.app.fastframework.kit.DataKit.CmdResult;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

public class DataController extends AdminController {
	
	/*
	 * 跳转到备份的页面
	 */
	public void backup() {
		render("/admin/backup.jsp");
	}

	//进行核心数据库的备份
	public void doCoreDbBackup(){
    	CmdResult result = DataKit.backupMysql();
    	if(result.flag){
    		renderJson(wrapJson(0));
    	}else{
    		renderJson(wrapJson(-1,"备份指令执行失败!\n可能原因如下:\n"+result.error));
    	}
	}
	
	/**
	 * 对业务数据进行备份
	 */
	public void doBizDbBackup(){
		CmdResult result = DataKit.backupMongodb();
    	if(result.flag){
    		renderJson(wrapJson(0));
    	}else{
    		renderJson(wrapJson(-1,"备份指令执行失败!\n可能原因如下:\n"+result.error));
    	}
	}
	
	/**
	 * 对业务数据进行还原
	 */
	public void doBizDbRestore(){
		int id = getParaToInt(0);
		CmdResult result = DataKit.restoreMongodb(id);
    	if(result.flag){
    		renderJson(wrapJson(0));
    	}else{
    		renderJson(wrapJson(-1,"还原指令执行失败!\n可能原因如下:\n"+result.error));
    	}
	}
	
	/*
	 * 跳转到还原页面
	 */
	public void restore() {
		
		List<Record> list = Db.find("select * from sys_file where status>0 and type='bak'");
		//封装成相应的数据结构传递到视图中进行显示
		Map<String,List<Record>> backup = new HashMap<String,List<Record>>();
		backup.put("core",list);
		
		list = Db.find("select * from sys_file where status>0 and type='bat'");
		backup.put("biz",list);
		
		setAttr("backupList", backup);
		render("/admin/restore.jsp");
	}
	
	
	/**
	 * 执行还原
	 */
	public void doCoreDbRestore() {
		int id = getParaToInt(0);
		CmdResult result = DataKit.restoreMysql(id);
    	if(result.flag){
    		renderJson(wrapJson(0));
    	}else{
    		renderJson(wrapJson(-1,"还原指令执行失败!\n可能原因如下:\n"+result.error));
    	}
	}
	
	/**
	 * 下载导出的文件
	 */
	public void download() {
		//已使用七牛云存储的方式进行文档的下载
	}
}
