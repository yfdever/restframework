package biz.yfsoft.app.fastframework.core.hex;

public class AuthErrorException extends Exception {

	private static final long serialVersionUID = 763458797653791112L;

	public String getMessage(){
		return "用户名或密码错误";
	}
}
