package biz.yfsoft.app.fastframework.plugin.mongodb;

import java.util.ArrayList;
import java.util.List;

public class BoSorter {

	private List<BoEntry> _list = new ArrayList<BoEntry>();
	public BoSorter add(String k,String v){
		_list.add(new BoEntry(k,v));
		return this;
	}
	
	public List<BoEntry> getList(){
		return this._list;
	}
}
