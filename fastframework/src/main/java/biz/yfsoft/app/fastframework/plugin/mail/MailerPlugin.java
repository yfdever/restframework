package biz.yfsoft.app.fastframework.plugin.mail;

import biz.yfsoft.app.fastframework.plugin.BaseFastPlugin;

/**
 * Created by wangrenhui on 14-5-6.
 */
public class MailerPlugin extends BaseFastPlugin {

	private String host;
	private String sslport = "465";
	private String timeout = "60000";
	private String connectout = "60000";
	private String port = "25";
	private String ssl = "false";
	private String tls = "false";
	private String debug = "false";
	private String user;
	private String password;
	private String name;
	private String from;
	private String encode = "UTF-8";

	public static MailerConf mailerConf;

	public MailerPlugin() {

	}

	@Override
	public boolean start() {
		// 动态读取配置信息
		host = configJson.getString("host");
		user = configJson.getString("user");
		from = configJson.getString("user");
		password = configJson.getString("password");
		name = configJson.getString("name");

		mailerConf = new MailerConf(host, sslport, Integer.parseInt(timeout),
				Integer.parseInt(connectout), port, Boolean.parseBoolean(ssl),
				Boolean.parseBoolean(tls), Boolean.parseBoolean(debug), user,
				password, name, from, encode);
		status = Status.ACTIVED;
		return true;
	}

}
