package biz.yfsoft.app.fastframework.admin;


public class SettingController extends AdminController {
	
	@Override
	protected String getTable() {
		return "sys_setting";
	}
	
	public void index() {
		render("/admin/setting.jsp");
	}
	
	@Override
	protected String getSearchFields(String searchs) {
		if(isSqlInject(searchs)){
			return " 1 = 2 ";
		}
		
		if(searchs == null){
			return " 1 = 1 ";
		}
		return " name like '%{key}%' or title like '%{key}%'".replace("{key}", searchs);
	}
	
}
