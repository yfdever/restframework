package biz.yfsoft.app.fastframework.core.hex;

public class EmailNotExistsException extends Exception {

	private static final long serialVersionUID = -8808245048545199361L;

	public String getMessage(){
		return "该邮箱不存在";
	}
}
