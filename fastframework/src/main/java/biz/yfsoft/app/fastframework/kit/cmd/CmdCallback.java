package biz.yfsoft.app.fastframework.kit.cmd;

public interface CmdCallback {
	public void success();
	public void error(String error);
}
