package biz.yfsoft.app.fastframework.kit;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.Expand;
import org.apache.tools.ant.taskdefs.Zip;
import org.apache.tools.ant.types.FileSet;

public class FileKit {
	
	private static final Project DEFAULT_PROJECT = new Project();
    

	public static String getExt(String path){
		int pos = path.lastIndexOf(".")+1;
		if(pos<0){
			return "dir";
		}
		return path.substring(pos);
	}
	
	/**
	 * 解压一个文件到指定的目录下。
	 * */
	public static void unZip(File orgin, File dest) {
        Expand expand = new Expand();
        expand.setProject(DEFAULT_PROJECT);
        expand.setSrc(orgin);
        expand.setDest(dest);
        expand.execute();
    }
	
	/**
	 * 压缩一个目录到指定的zip文件中
	 */
    public static void zip(File orgin, File dest) {
         
        Zip zip = new Zip();
        zip.setProject(DEFAULT_PROJECT);
        zip.setDestFile(dest);
         
        FileSet fs = new FileSet();
        fs.setProject(DEFAULT_PROJECT);
        fs.setDir(orgin);
//      fs.setIncludes("**/*.java");
//      fs.setExcludes("**/*.xml");
         
        zip.addFileset(fs);
        zip.execute();
         
    }
	
    
    public static boolean checkFileExists(String filepath){
    	File f = new File(filepath);
    	return f.exists();
    }
	
	
	public static void main(String[] args){
		zip(new File("/tmp/bat_20150706171058"),new File("/tmp/bat_20150706171058.zip"));
		
	}
	
	/**
	 * 将指定目录下的文件加载到字节数组中
	 * @param filename
	 * @return
	 * @throws IOException
	 */
	public static byte[] toByteArray(String filename) throws IOException {  
		  
        File f = new File(filename);  
        return toByteArray(new FileInputStream(f));
    }
	
	/**
	 * 将指定目录下的文件加载到字节数组中
	 * @param filename
	 * @return
	 * @throws IOException
	 */
	public static byte[] toByteArray(InputStream in) throws IOException {  
		  
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        BufferedInputStream bin = null;  
        try {  
            bin = new BufferedInputStream(in);
            int buf_size = 1024;
            byte[] buffer = new byte[buf_size];
            int len = 0;  
            while (-1 != (len = bin.read(buffer, 0, buf_size))) {  
                bos.write(buffer, 0, len);  
            }  
            return bos.toByteArray();  
        } catch (IOException e) {  
            throw e;  
        } finally {  
            try {  
                bin.close();  
            } catch (IOException e) {  
            } 
            bos.close();  
        }  
    }
	
	/**
	 * 创建一个随机的文件
	 * @return
	 */
	public static File createTempFile(String suffix){
		File f = null;
		try {
			f = File.createTempFile("data", "."+suffix);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return f;
	}
	
	public static void writeFile(File f,byte[] data){
		FileOutputStream fos = null;
		BufferedOutputStream bos = null;
		try {
			fos = new FileOutputStream(f);
			bos = new BufferedOutputStream(fos);
			bos.write(data);
			bos.flush();   
			bos.close();   
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public static String readFile(String fp){
		return readFile(new File(fp));
	}
	
	public static String readFile(File f){
		BufferedReader reader = null;
		String tempString = null;
		String finalString = "";
		try {
			reader = new BufferedReader(new FileReader(f));
			while ((tempString = reader.readLine()) != null) {
				finalString += tempString;
            }
            reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return finalString;
		
	}
}
