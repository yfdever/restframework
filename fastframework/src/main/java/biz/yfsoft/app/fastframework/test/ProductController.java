package biz.yfsoft.app.fastframework.test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.snaker.engine.entity.Order;

import biz.yfsoft.app.fastframework.Constant;
import biz.yfsoft.app.fastframework.interceptor.WorkFlowInterceptor;
import biz.yfsoft.app.fastframework.snaker.FastWork;
import biz.yfsoft.app.fastframework.snaker.WorkFlowController;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.tx.Tx;

/**
 * 商品控制器
 *
 * @author yanhao
 */
public class ProductController extends WorkFlowController {

	@Override
	protected String getTable() {
		return "test_product";
	}
	
	public void list() {
		Record user = getSessionAttr(Constant.CURRENT_USER);
		int limit = getParaToInt("limit",10);//每页显示的条数
		int offset = getParaToInt("offset",0);//从0开始偏移
		int page = offset/limit+1;
		Page<Record> pageRecord = Db.paginate(page, limit, "select * ", "FROM " + getTable() + " WHERE creator=? order by ? ?", new Object[]{user.getStr("login_name"), getPara("sort"), getPara("order")});
		JSONObject rst = new JSONObject();
		rst.put("total", pageRecord.getTotalRow());
		rst.put("rows", pageRecord.getList());
		renderJson(rst);
	}
	
	@Override
	public void page() {
		render("/admin/task/product/list.jsp");
	}
	
	@Before({WorkFlowInterceptor.class, Tx.class}) //添加事务
	public void task() {
		boolean isok = false;
		String taskId = getPara("taskId");
		System.out.println(getParams());
		//获取操作用户
		Record user = getSessionAttr(Constant.CURRENT_USER);
		String operator = user.getStr("login_name");
		//表单数据保存数据库
		Record record = new Record();
		record.set("productName", getPara("productName"));
		record.set("createTime", System.currentTimeMillis());
		record.set("productSize", getPara("productSize"));
		record.set("actorId", getPara("actorId"));
		record.set("creator", operator);
		if(taskId != null){ //执行当前任务
			executeTask(taskId, operator, getParams());
			int code = Db.update("update test_product set status=? where id=?", new Object[]{getParaToInt("status"), getPara("id")});
			isok = (code > 0);
		} else {			//创建实例
			record.set("status", 0);
			isok = Db.save(getTable(), record);
			if(isok){
				//启动任务
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("operator",  operator);
				params.put("id", record.getLong("id"));
				params.put("actorId", getPara("actorId"));
				params.put("processId",  processId);
				Order order = startAndExecute(processId, params);
				record.set("orderId", order.getId());
				isok = Db.update(getTable(), record);
			}
		}
		if(isok)
			renderJson(wrapJson(0,""));
		else
			renderJson(wrapJson(-1,"商品创建失败!"));
	}
	
	public void all(){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("orderId", getPara(0, null));
		List<FastWork> fastWorks = query(HSITFASEWORKESQL, FastWork.class, null, params, "createTime", "asc");
		renderJson(fastWorks);
	}
	
	@Override
	@Before(Tx.class) //添加事务
	public void form() {
		//获取哪种表单
		String form = getPara(0, null);
		
		//获取商品ID查看商品信息
		String produceId = getPara(1, null);
		Record record = null;
		if(produceId != null)
			record = Db.findById(getTable(), produceId);
		
		if(form.equalsIgnoreCase("order")){		//展示商品表单
			if(record != null){
				setAttr("product", record);
				setAttr("isread", true);
			}
			//获取工作流的ID
			Map<String,Object> params = new HashMap<String, Object>();
			params.put("instanceUrl", "/admin/task/product/form/order");
			List<Record> users = Db.find("SELECT sys_user.id id, sys_user.login_name username FROM sys_user left join sys_user_role on sys_user_role.userid=sys_user.id where sys_user_role.roleid=?", new Object[]{10});
			setAttr("users", users);
			render("/admin/task/product/order.jsp");
		}else if(form.equalsIgnoreCase("approve")){	//展示审批表单
			String taskId = getPara(2, null);
			setAttr("id", produceId);
			setAttr("taskId", taskId);
			if(record != null){
				if(record.getInt("status") != 0){
					setAttr("product", record);
					setAttr("isread", true);
				}
			}
			render("/admin/task/product/approve.jsp");
		}
		
	}
}
