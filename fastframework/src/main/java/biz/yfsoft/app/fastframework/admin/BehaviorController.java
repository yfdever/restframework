package biz.yfsoft.app.fastframework.admin;


public class BehaviorController extends AdminController {
	
	@Override
	protected String getTable() {
		return "sys_behavior";
	}


	public void index() {
		render("/admin/system/behavior.jsp");
	}
	
	@Override
	protected String getSearchFields(String searchs) {
		if(isSqlInject(searchs)){
			return " 1 = 2 ";
		}
		
		if(searchs == null){
			return " 1 = 1 ";
		}
		return " name like '%{key}%' or title like '%{key}%' or webhook like '%{key}%' ".replace("{key}", searchs);
	}
	
}
