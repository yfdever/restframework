package biz.yfsoft.app.fastframework.plugin.shiro;

import javax.sql.DataSource;

import biz.yfsoft.app.fastframework.plugin.BaseFastPlugin;

public class ShiroPlugin extends BaseFastPlugin {

	DataSource _ds ;
	public ShiroPlugin(DataSource ds){
		_ds = ds;
	}
	
	@Override
	public boolean start() {
		status = Status.ACTIVED;
		return true;
	}

}
