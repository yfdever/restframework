package biz.yfsoft.app.fastframework.bo;

import java.util.Map;

import com.jfinal.plugin.activerecord.Record;

public class MetaObject extends Record{
	
	private static final long serialVersionUID = 1039791052455989472L;

//	public static final MetaObject dao = new MetaObject();

	private static final String CREATEAT = "createAt";
	public static final String ID = "id";
	public static final String UPDATEAT = "updateAt";
	private static final String STATUS = "status";
	
	public int getId() {
		Object o = this.get(ID);
		return Integer.getInteger(o.toString());
	}

	public MetaObject setId(int id) {
		this.set(ID,id);
		return this;
	}

	public long getCreateAt() {
		Object o = this.get(CREATEAT);
		return Long.getLong(o.toString());
	}

	public MetaObject setCreateAt(long createat) {
		this.set(CREATEAT,createat);
		return this;
	}

	public long getUpdateAt() {
		Object o = this.get(UPDATEAT);
		return Long.getLong(o.toString());
	}

	public MetaObject setUpdateAt(long updateAt) {
		this.set(UPDATEAT,updateAt);
		return this;
	}

	public int getStatus() {
		Object o = this.get(STATUS);
		return Integer.getInteger(o.toString());
	}

	public MetaObject setStatus(int status) {
		this.set(STATUS,status);
		return this;
	}
	
	public MetaObject create(Map<String,Object> args){
		this.setColumns(args);
		this.setStatus(1);
		long now = System.currentTimeMillis();
		this.setCreateAt(now);
		this.setUpdateAt(now);
		return this;
	}
	public MetaObject create(){
		this.setStatus(1);
		long now = System.currentTimeMillis();
		this.setCreateAt(now);
		this.setUpdateAt(now);
		return this;
	}

	@Override
	public String toString() {
		return getColumns().toString();
	}
	
	
}
