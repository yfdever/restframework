package biz.yfsoft.app.fastframework.bo;

import java.util.Map;

import com.jfinal.plugin.activerecord.Record;

public class Bo extends Record{

	private static final long serialVersionUID = 8575578491128991292L;
	
//	public static final Bo dao = new Bo();

	private static final String CREATE_TIME = "createtime";
	public static final String ID = "id";
	public static final String UPDATE_TIME = "updatetime";
	private static final String STATUS = "status";
	
	public int getId() {
		Object o = this.get(ID);
		return Integer.getInteger(o.toString());
	}

	public Bo setId(int id) {
		this.set(ID,id);
		return this;
	}

	public long getCreatetime() {
		Object o = this.get(CREATE_TIME);
		return Long.getLong(o.toString());
	}

	public Bo setCreatetime(long createtime) {
		this.set(CREATE_TIME,createtime);
		return this;
	}

	public long getUpdatetime() {
		Object o = this.get(UPDATE_TIME);
		return Long.getLong(o.toString());
	}

	public Bo setUpdatetime(long updatetime) {
		this.set(UPDATE_TIME,updatetime);
		return this;
	}

	public int getStatus() {
		Object o = this.get(STATUS);
		return Integer.getInteger(o.toString());
	}

	public Bo setStatus(int status) {
		this.set(STATUS,status);
		return this;
	}
	
	public Bo create(Map<String,Object> args){
		this.setColumns(args);
		this.setStatus(1);
		long now = System.currentTimeMillis();
		this.setCreatetime(now);
		this.setUpdatetime(now);
		return this;
	}
	public Bo create(){
		this.setStatus(1);
		long now = System.currentTimeMillis();
		this.setCreatetime(now);
		this.setUpdatetime(now);
		return this;
	}

	@Override
	public String toString() {
		return getColumns().toString();
	}
	
	
}
