package biz.yfsoft.app.fastframework.snaker;

import java.io.Serializable;
import java.util.Collections;
import java.util.Map;

import org.snaker.engine.helper.JsonHelper;

/**
 * 实体与任务
 *
 * @author yanhao
 */
public class FastWork implements Serializable {

	private static final long serialVersionUID = -4550750452844802633L;

	//任务ID
	private String taskId;
	
	//工作流ID
	private String processId;
	
	//实例ID
	private String orderId;
	
	//父级任务ID
	private String parentTaskId;
	
	//任务名称
	private String taskName;
	
	//显示名称
	private String displayName;
	
	//实例创建者
	private String creator;
	
	//参与者
	private String actorId;
	
	//任务的表单
	private String actionUrl;
	
	//任务的创建时间
	private String createTime;

	//任务表单信息
	private String variable;
	
	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getParentTaskId() {
		return parentTaskId;
	}

	public void setParentTaskId(String parentTaskId) {
		this.parentTaskId = parentTaskId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getActorId() {
		return actorId;
	}

	public void setActorId(String actorId) {
		this.actorId = actorId;
	}

	public String getActionUrl() {
		return actionUrl;
	}

	public void setActionUrl(String actionUrl) {
		this.actionUrl = actionUrl;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getVariable() {
		return variable;
	}

	public void setVariable(String variable) {
		this.variable = variable;
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> getVariableMap() {
        Map<String, Object> map = JsonHelper.fromJson(this.variable, Map.class);
        if(map == null) return Collections.emptyMap();
        return map;
	}
}
