package biz.yfsoft.app.fastframework.interceptor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import biz.yfsoft.app.fastframework.snaker.FastProcess;
import biz.yfsoft.app.fastframework.snaker.WorkFlowController;

import com.jfinal.aop.Interceptor;
import com.jfinal.core.ActionInvocation;
import com.jfinal.core.Controller;

/**
 * 工作流拦截器
 *
 * @author yanhao
 */
public class WorkFlowInterceptor implements Interceptor {

	@Override
	public void intercept(ActionInvocation ai) {
		
		Controller controller = ai.getController();
		
		if(controller instanceof WorkFlowController){
			WorkFlowController wfc = (WorkFlowController) controller;
			//获取工作流的ID
			Map<String,Object> params = new HashMap<String, Object>();
			System.out.println(ai.getActionKey());
			params.put("instanceUrl", ai.getActionKey());
			List<FastProcess> fastProcess = wfc.query(wfc.PROCESSSQLID, FastProcess.class, params);
			if(fastProcess != null && fastProcess.size() > 0){
				FastProcess process = fastProcess.get(0);
				wfc.setProcessId(process.getId());
			}
		}
		
		ai.invoke();
		
	}
	
}
