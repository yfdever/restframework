package biz.yfsoft.app.fastframework.kit;

import java.util.Properties;

public class RuntimeKit {
	
	public enum OSType{
		WIN,MAC,LINUX
	}
	
	//系统配置信息
	private static Properties props = null;
	
	//获取当前的操作系统类型
	public static OSType currentOS = null;
	
	public static String tempDir = null;
	
	static{
		props = System.getProperties();
		currentOS = getOSType();
		tempDir = tempDir();
	}
		
	private static OSType getOSType(){
		String deviceType = deviceType();
		if("Linux".equals(deviceType))
			return OSType.LINUX;
		if(deviceType.startsWith("Mac")){
			return OSType.MAC;
		}
		return OSType.WIN;
	}

	//执行操作系统的控制台命令
	public static Process exec(String cmd) throws Exception{
		Runtime rt = Runtime.getRuntime();
		//非window平台
		if(currentOS!=OSType.WIN){
			return rt.exec(new String[]{"sh","-c",cmd});
		}else{
			//window平台
			return rt.exec("cmd /c " + cmd);
		}
	}
	
	private static String deviceType(){
		return props.getProperty("os.name");
	}
	
	private static String tempDir(){
		return props.getProperty("java.io.tmpdir");
	}
	
}
