package biz.yfsoft.app.fastframework.config;

import java.text.MessageFormat;

import biz.yfsoft.app.fastframework.config.route.AdminRoutes;
import biz.yfsoft.app.fastframework.config.route.RestV01Routes;
import biz.yfsoft.app.fastframework.config.route.UserRoutes;
import biz.yfsoft.app.fastframework.config.route.OrderRoutes;
import biz.yfsoft.app.fastframework.config.route.ThemeRoutes;
import biz.yfsoft.app.fastframework.core.Fast;
import biz.yfsoft.app.fastframework.interceptor.AdminIntercept;
import biz.yfsoft.app.fastframework.kit.DataKit;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import com.jfinal.render.ViewType;

public class Config extends JFinalConfig {
	
	/**
	 * 配置常量
	 */
	public void configConstant(Constants me) {
		me.setViewType(ViewType.JSP);
		// 加载少量必要配置，随后可用getProperty(...)获取值
		loadPropertyFile("config.txt");
		me.setBaseViewPath(getProperty("baseViewPath","/"));
		boolean devMode = getPropertyToBoolean("devMode", false);
		me.setDevMode(devMode);
		if(!devMode){
			//正式部署
			//loadPropertyFile("deploy.txt");
		}
	}
	
	/**
	 * 配置路由
	 */
	public void configRoute(Routes me) {
		me.add(new ThemeRoutes());
		me.add(new RestV01Routes());
		me.add(new AdminRoutes());
		me.add(new UserRoutes());
		me.add(new OrderRoutes());
	}
	
	/**
	 * 配置插件
	 */
	public void configPlugin(Plugins me) {
		//临时的配置变量
		JSONObject config = null;
		// 配置C3p0数据库连接池插件
		config = DataKit.getMysqlConfig();
		
		String user = config.getString("user");
		String pass = config.getString("pass");
		String host = config.getString("host");
		String port = config.getString("port");
		String name = config.getString("name");
		String url = config.getString("url");
		
		url = MessageFormat.format(url, host,port,name);
		
		//创建c3p0的插件
		
		//添加基本的系统数据源
		C3p0Plugin c3p0Plugin = new C3p0Plugin(url, user, pass.trim());
		me.add(c3p0Plugin);
		
		//创建操作数据库的插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
		arp.setShowSql(true);
		me.add(arp);
		
		
		//添加业务数据库的数据源
		// 配置C3p0数据库连接池插件
		config = DataKit.getBizMysqlConfig();
		
		user = config.getString("user");
		pass = config.getString("pass");
		host = config.getString("host");
		port = config.getString("port");
		name = config.getString("name");
		url = config.getString("url");
		
		url = MessageFormat.format(url, host,port,name);
		C3p0Plugin c3p0Plugin2 = new C3p0Plugin(url, user, pass.trim());
		me.add(c3p0Plugin2);
		
		//创建操作数据库的插件
		ActiveRecordPlugin arp2 = new ActiveRecordPlugin("biz",c3p0Plugin2);
		arp2.setShowSql(true);
		me.add(arp2);
		
		
		//添加业务数据库的数据源
		// 配置C3p0数据库连接池插件
		config = DataKit.getEcMysqlConfig();
		
		user = config.getString("user");
		pass = config.getString("pass");
		host = config.getString("host");
		port = config.getString("port");
		name = config.getString("name");
		url = config.getString("url");
		
		url = MessageFormat.format(url, host,port,name);
		C3p0Plugin c3p0Plugin3 = new C3p0Plugin(url, user, pass.trim());
		me.add(c3p0Plugin3);
		
		//创建操作数据库的插件
		ActiveRecordPlugin arp3 = new ActiveRecordPlugin("ec",c3p0Plugin3);
		arp3.setShowSql(true);
		me.add(arp3);
		
		
		//缓存插件
		me.add(new EhCachePlugin());
		
		// 配置Snaker插件
//	    SnakerPlugin snakerPlugin = new SnakerPlugin(c3p0Plugin);
//	    me.add(snakerPlugin);
		
		//剩余的插件需要在Fast.init中进行启动，通过后台设置的启用状态进行插件的启动
	}
	
	/**
	 * 配置全局拦截器
	 */
	public void configInterceptor(Interceptors me) {
		me.add(new AdminIntercept());
	}
	
	
	@Override
	public void afterJFinalStart() {
		super.afterJFinalStart();
		Fast.AynTask(new Runnable(){

			@Override
			public void run() {
				Fast.init();
			}
			
		});
	}

	/**
	 * 配置处理器
	 */
	public void configHandler(Handlers me) {
		
	}
	
}