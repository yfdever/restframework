package biz.yfsoft.app.fastframework.config.route;

import biz.yfsoft.app.fastframework.admin.ApiController;
import biz.yfsoft.app.fastframework.admin.AttachmentController;
import biz.yfsoft.app.fastframework.admin.BehaviorController;
import biz.yfsoft.app.fastframework.admin.DataController;
import biz.yfsoft.app.fastframework.admin.DocumentController;
import biz.yfsoft.app.fastframework.admin.IndexController;
import biz.yfsoft.app.fastframework.admin.LogController;
import biz.yfsoft.app.fastframework.admin.MessageController;
import biz.yfsoft.app.fastframework.admin.ModelController;
import biz.yfsoft.app.fastframework.admin.NotifyController;
import biz.yfsoft.app.fastframework.admin.PermissionController;
import biz.yfsoft.app.fastframework.admin.PluginController;
import biz.yfsoft.app.fastframework.admin.SettingController;
import biz.yfsoft.app.fastframework.admin.SystemController;
import biz.yfsoft.app.fastframework.admin.UserController;
import biz.yfsoft.app.fastframework.admin.UserRoleController;
import biz.yfsoft.app.fastframework.snaker.ProcessController;
import biz.yfsoft.app.fastframework.test.JudgeController;
import biz.yfsoft.app.fastframework.test.ProductController;

import com.jfinal.config.Routes;

public class AdminRoutes extends Routes {

	@Override
	public void config() {
		add("/admin", IndexController.class,"admin");
		add("/admin/setting", SettingController.class,"admin");
		add("/admin/system", SystemController.class,"admin");
		add("/admin/system/behavior", BehaviorController.class,"admin");
		add("/admin/data", DataController.class,"admin");
		add("/admin/notify", NotifyController.class,"admin");
		add("/admin/api", ApiController.class,"admin");
		add("/admin/log", LogController.class,"admin");
		add("/admin/document", DocumentController.class,"admin");
		add("/admin/model", ModelController.class,"admin");
		add("/admin/user", UserController.class,"admin");
		add("/admin/permission", PermissionController.class,"admin");
		add("/admin/user/role", UserRoleController.class,"admin");
		add("/admin/process", ProcessController.class,"admin");
		add("/admin/judge", JudgeController.class,"admin");
		add("/admin/product", ProductController.class,"admin");
		add("/admin/message", MessageController.class,"admin");
		add("/admin/attachment", AttachmentController.class,"admin");
		add("/admin/plugin", PluginController.class,"admin");
	}

}
