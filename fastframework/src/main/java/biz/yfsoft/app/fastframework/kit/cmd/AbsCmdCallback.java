package biz.yfsoft.app.fastframework.kit.cmd;

import java.io.File;

import biz.yfsoft.app.fastframework.bo.Bo;
import biz.yfsoft.app.fastframework.core.Fast;
import biz.yfsoft.app.fastframework.core.document.Document;
import biz.yfsoft.app.fastframework.core.document.DocumentSaveHex;

public abstract class AbsCmdCallback implements CmdCallback {

	protected Bo fRecord;
	public AbsCmdCallback(Bo fRecord){
		this.fRecord = fRecord;
	}
	@Override
	public void success() {
		
    	Fast.AynTask(new Runnable(){
			@Override
			public void run() {
				final String path = AbsCmdCallback.this.fRecord.getStr("path");
				final String name = AbsCmdCallback.this.fRecord.getStr("name");
				File f = new File(path);
				//文件不存在，则跳出
				if(!f.exists()){
					return;
				}
				try {
					Document.me().save(Document.folder+"/"+name, fRecord);
				} catch (DocumentSaveHex e) {
					e.printStackTrace();
				}
			}
    		
    	},20*1000L);//20s 之后执行
	}

}
