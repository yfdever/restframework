package biz.yfsoft.app.fastframework.plugin.qiniu;

import biz.yfsoft.app.fastframework.core.document.Document;
import biz.yfsoft.app.fastframework.core.document.LocalDocument;
import biz.yfsoft.app.fastframework.plugin.BaseFastPlugin;

import com.qiniu.storage.UploadManager;
import com.qiniu.util.Auth;

public class QiniuPlugin extends BaseFastPlugin {
	
	/*~~~~~~基本的常量设置~~~~~~~
	 参数说明： 
	 BUCKET_NAME:七牛云存储上分配的空间目录名
	 ACCESS_KEY：云存储调用的访问密钥
	 SECRET_KEY：密码Key
	 DOMAIN：用于访问文件的主机域名
	{"DOMAIN":"http://yfdocument.qiniudn.com","BUCKET_NAME":"yfdocument","ACCESS_KEY":"65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81","SECRET_KEY":"kZxy-i93_B98yg4lNn7XmSujeZh_JWRxQOJX3E_m"}
	 ~~~~~~~~~~~~~~~~~~~~~~~~~*/
	
	public static String DOMAIN = "http://yfdocument.qiniudn.com";
    // 请确保该bucket已经存在
	public static String BUCKET_NAME = "yfdocument";
	private static String ACCESS_KEY = "65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81";
	private static String SECRET_KEY = "kZxy-i93_B98yg4lNn7XmSujeZh_JWRxQOJX3E_m";
	
	// 重用 uploadManager。一般地，只需要创建一个 uploadManager 对象
	public static UploadManager uploadManager = null;
	
	private static Auth auth = null;

	@Override
	public boolean start() {
		
		uploadManager = new UploadManager();
		if(configJson.containsKey("ACCESS_KEY")){
			ACCESS_KEY = configJson.getString("ACCESS_KEY");
		}
		if(configJson.containsKey("SECRET_KEY")){
			SECRET_KEY = configJson.getString("SECRET_KEY");
		}
		if(configJson.containsKey("BUCKET_NAME")){
			BUCKET_NAME = configJson.getString("BUCKET_NAME");
		}
		if(configJson.containsKey("DOMAIN")){
			DOMAIN = configJson.getString("DOMAIN");
		}
		auth = Auth.create(ACCESS_KEY, SECRET_KEY);
		//调用Document进行文档接口的创建
		Document.create(QiniuDocument.class);
		
		status = Status.ACTIVED;
		return true;
	}
	
	
	
	@Override
	public boolean stop() {
		if(!super.stop())
			return false;
		Document.create(LocalDocument.class);
		return true;
	}



	public static String getUpToken(){
		return auth.uploadToken(BUCKET_NAME);
	}


}
