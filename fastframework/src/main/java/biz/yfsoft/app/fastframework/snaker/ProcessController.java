package biz.yfsoft.app.fastframework.snaker;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.lang.StringUtils;
import org.snaker.engine.access.Page;
import org.snaker.engine.access.QueryFilter;
import org.snaker.engine.entity.Process;
import org.snaker.engine.helper.AssertHelper;
import org.snaker.engine.helper.StreamHelper;
import org.snaker.engine.helper.StringHelper;
import org.snaker.engine.model.ProcessModel;

import biz.yfsoft.app.fastframework.kit.SnakerKit;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.upload.UploadFile;

/**
 * 工作流 Controller
 *
 * @author yanhao
 */
public class ProcessController extends WorkFlowController {

	public void list(){
		render("/admin/snaker/process/list.jsp");
	}
	
	public void save(){
		String processId = getPara(0,null);
//		String processId = getPara(PARA_PROCESSID);
		System.out.println(processId + " ======");
		if(processId != null)
			setAttr("processId", processId);
		render("/admin/snaker/process/save.jsp");
	}
	
	public void designer(){
		String processId = getPara(0,null);
		System.out.println(processId + " ======");
//		String processId = getPara(PARA_PROCESSID);
		if(StringUtils.isNotEmpty(processId)) {
			Process process = engine.process().getProcessById(processId);
			AssertHelper.notNull(process);
			ProcessModel processModel = process.getModel();
			if(processModel != null) {
				String json = SnakerKit.getModelJson(processModel);
				setAttr("process", json);
			}
			setAttr("processId", processId);
		}
		render("/admin/snaker/process/designer.jsp");
	}
	
	public void ajax(){
		QueryFilter filter = new QueryFilter();
		filter.setOrderBy(getPara("sort"));
		filter.setOrder(getPara("order"));
		String displayName = getPara("displayName");
		if(StringHelper.isNotEmpty(displayName)) {
			filter.setDisplayName(displayName);
		}
		Page<Process> page = new Page<Process>();
		page.setPageNo(getParaToInt("offset", 1));
		page.setPageSize(getParaToInt("limit", 1));
		this.engine.process().getProcesss(page, filter);
		JSONObject rst = new JSONObject();
		rst.put("total", page.getTotalCount());
		rst.put("rows", page.getResult());
		renderJson(rst);
	}
	
	/**
	 * 添加流程定义后的部署
	 */
	public void doFileDeploy() {
		InputStream input = null;
		try {
			String id = getPara("id");
			UploadFile file = getFile("snakerFile");
			input = new FileInputStream(file.getFile());
			if(StringUtils.isNotEmpty(id)) {
				engine.process().redeploy(id, input);
			} else {
				engine.process().deploy(input);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		redirect("/snaker/process");
	}
	
	/**
	 * 保存流程定义[web流程设计器]
	 * @param model
	 * @return
	 */
	public void doStringDeploy() {
		InputStream input = null;
		try {
			String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" + SnakerKit.convertXml(getPara("model"));
			System.out.println("model xml=\n" + xml);
			String id = getPara("id");
			input = StreamHelper.getStreamFromString(xml);
			if(StringUtils.isNotEmpty(id)) {
				engine.process().redeploy(id, input);
			} else {
				engine.process().deploy(input);
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(false);
		} finally {
			if(input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		renderJson(true);
	}

	@Override
	public void form() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void page() {
		// TODO Auto-generated method stub
		
	}
}
