package biz.yfsoft.app.fastframework.config.route;

import biz.yfsoft.app.fastframework.api.ShopController;
import biz.yfsoft.app.fastframework.api.UserController;

import com.jfinal.config.Routes;

public class UserRoutes extends Routes {

	@Override
	public void config() {
		add("/user", UserController.class);
		//add("/rest/test", MyCloudController.class);
	}

}
