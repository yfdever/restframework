package biz.yfsoft.app.fastframework.interceptor;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import com.jfinal.aop.Interceptor;
import com.jfinal.core.ActionInvocation;

public class AdminSessionInterceptor implements Interceptor {

	
	
	public void intercept(ActionInvocation ai) {
		Subject currentUser = SecurityUtils.getSubject();
		
		if (!currentUser.isAuthenticated()) {
			ai.getController().redirect("/admin/login");
		}else{
			ai.invoke();
		}
	}

}
