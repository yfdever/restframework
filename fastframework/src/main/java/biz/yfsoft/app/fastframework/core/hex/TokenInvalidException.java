package biz.yfsoft.app.fastframework.core.hex;

public class TokenInvalidException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1472608606843097147L;

	public String getMessage(){
		return "该Token已失效";
	}
}
