package biz.yfsoft.app.fastframework.admin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;


public class UserRoleController extends AdminController {
	
	@Override
	protected String getTable() {
		return "sys_role";
	}
	
	public void index() {
		render("/admin/users/role.jsp");
	}
	
	@Override
	protected String getSearchFields(String searchs) {
		if(isSqlInject(searchs)){
			return " 1 = 2 ";
		}
		
		if(searchs == null){
			return " 1 = 1 ";
		}
		return " name like '%{key}%' or title like '%{key}%'".replace("{key}", searchs);
	}
	
	public void security() {
		
		//renderText(getPara(0,"-1"));
		String roleid = getPara(0,null);
		if(StrKit.isBlank(roleid)){
			renderText("角色不能为空");
			return;
		}
		//加载权限体系
		List<Record> list = Db.find("select * from sys_permission where status > 0");
		Map<String,List<Record>> group = group(list);
		System.out.println(group);
		this.setAttr("permissionList", group);
		this.setAttr("roleid",roleid);
		render("/admin/users/security.jsp");
	}
	
	public Map<String,List<Record>> group(List<Record> list){
		Map<String,List<Record>> map = new HashMap<String,List<Record>>();
		List<Record> l;
		for(Record r:list){
			String scope = r.getStr("scope");
			
			if(map.containsKey(scope)){
				l = map.get(scope);
			}else{
				l = new ArrayList<Record>();
			}
			l.add(r);
			map.put(scope, l);
		}
		return  map;
	}
}
