package biz.yfsoft.app.fastframework.plugin.sms;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import biz.yfsoft.app.fastframework.kit.RestKit;

import com.alibaba.fastjson.JSONObject;

public class Sms {

//	public static boolean send(String phoneNumber,String templateId,Map<String,String> args){
//		StringBuffer tpl_value = new StringBuffer();
//		if(null!= args){
//			for(String k : args.keySet()){
//				tpl_value.append("#").append(k).append("#=").append(args.get(k)).append("&");
//			}
//			if(args.size()>0){
//				tpl_value.deleteCharAt(tpl_value.length()-1);
//			}
//		}
//		
//		String url;
//		try {
//			url = "http://v.juhe.cn/sms/send?mobile="+phoneNumber+"&tpl_id="+templateId+"&tpl_value="+URLEncoder.encode(tpl_value.toString(),"utf-8")+"&key=d8dc71c0b693596e5450559c7f47fe7d&dtype=json";
//			System.out.println(url);
//			String result = HttpKit.get(url);
//			System.out.println(result);
//			return true;
//		} catch (UnsupportedEncodingException e) {
//		}
//		return false;
//	}
	
	public static boolean send(String phoneNumber,String templateId,Map<String,String> args){
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("X-AVOSCloud-Application-Id", "2lco5zd9yrym993r0li8dhipksuizp2miv9c4i8nedadfhqk");
		headers.put("X-AVOSCloud-Application-Key", "o9w9674ouz01usc8lmlophb6cgdjsaki2pgvc409yjlqp1ra");
		headers.put("Content-Type", "application/json");
		JSONObject jo = new JSONObject();
		jo.put("mobilePhoneNumber", phoneNumber);
		jo.put("template", templateId);
		if(args != null)
			jo.putAll(args);
		try {
			System.out.println(RestKit.connection("https://api.leancloud.cn/1.1/requestSmsCode", "POST", headers, jo.toJSONString()));
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public static void main(String[] arg){
		Map<String,String> args = new HashMap<String,String>();
		args.put("msg", "您申请的资金已经通过审核!");
		Sms.send("15151805826","CommMsg",args);
	}
	
}
