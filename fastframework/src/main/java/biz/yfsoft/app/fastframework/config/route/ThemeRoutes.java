package biz.yfsoft.app.fastframework.config.route;

import biz.yfsoft.app.fastframework.user.UserController;

import com.jfinal.config.Routes;

public class ThemeRoutes extends Routes {

	@Override
	public void config() {
		add("/", UserController.class);
	}

}
