package biz.yfsoft.app.fastframework.api;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import biz.yfsoft.app.fastframework.api.m.TaskM;
import biz.yfsoft.app.fastframework.core.Fast;
import biz.yfsoft.app.fastframework.core.document.Document;
import biz.yfsoft.app.fastframework.kit.FileKit;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.aop.ClearInterceptor;
import com.jfinal.core.Controller;

/**
 * 关于门店使用到的较为复杂的业务api
 * @author yfsoft
 *
 */
@ClearInterceptor
public class ShopController extends Controller{
	
	
	public void foo(){
		Map<String, Object> m = new HashMap<String,Object>();
		m.put("aaa", "test");
		renderJson(m);
	}
	
	/**
	 * 同步门店详细数据的接口
	 * 1.获取七牛的 key
	 * 2.创建一个新的task	任务状态置为 QUEUEING
	 * 3.获取远程的文件	任务状态置为 DOWNLOADING
	 * 4.解压数据文件		任务状态置为 UNZIPING
	 * 5.加载到内存		任务状态置为 LOADING
	 * 6.同步到数据库中	任务状态置为 SYNCING
	 * 7.任务状态置为 FINISHED
	 *	//测试的 key ：  FilC9uR6y80iIOOOTqD8oCKTRGtP
	 */
	public void syncData(){
		//1.
		String key = getPara("key");
		Map<String, Object> m = new HashMap<String,Object>();
		
		final TaskM t = TaskM.create("SHOP_SYNC_DATA",key,"QINIU");
		if(t == null){
			m.put("errno", -20);
			m.put("code", "TASK CREATE ERROR!");
			renderJson(m);
			return;
		}
		m.put("errno", 0);
		m.put("taskId", t.get("id"));
		renderJson(m);
		
		Fast.AynTask(new Runnable(){

			@Override
			public void run() {
				
				byte[] data = null;		//数据文件的二进制数据数组
				JSONObject jsonData1 = null;//解析出的数据之一
				JSONArray jsonData2 = null;//解析出的数据之二
				String tempFilePath = null;//缓存文件的存放目录
				String json1 = "data1.json",json2 = "data2.json";
				
				try {
					data = Document.me().loadToMemory(key);
				} catch (IOException e) {
					t.error("文件下载失败！"+e.getMessage());
					return;
				}
				
				//创建缓存文件，保存该临时文件
				File f = FileKit.createTempFile("zip");
				if(f!=null){
					FileKit.writeFile(f, data);
					//程序退出时自动删除
					f.deleteOnExit();
				}else{
					t.error("创建临时文件失败！查看一下权限～");
					return;
				}
				
				tempFilePath = f.getAbsolutePath().replace(".zip", "");
				
				//解压数据文件
				FileKit.unZip(f, new File(tempFilePath));
				
				//读取json数据文件
				json1 = tempFilePath + File.separator + json1;
				json2 = tempFilePath + File.separator + json2;
				
				//将json文件转换成数据
				json1 = FileKit.readFile(json1);
				json2 = FileKit.readFile(json2);
				
				jsonData1 = JSONObject.parseObject(json1);
				jsonData2 = JSONArray.parseArray(json2);
				
				//TODO:将数据保存到db
				System.out.println(jsonData1);
				System.out.println(jsonData2);
				//任务结束
				t.update(TaskM.FINISHED, null);
			}
			
		});
		
		
	}
}
