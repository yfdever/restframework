package biz.yfsoft.app.fastframework.plugin.mongodb;

import java.util.HashMap;
import java.util.Map;

public class BoParam {

	private Map<String,Object> _attr = new HashMap<String,Object>();
	
	public BoParam add(String key,Object v){
		_attr.put(key, v);
		return this;
	}
	
	public Map<String,Object> getData(){
		return _attr;
	}
}
