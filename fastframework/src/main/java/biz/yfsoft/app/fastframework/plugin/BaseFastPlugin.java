package biz.yfsoft.app.fastframework.plugin;

import java.util.Properties;

import com.alibaba.fastjson.JSONObject;

public abstract class BaseFastPlugin implements IFastPlugin {

	protected JSONObject configJson = new JSONObject();
	
	protected Properties properties;
	
	protected Status status = Status.NOTSTART;
	
	@Override
	public boolean stop() {
		status = Status.STOP;
		return true;
	}

	@Override
	public void init(Properties properties) {
		this.properties = properties;
	}

	@Override
	public void init(JSONObject configJson) {
		this.configJson = configJson;
	}

	@Override
	public Status getStatus() {
		return status;
	}
	
	@Override
	public JSONObject getConfigJson(){
		return configJson;
	}
	
	@Override
	public Properties getProperties(){
		return properties;
	}
}
