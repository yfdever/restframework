package biz.yfsoft.app.fastframework.admin;


import java.text.MessageFormat;
import java.util.Map;

import biz.yfsoft.app.fastframework.bo.Bo;
import biz.yfsoft.app.fastframework.core.BaseFastController;
import biz.yfsoft.app.fastframework.interceptor.AdminSessionInterceptor;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.aop.Before;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.ActiveRecordException;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

/**
 * 用于后台管理的控制器的抽象
 * @author zt
 *
 */
@Before(AdminSessionInterceptor.class)
public abstract class AdminController extends BaseFastController {
	
	
	protected String getTable(){
		return "";
	}
	
	protected String getSearchFields(String searchs){
		if(StrKit.isBlank(searchs)){
			return " 1 = 1 ";
		}
		return "";
	}
	
	protected boolean isSqlInject(String search){
		//做sql注入的防范
		//1. 不允许有。。
		return false;
	}
	
	protected String getField(){
		return "*";
	}
	protected String getCondition(){
		return "status > 0";
	}
	
	
	/**
	 * 添加实体
	 * 需遵守关系型数据库添加实体的规则，如：不允许出现重复的主键，不可忽略非空字段等
	 * 返回json
	 */
	public void add(){
		Map<String,Object> args = prevAdd(getParams());
		Bo bo = new Bo().create(args);
		try{
			boolean flag = Db.save(getTable(), bo);
			if(flag){
				long id = bo.get(Bo.ID);
				renderJson(wrapJson(0,"",id));
			}else{
				renderJson(wrapJson(-1,"操作失败!"));
			}
		}catch(ActiveRecordException hex){
			renderJson(wrapJson(-2,"操作失败!可能原因如下：\n"+hex.getMessage()));
		}
	}
	
	/**
	 * 通过ajax获取列表
	 * 通过是进行分页展示
	 * 需要传递如下参数：
	 * limit 每页显示的条数
	 * offset 从0开始偏移
	 * sort 获取排序的列
	 * order 升降序
	 * 前台与 bootstrap table 结合起来使用
	 */
	public void ajaxList() {
		int limit = getParaToInt("limit",10);//每页显示的条数
		int offset = getParaToInt("offset",0);//从0开始偏移
		String sort = getPara("sort");//获取排序的列
		String order = getPara("order");//升降序
		String orderby = "";//排序的sql
		String search = getPara("search");
		//使用到了搜索功能
		search = getSearchFields(search);
		if(StrKit.notNull(sort)){
			//需要进行排序
			orderby = MessageFormat.format("order by {0} {1}",sort,order);
		}
		int page = offset/limit+1;
		Page<Record> pageRecord = Db.paginate(page, limit, "select "+getField(), MessageFormat.format("from {0} where {1} {2}", getTable(),getCondition()+" and ("+search+ " )",orderby));
		JSONObject rst = new JSONObject();
		rst.put("total", pageRecord.getTotalRow());
		rst.put("rows", pageRecord.getList());
		renderJson(rst);
	}
	
	/**
	 * 删除实体的信息
	 * 通过url传递名称为id[]的参数
	 * 以英文逗号隔开
	 * 通过更新 status＝ －1 来进行删除状态的标记
	 * 
	 */
	public void del(){
		String ids = getPara("id[]");
		int rows = Db.update("update "+ getTable() +" set status= -1 and updatetime = ? where id in ("+ ids+")",System.currentTimeMillis());
		if(rows>0){
			renderJson(wrapJson(0,"",rows));
			invokeResult = true;
		}else{
			renderJson(wrapJson(-1,"删除失败!"));
			invokeResult = false;
		}
	}
	
	
	/**
	 * 修改实体的信息
	 * 必须通过url传递名称为id的参数
	 * 更新时间会自动修改
	 * 
	 * 返回 json
	 */
	public void edit(){
		String id = getPara("id");
		Map<String,Object> args = getParams();
		args.put(Bo.UPDATE_TIME, System.currentTimeMillis());
		Record record = Db.findById(getTable(), id);
		record.setColumns(args);
		if(Db.update(getTable(), record)){
			renderJson(wrapJson(0,"",record));
		}else{
			renderJson(wrapJson(-1,"修改失败!"));
		}
	}
	
}

