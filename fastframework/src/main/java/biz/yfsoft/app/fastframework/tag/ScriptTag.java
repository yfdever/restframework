package biz.yfsoft.app.fastframework.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTag;
import javax.servlet.jsp.tagext.BodyTagSupport;
import javax.servlet.jsp.tagext.Tag;

public class ScriptTag extends BodyTagSupport  {
	private static final long serialVersionUID = -2589105425101166528L;

	//把标签体改为大写的  
    @Override  
    public int doStartTag() throws JspException {  
        return BodyTag.EVAL_BODY_BUFFERED;  
    }  
  
    @Override  
    public int doEndTag() throws JspException {  
          
        BodyContent bc=this.getBodyContent();  
        String content=bc.getString();  
        pageContext.setAttribute("script", content);
        return Tag.EVAL_PAGE;  
    }  
	
}
