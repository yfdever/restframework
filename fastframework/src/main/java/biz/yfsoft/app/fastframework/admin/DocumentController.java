package biz.yfsoft.app.fastframework.admin;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

import biz.yfsoft.app.fastframework.core.Fast;
import biz.yfsoft.app.fastframework.plugin.IFastPlugin;
import biz.yfsoft.app.fastframework.plugin.IFastPlugin.Status;
import biz.yfsoft.app.fastframework.plugin.icbc.IcbcPay;

import com.jfinal.aop.ClearInterceptor;
import com.jfinal.aop.ClearLayer;

public class DocumentController extends AdminController {
	
	private static String orderId = "";
	
	private static String tranTime = "";
	
	@ClearInterceptor(ClearLayer.ALL)
	public void index() {
		IFastPlugin plugin = Fast.me().getPlugin("IcbcPayPlugin");
		if(plugin.getStatus() == Status.ACTIVED){
			StringBuilder sb = new StringBuilder();
			
			sb.append("<a href='https://corporbank3.dccnet.com.cn/icbc/corporbank/logon.jsp' target='_blank'>进入测试系统账户中心</a><br/>");
			sb.append("<a href='pay' target='_blank'>支付一笔订单</a><br/>");
			sb.append(MessageFormat.format("<a href=\"delayOrder?orderId={0}&tranTime={1}\" target='_blank'>延迟订单</a><br/>",orderId,tranTime));
			sb.append(MessageFormat.format("<a href=\"aheadPay?orderId={0}&tranTime={1}\" target='_blank'>确认这笔订单</a><br/>",orderId,tranTime));
			sb.append(MessageFormat.format("<a href=\"applyRefund?orderId={0}&tranTime={1}\" target='_blank'>申请退款</a><br/>",orderId,tranTime));
			sb.append(MessageFormat.format("<a href=\"commitRefund?orderId={0}&tranTime={1}\" target='_blank'>确认退款</a><br/>",orderId,tranTime));
			sb.append(MessageFormat.format("<a href=\"rejectRefund?orderId={0}&tranTime={1}\" target='_blank'>驳回退款</a><br/>",orderId,tranTime));
			setAttr("formStr", sb.toString());
			render("/pay.jsp");
		}else{
			renderText("支付插件未启动，请联系管理员！");
		}
		//*/
	}
	
	@ClearInterceptor(ClearLayer.ALL)
	public void pay(){
		//需要工商银行的接口进行测试
		///*
		Map<String,Object> order = new HashMap<String,Object>();
		orderId = "O1441067270594";
		order.put("id", orderId);
		tranTime = "20150901083526";
		order.put("payTime", tranTime);
//		order.put("id", "O123123123");
//		order.put("payTime", "20150901211629");
		order.put("amount", "500");
		order.put("sellerAcct", "3400200219300283775");
		order.put("sellerName", "中达电力燃料有限公司");
		IcbcPay.me().pay(this,order);
		
	}
	
	@ClearInterceptor(ClearLayer.ALL)
	public void rejectRefund(){
		Map<String,Object> refund = new HashMap<String,Object>();
		refund.put("amount", "100"); //申请亏款的金额
		refund.put("msg", "babab"); //申请退款的原因
		
		String orderId = getPara("orderId");
		String tranTime = getPara("tranTime");
		
		String result = IcbcPay.me().rejectRefund(orderId,tranTime,refund);
		Map<String,String> data = IcbcPay.me().parseResult(result);
		renderText(data.toString());
	}
	
	@ClearInterceptor(ClearLayer.ALL)
	public void delayOrder(){
		String orderId = getPara("orderId");
		String tranTime = getPara("tranTime");
		
		String result = IcbcPay.me().delayOrder(orderId,tranTime);
		Map<String,String> data = IcbcPay.me().parseResult(result);
		renderText(data.toString());
	}
	
	@ClearInterceptor(ClearLayer.ALL)
	public void aheadPay(){
		String orderId = getPara("orderId");
		String tranTime = getPara("tranTime");
		IcbcPay.me().aheadPay(this,orderId,tranTime);
	}
	
	@ClearInterceptor(ClearLayer.ALL)
	public void applyRefund(){
		Map<String,Object> refund = new HashMap<String,Object>();
		refund.put("amount", "100"); //申请亏款的金额
		refund.put("msg", "babab"); //申请退款的原因
		
		String orderId = getPara("orderId");
		String tranTime = getPara("tranTime");
		
		String result = IcbcPay.me().applyRefund(orderId,tranTime,refund);
		Map<String,String> data = IcbcPay.me().parseResult(result);
		renderText(data.toString());
	}
	
	@ClearInterceptor(ClearLayer.ALL)
	public void commitRefund(){
		Map<String,Object> refund = new HashMap<String,Object>();
		refund.put("amount", "100"); //申请亏款的金额
		refund.put("msg", "babab"); //申请退款的原因
		
		String orderId = getPara("orderId");
		String tranTime = getPara("tranTime");
		
		String result = IcbcPay.me().commitRefund(orderId,tranTime,refund);
		Map<String,String> data = IcbcPay.me().parseResult(result);
		renderText(data.toString());
	}
}
