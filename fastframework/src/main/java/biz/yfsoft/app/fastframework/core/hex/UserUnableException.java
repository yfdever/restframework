package biz.yfsoft.app.fastframework.core.hex;

public class UserUnableException extends AuthErrorException {


	private static final long serialVersionUID = 7760085823060120897L;

	@Override
	public String getMessage(){
		return "用户已被冻结";
	}
}
