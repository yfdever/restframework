package biz.yfsoft.app.fastframework.config.route;

import biz.yfsoft.app.fastframework.api.ShopController;
import biz.yfsoft.app.fastframework.api.OrderController;

import com.jfinal.config.Routes;

public class OrderRoutes extends Routes {

	@Override
	public void config() {
		add("/order", OrderController.class);
		//add("/rest/test", MyCloudController.class);
	}

}
