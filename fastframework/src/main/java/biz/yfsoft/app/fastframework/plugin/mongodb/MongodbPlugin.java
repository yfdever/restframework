package biz.yfsoft.app.fastframework.plugin.mongodb;
import java.net.UnknownHostException;

import biz.yfsoft.app.fastframework.plugin.BaseFastPlugin;

import com.jfinal.log.Logger;
import com.mongodb.MongoClient;

public class MongodbPlugin extends BaseFastPlugin {

    protected final Logger logger = Logger.getLogger(getClass());

    private MongoClient client;
    private String host;
    private int port;
    private String database;

    @Override
    public boolean start() {
    	this.host = configJson.getString("host");
        this.port = Integer.parseInt(configJson.getString("port"));
        this.database = configJson.getString("name");
        try {
            client = new MongoClient(host, port);
        } catch (UnknownHostException e) {
            throw new RuntimeException("can't connect mongodb, please check the host and port:" + host + "," + port, e);
        }

        MongoKit.init(client, database);
        status = Status.ACTIVED;
        return true;
    }

    @Override
    public boolean stop() {
    	super.stop();
        if (client != null) {
            client.close();
        }
        return true;
    }

}
