package biz.yfsoft.app.fastframework.plugin.akka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import akka.actor.ActorSystem;
import biz.yfsoft.app.fastframework.plugin.BaseFastPlugin;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

/**
 * 该插件尚未做任何运行调试
 * 需要对该插件进行集成测试
 * @author YFsoft
 *
 */
public class AkkaPlugin extends BaseFastPlugin {

	private Logger logger = LoggerFactory.getLogger(getClass());

	private boolean applicationSystemEnabled = false;
	static ActorSystem applicationSystem;

	/**
	 * no /
	 */
	private String config = "akka.conf";
	
	public AkkaPlugin() {

	}

	private ActorSystem applicationSystem() {
		applicationSystemEnabled = true;
		Config akkaConf = ConfigFactory.load(config);
		ActorSystem system = ActorSystem.create("application", akkaConf);
		logger.info("Starting application default Akka system.");
		return system;
	}

	@Override
	public boolean start() {
		applicationSystem = applicationSystem();
		status = Status.ACTIVED;
		return true;
	}

	@Override
	public boolean stop() {
		super.stop();
		if (applicationSystemEnabled) {
			logger.info("Shutdown application default Akka system.");
			applicationSystem.shutdown();
			applicationSystem.awaitTermination();
		}
		return true;
	}
	
}
