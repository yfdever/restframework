package biz.yfsoft.app.fastframework.admin;

import com.jfinal.plugin.activerecord.Db;


public class LogController extends AdminController {
	
	@Override
	protected String getField() {
		return "l.*,u.login_name";
	}

	@Override
	protected String getCondition() {
		return "l.status > 0 and l.user = u.id";
	}
	
	@Override
	protected String getSearchFields(String searchs) {
		if(isSqlInject(searchs)){
			return " 1 = 2 ";
		}
		
		if(searchs == null){
			return " 1 = 1 ";
		}
		return " action like '%{key}%' or args  like '%{key}%'".replace("{key}", searchs);
	}	
	
	@Override
	protected String getTable() {
		return "sys_log l , sys_user u";
	}
	
	public void index() {
		render("/admin/log.jsp");
	}
	
	public void clear(){
		int rows = Db.update("update sys_log set status= -1 ");
		if(rows>0){
			renderJson(wrapJson(0,"",rows));
		}else{
			renderJson(wrapJson(-1,"删除失败!"));
		}
	}
	
	
	
}
