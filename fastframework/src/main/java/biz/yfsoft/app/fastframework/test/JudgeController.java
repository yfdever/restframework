package biz.yfsoft.app.fastframework.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.snaker.engine.access.Page;

import biz.yfsoft.app.fastframework.Constant;
import biz.yfsoft.app.fastframework.snaker.FastWork;
import biz.yfsoft.app.fastframework.snaker.WorkFlowController;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.plugin.activerecord.Record;

/**
 * 代办控制器
 *
 * @author yanhao
 */
public class JudgeController extends WorkFlowController {

	
	public void list() {
		//获取用户
		Record user = getSessionAttr(Constant.CURRENT_USER);
		//分页信息
		Page<FastWork> page = new Page<FastWork>();
		page.setPageNo(getParaToInt("offset", 1));
		page.setPageSize(getParaToInt("limit", 1));
		//查询参数
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("actorId", user.getStr("login_name"));
		//执行查询条件
		query(FASEWORKESQL, FastWork.class, page, params, getPara("sort") ,getPara("order"));
		//返回数据源
		JSONObject rst = new JSONObject();
		rst.put("total", page.getTotalCount());
		rst.put("rows", page.getResult());
		renderJson(rst);
	}
	
	@Override
	public void page() {
		render("/admin/task/judge/list.jsp");
	}

	@Override
	public void form() {
		setAttr("taskId", getPara(1,null));
		render("/admin/task/judge/form.jsp");
	}
	
	public void task(){
		String orderId = getPara(0,null);
		String taskId = getPara(1,null);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("orderId", orderId);
		params.put("parentTaskId", "start");
		List<FastWork> fastWorks = new ArrayList<FastWork>();
		FastWork fastWork = find(HSITFASEWORKESQL, FastWork.class, params);
		if(fastWork != null)
			fastWorks.add(fastWork);
		params.clear();
		params.put("orderId", orderId);
		params.put("taskId", taskId);
		fastWork = find(FASEWORKESQL, FastWork.class, params);
		if(fastWork != null)
			fastWorks.add(fastWork);
		renderJson(fastWorks);
	}
	
}
