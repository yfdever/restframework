package biz.yfsoft.app.fastframework.plugin;

import java.util.Properties;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.plugin.IPlugin;

public interface IFastPlugin extends IPlugin {
	
	public enum Status {
		STOP,ACTIVED,EXCEPTION,NOTSTART
	}

	public void init(Properties properties);
	
	public void init(JSONObject configJson);
	
	public JSONObject getConfigJson();
	
	public Properties getProperties();
	
	public Status getStatus();
	
}
