package biz.yfsoft.app.fastframework.kit;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import biz.yfsoft.app.fastframework.plugin.mongodb.BoParam;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.StrKit;

public class PushKit {

	/**
	 * 给指定的频道发送消息
	 * @param channels 频道列表，要使用逗号隔开
	 * @param data 需要发送的数据信息
	 */
	public static void bordcast(String channels,Map<String,Object> data){
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("X-AVOSCloud-Application-Id", "2lco5zd9yrym993r0li8dhipksuizp2miv9c4i8nedadfhqk");
		headers.put("X-AVOSCloud-Application-Key", "o9w9674ouz01usc8lmlophb6cgdjsaki2pgvc409yjlqp1ra");
		headers.put("Content-Type", "application/json");
		JSONObject jsonMsg = new JSONObject();
		jsonMsg.putAll(data);
		JSONObject json = new JSONObject();
		if(StrKit.notBlank(channels)){
			JSONArray array = new JSONArray();
			array.addAll(Arrays.asList(channels.split(",")));
			json.put("channels", array);
		}
		json.put("data", jsonMsg);
		try {
			RestKit.connection("https://leancloud.cn/1.1/push", "POST", headers, json);
		} catch (IOException e) {
		}
	}
	public static void bordcast(Map<String,Object> data){
		bordcast(null,data);
	}
	
	public static void main(String[] args){
		bordcast(new BoParam().add("title", "中文的看看").add("href", "http://www.baidu.com").getData());
	}
	
	/**
	 * 还是广播一条消息，在消息内容中添加一个id
	 * 用户在获取消息之后，通过该id进行过滤
	 * @param id
	 * @param data
	 */
	public static void notify(String id,Map<String,Object> data){
		data.put("clientid", id);
		bordcast(null,data);
	}
}
