package biz.yfsoft.app.fastframework.core.document;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import biz.yfsoft.app.fastframework.bo.Bo;
import biz.yfsoft.app.fastframework.kit.FileKit;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.upload.UploadFile;

/**
 * 上传文档使用到的组件
 * @author Yfsoft
 *
 */
public abstract class Document {
	
	protected String uuid;

	/**
	 * 通过File对象上传内容
	 * @param file
	 * @return
	 * @throws DocumentSaveHex
	 */
	public abstract void save(File file,Bo fRecord) throws DocumentSaveHex;
	
	public abstract void save(String path,Bo fRecord) throws DocumentSaveHex;
	
	public abstract void saveSync(String path,Bo fRecord) throws DocumentSaveHex;
	
	public abstract void save(byte[] data,Bo fRecord) throws DocumentSaveHex;
	
	public abstract void save(InputStream in,Bo fRecord) throws DocumentSaveHex;
	
	public abstract void loadToLocal(String key,String path) throws IOException;
	
	public abstract void loadToLocal(int fileId,String path) throws IOException;
	
	public abstract byte[] loadToMemory(String key) throws IOException;
	
	public abstract byte[] loadToMemory(int fileId) throws IOException;
	
	
	public Bo save(UploadFile upFile) throws DocumentSaveHex{
		String title = upFile.getFileName();
		Bo fRecord = Document.me().prepar(title);
		Document.me().save(upFile.getFile().getAbsolutePath(), fRecord);
		return fRecord;
	}
	
	public Bo saveSync(UploadFile upFile) throws DocumentSaveHex{
		String title = upFile.getFileName();
		Bo fRecord = Document.me().prepar(title);
		Document.me().saveSync(upFile.getFile().getAbsolutePath(), fRecord);
		return fRecord;
	}
	
	
	public Bo prepar(String name){
		String type = FileKit.getExt(name);
		uuid = UUID.randomUUID().toString().replace("-", "");
		String path =  folder.concat(uuid).concat(".").concat(type);
		return prepar(name,path);
	}
	
	public Bo prepar(String name,String path){
		String type = FileKit.getExt(name);
		//创建一个实体对象，用于存储
		Bo preFile = new Bo();
		preFile.set("path", path);
		if(uuid == null){
			uuid = UUID.randomUUID().toString().replace("-", "");
		}
		preFile.set("uuid", uuid);
		preFile.set("type", type);
		preFile.set("name", name);
		preFile.set("token", getToken());
		preFile.set("url", DEFAULT_UPLOAD_FOLDER + uuid + "." + type);
		preFile.create();
		Db.save("sys_file", preFile);
		uuid = null;
		return preFile;
	}
	
	public static final String DEFAULT_UPLOAD_FOLDER = "/upload/";
	
	public static String folder;
	
	public static void setUploadFolder(String f){
		folder = f;
		File dir = new File(folder);
		if(!dir.exists()){
			dir.mkdirs();
		}
	}
	
	public abstract void init(Object[] args);
	
	public String getToken(){
		return String.valueOf(System.currentTimeMillis());
	}
	
	private static Document _me;
	
	public static Document me(){
		if(_me == null){
			//创建一个本地存储文件的实体
			_me = create(LocalDocument.class);
		}
		return _me;
	}
	
	public static Document create(Class<? extends Document> clz){
		//根据配置文件生成实例
		try {
			_me = (Document)clz.newInstance();
			_me.init(null);
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return _me;
	}
}
