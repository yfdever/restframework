package biz.yfsoft.app.fastframework.user;

import com.jfinal.aop.ClearInterceptor;
import com.jfinal.aop.ClearLayer;
import com.jfinal.core.Controller;

@ClearInterceptor(ClearLayer.ALL)
public class UserController extends Controller {

	public void index(){
		
		render("");
	}
	
	public void signin(){
		render("signin.jsp");
	}
	
	public void signup(){
		render("signup.jsp");
	}
	
	public void doLogin(){
		redirect("main");
	}
	
	public void main(){
		render("main.jsp");
	}
}
