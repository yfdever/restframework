package biz.yfsoft.app.fastframework.admin;

import java.util.HashMap;
import java.util.Map;

import org.bson.types.ObjectId;

import biz.yfsoft.app.fastframework.plugin.mongodb.MongoKit;

import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

public class MessageController extends AdminController {

	private String collectionName="tast_message";
	
	public void save(){
		Record record = new Record();
		record.set("sender", getPara("sender"));
		record.set("message", getPara("message"));
		record.set("receiver", getPara("receiver"));
		record.set("createTime", System.currentTimeMillis());
		record.set("read", 0);
		MongoKit.save(collectionName, record);
		renderJson(record);
	}
	
	public void delete(){
		Map<String, Object> filters = new HashMap<String, Object>();
		filters.put("_id", new ObjectId(getPara("id")));
		MongoKit.remove(collectionName, filters);
	}
	
	public void find(){
		Page<Record> records = MongoKit.paginate(collectionName, getParaToInt("no"), getParaToInt("size"));
		renderJson(records);
	}
	
	public void update(){
		Map<String, Object> query = new HashMap<String, Object>();
		query.put("_id", new ObjectId(getPara("id")));
		Map<String, Object> o = new HashMap<String, Object>();
		o.putAll(getParaMap());
		MongoKit.updateFirst(collectionName, query, o);
	}
}
