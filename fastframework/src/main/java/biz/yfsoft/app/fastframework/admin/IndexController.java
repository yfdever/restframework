package biz.yfsoft.app.fastframework.admin;

import biz.yfsoft.app.fastframework.core.Fast;
import biz.yfsoft.app.fastframework.interceptor.AdminIntercept;
import biz.yfsoft.app.fastframework.kit.IPKit;

import com.jfinal.aop.Before;
import com.jfinal.aop.ClearInterceptor;
import com.jfinal.aop.ClearLayer;

public class IndexController extends AdminController {
	
	@ClearInterceptor(ClearLayer.ALL)
	public void auth(){
		//获取表单参数
		String login_name = getPara("username");
		String login_pass = getPara("password");
		try {
			Fast.me().auth(login_name, login_pass,IPKit.getRemoteLoginUserIp(getRequest()));
			redirect("/admin/index");
		} catch (Exception e) {
			e.printStackTrace();
			redirect("/admin/login");
		}
	}
	
	@ClearInterceptor(ClearLayer.ALL)
	@Before(AdminIntercept.class)
	public void logout() {
		Fast.me().logout();
		redirect("/admin/login");
	}
	
	@ClearInterceptor(ClearLayer.ALL)
	public void login() {
		render("/admin/login.jsp");
	}
	
	public void index() {
		render("/admin/dashboard.jsp");
	}
	
	public void dashboard() {
		render("/admin/dashboard.jsp");
	}
	
	public void users() {
		render("/admin/users/list.jsp");
	}
}
