-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 17, 2015 at 09:43 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `fast`
--

-- --------------------------------------------------------

--
-- Table structure for table `sys_artical`
--

CREATE TABLE IF NOT EXISTS `sys_artical` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `createtime` bigint(20) DEFAULT NULL,
  `creator` varchar(40) DEFAULT NULL,
  `updatetime` bigint(20) DEFAULT NULL,
  `status` int(2) DEFAULT '0',
  `keyword` varchar(500) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `tag` varchar(100) DEFAULT NULL COMMENT ' 标签，如：新手上路，关于我们，等',
  `shortCode` varchar(100) DEFAULT NULL COMMENT '文章简码，如：新手上路的简码为：xssl等，通常以中文名的首字母命名',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `sys_artical`
--

INSERT INTO `sys_artical` (`id`, `title`, `content`, `createtime`, `creator`, `updatetime`, `status`, `keyword`, `description`, `tag`, `shortCode`) VALUES
(1, '关于我们', '<p><span style="font-family:微软雅黑, Microsoft YaHei"><strong><span style="font-size: 19px;">关于我们</span></strong><strong></strong></span></p><p style="margin: 8px 0"><span style="font-family:微软雅黑, Microsoft YaHei"><span style="font-size: 19px;">江阴海运煤炭市场交易支付平台是可以为买卖双方提供安全、便利、快捷、融资等多方面的综合交易平台，在此交易平台上面，会员单位可以享受到物流节点查询、交易实时查询等增值服务，是区别于传统的大宗商品线下交易的线上</span><span style="font-size: 19px;">企业间</span><span style="font-size: 19px;">(B-TO-B)</span><span style="font-size: 19px;">交易，平台采用的第三方支付平台保障了资金的安全性和可操作性，可以为买卖双方解决在大宗商品交易环节资金全程可控性，减少大众商品交易所带来的风险。</span></span></p><hr/><p><span style="font-family:微软雅黑, Microsoft YaHei"><strong><span style="font-size: 19px;">联系电话：</span></strong><strong><span style="font-size:19px">0510-81601966</span></strong></span></p><hr/><p><span style="font-family:微软雅黑, Microsoft YaHei"><strong><span style="font-size: 19px;">传真地址：</span></strong><strong><span style="font-size:19px">0510-81601900</span></strong></span></p><hr/><p><span style="font-family:微软雅黑, Microsoft YaHei"><strong><span style="font-size: 19px;">办公地址：江阴市临港新城珠江路</span></strong><strong><span style="font-size:19px">203</span></strong><strong><span style="font-size: 19px;">号新城监理大厦</span></strong><strong><span style="font-size:19px">16</span></strong><strong><span style="font-size: 19px;">楼</span></strong></span><strong></strong></p><p><br/></p>', 1435318645599, NULL, 1435319931723, 2, '关于我们', '关于我们', 'aboutus', 'gywm'),
(2, '怎样成为卖家', '<p><img src="http://yfdocument.qiniudn.com/FujzV41bhWzu_HaWrgmNWCXuwLsu" _src="http://yfdocument.qiniudn.com/FujzV41bhWzu_HaWrgmNWCXuwLsu"/></p>', 1435318884002, NULL, 1436162321925, 2, '卖家', '注册成为卖家', 'greenhand', 'registerseller'),
(3, '如何买煤', '<p><img src="http://yfdocument.qiniudn.com/Fq6hZLuYheUJ2RthRyYaTy0Gggo2" _src="http://yfdocument.qiniudn.com/Fq6hZLuYheUJ2RthRyYaTy0Gggo2"/></p>', 1435319012028, NULL, 1436162219168, 2, '买煤', '如何买煤', 'abouttrade', 'howtobuycoal'),
(4, '如何卖煤', '<p><img src="http://yfdocument.qiniudn.com/FoAoZ11NrIheKCrvxNAim1YDILSq" _src="http://yfdocument.qiniudn.com/FoAoZ11NrIheKCrvxNAim1YDILSq"/></p>', 1435319553221, NULL, 1436162303247, 2, '卖煤', '如何卖煤', 'abouttrade', 'howtosellcoal'),
(5, '煤炭资讯', '<p><strong><span style="font-size:19px;font-family:宋体">煤炭要闻</span></strong><strong></strong></p><p><strong><span style="font-size:19px"><a href="http://www.cscoal.com/content/0/1/19.xhtml" target="_blank" title="煤炭要闻">http://www.cscoal.com/content/0/1/19.xhtml</a></span></strong></p><p><strong><span style="font-size:19px"><br/></span></strong></p><p><strong><span style="font-size:19px;font-family:宋体">国内资讯</span></strong><strong></strong></p><p><strong><span style="font-size:19px"><a href="http://www.cscoal.com/content/0/1/15.xhtml" target="_blank" title="国内资讯">http://www.cscoal.com/content/0/1/15.xhtml</a></span></strong></p><p><strong><span style="font-size:19px"><br/></span></strong></p><p><strong><span style="font-size:19px;font-family:宋体">国际资讯</span></strong><strong></strong></p><p><strong><span style="font-size:19px"><a href="http://www.cscoal.com/content/0/1/16.xhtml" target="_blank" title="国际咨询">http://www.cscoal.com/content/0/1/16.xhtml</a></span></strong></p><p><strong><span style="font-size:19px"><br/></span></strong></p><p><strong><span style="font-size:19px;font-family:宋体">热点专题</span></strong><strong></strong></p><p><strong><span style="font-size:19px"><a href="http://www.cscoal.com/content/0/1/24.xhtml" target="_blank" title="热点专题">http://www.cscoal.com/content/0/1/24.xhtml</a></span></strong></p><p><strong><span style="font-size:19px"><br/></span></strong></p><p><strong><span style="font-size:19px;font-family:宋体">江阴港平仓价</span></strong><strong></strong></p><p><strong><span style="font-size:19px"><a href="http://www.cscoal.com/content/0/1/5700.xhtml" target="_blank" title="江阴港平仓价">http://www.cscoal.com/content/0/1/5700.xhtml</a> </span></strong></p><p><br/></p>', 1435319759025, NULL, 1435319768103, 2, '资讯', '行情资讯', 'news', 'hqzx'),
(6, '注意事项', '<p style="margin-top:8px;margin-bottom:8px"><span style="font-family:微软雅黑, Microsoft YaHei"><span style="font-size: 19px;">1.</span> <span style="font-size: 19px;">买卖双方确认在线交易前已充分阅读、理解并接受江阴海运煤炭市场已经发布或将来可能公示的各类规则。</span></span></p><hr/><p style="margin-top:8px;margin-bottom:8px"><span style="font-family:微软雅黑, Microsoft YaHei"><span style="font-size: 19px;">2.</span><span style="font-size: 19px;">买方货物需要质检时，需由买卖双方认定的第三方进行质检，采样人员、采样量、检验标准、检验地点等也由双方一致确认，双方达成一致签字后，对货物的检验结果不得再有异议。</span></span></p><hr/><p style="margin-top:8px;margin-bottom:8px"><span style="font-family:微软雅黑, Microsoft YaHei"><span style="font-size: 19px;">3.</span><span style="font-size: 19px;">买卖双方需在协议中一致达成违约赔偿金的比例，违约责任包括卖方未按时发货、买方未按时将资金汇入支付平台、买方未按时签收货物等。</span></span></p><hr/><p style="margin-top:8px;margin-bottom:8px"><span style="font-family:微软雅黑, Microsoft YaHei"><span style="font-size: 19px;">4.</span><span style="font-size: 19px;">买卖双方可以采用江阴海运煤炭市场所提供的法律协议文本，买卖双方可以线下签订合同。</span></span></p><hr/><p style="margin-top:8px;margin-bottom:8px"><span style="font-family:微软雅黑, Microsoft YaHei"><span style="font-size: 20px;">5.</span><span style="font-size: 19px;">货物交付时，原则上需要收货单位指定责任人进行验收。如收货人不能亲自签收，委托第三人签收时，第三人应当提供收货人的授权文件并出示收货人及第三人本人身份证原件。对于卖家负责运输的货物，货物到达买家指定交货地点后，需要先签收再开仓查看的货物，收货人应当要求承运人当场监督并开仓查看，如发现封条损坏或货物缺失等，应当直接退货或者要求在签收单（收货人联和承运人联）上加注详细情况并让承运人签字确认。收货人签字确认后，即表示此次物流节点终结。</span></span></p><p><br/></p>', 1435319908633, NULL, 1435936094187, 2, '注意', '注意事项', 'abouttrade', 'zysx'),
(7, '争议处理', '<p style="margin-top:8px;margin-bottom:8px"><span style="font-family:微软雅黑, Microsoft YaHei"><span style="font-size: 19px;">1</span><span style="font-size: 19px;">）货物风险的转移：除非法律规定或者交易双方另有约定，货物毁损、灭失的风险，在货物交付（收货人签收）之前由发货人承担，交付（收货人签收）之后由收货人承担；在承运人责任导致货物毁损、灭失的情况下，发货人向承运人追偿不影响交易纠纷的处理，发货人应依照本规则承担相应损失。</span></span></p><p style="margin-top:8px;margin-bottom:8px"><span style="font-size: 19px;font-family:微软雅黑, Microsoft YaHei">交易双方可以自行约定货物的交付地点，没有约定或者约定不清的，以买家留下的收货地址作为货物交付地点；双方协议退货的，以卖家留下的退货地址作为交付地点。</span></p><hr/><p style="margin-top:8px;margin-bottom:8px"><span style="font-family:微软雅黑, Microsoft YaHei"><span style="font-size: 19px;">2</span><span style="font-size: 19px;">）发货期限：除非法律规定或者交易双方另有约定，卖家如在买家提出退款申请之前未实际发货的，视为该交易撤销。如卖家在明知或应知买家已申请退款后发货，买家有权拒绝签收</span><span style="font-size: 19px;">(</span><span style="font-size: 19px;">或不接受</span><span style="font-size: 19px;">)</span><span style="font-size: 19px;">，所有因此而造成的风险和损失均由卖家自行承担；如卖家主张在买家提出退款申请前虽未声明发货，但已实际发货的，卖家应提供相应的发货凭证。</span></span></p><hr/><p><span style="font-family:微软雅黑, Microsoft YaHei"><span style="font-size: 19px;">3</span><span style="font-size: 19px;">）争议货款的支付：产生争议后，江阴海运煤炭市场将根据交易双方提供的相关材料，以及双方所签订的违约约定，在双方均认可的情况下，将争议货款的全部或部分支付给交易一方或双方；如一方或双方提出异议，可选择自行协商解决或者通过司法途径解决争议的，江阴海运煤炭市场将暂时保留争议货款并提供必要的配合，直至仲裁机构、人民法院等出具判决结果后，依照判决结果将争议货款的全部或部分支付给交易一方或双方。但自争议发生后的</span><span style="font-size: 19px;">30</span><span style="font-size: 19px;">天内，江阴海运煤炭市场未收到交易双方协商一致的意见或公安机关、人民法院的案件受理通知书等法律文书，或公安机关在受理后</span><span style="font-size: 19px;">7</span><span style="font-size: 19px;">日内未立案或立案后六个月内未对争议款项做出冻结、划拨等处理要求的，江阴海运煤炭市场将暂时保留款项，并定时敦促双方尽快达成进一步协议。</span></span></p><p><br/></p>', 1435320487718, NULL, 1435320491215, 2, '争议处理', '争议处理', 'abouttrade', 'zycl'),
(8, '金融服务', '<p style="text-align: center;"><span style="color: red;font-size:32px;font-family:微软雅黑, Microsoft YaHei">江苏苏美达集团有限公司</span></p><p><span style="font-size: 16px;font-family:微软雅黑, Microsoft YaHei"><br/></span></p><p><img src="http://yfdocument.qiniudn.com/FvJRHikM8FnD-E14wBTWeAvtdCVV" _src="http://yfdocument.qiniudn.com/FvJRHikM8FnD-E14wBTWeAvtdCVV"/></p><p><span style="font-size: 16px;font-family:微软雅黑, Microsoft YaHei">&nbsp;</span></p><p><span style="font-size: 16px;font-family:微软雅黑, Microsoft YaHei">江苏苏美达集团有限公司凭借良好的商业信誉及经营业绩，与海内外诸多金融机构开展广泛的战略合作，构建了具有竞争力的金融资源平台<span>, </span>可以为客户提供专业化、高效率、低成本的供应链金融服务以及包括投融资在内的定制金融解决方案，助力客户做得更多，走得更远。</span></p><p><span style="font-size: 16px;font-family:微软雅黑, Microsoft YaHei"><br/></span></p><p><span style="font-size: 16px;font-family:微软雅黑, Microsoft YaHei">我们在香港成立了苏美达香港公司，并以此为海外市场运营的载体，进行项目投资和海外市场拓展，成为未来项目经营的投资孵化平台和境外融资平台。</span></p><p><span style="font-size: 16px;font-family:微软雅黑, Microsoft YaHei"><br/></span></p><p><span style="font-size: 16px;font-family:微软雅黑, Microsoft YaHei">我们可以为客户提供：</span></p><p><span style="font-size: 16px;font-family:微软雅黑, Microsoft YaHei"><span><br/> • </span>基于贸易业务开展，量身定制各类贸易资金融通方案以及汇率管理、金融产品应用等专业金融咨询服务。</span></p><p><span style="font-size: 16px;font-family:微软雅黑, Microsoft YaHei"><span><br/> • </span>船舶、机电设备、医疗仪器、工程机械等设备融资租赁服务。</span></p><p><span style="font-size: 16px;font-family:微软雅黑, Microsoft YaHei"><span><br/> • </span>包含结构性的过桥融资、杠杆性的长期融资等金融方案，量身定做<span>“EPC+F(finance)”</span>的投融资整体解决方案。</span></p><p><span style="font-size: 16px;font-family:微软雅黑, Microsoft YaHei"><br/></span></p><p><span style="font-size: 16px;font-family:微软雅黑, Microsoft YaHei">联系人：刘汉林</span></p><p><span style="font-size: 16px;font-family:微软雅黑, Microsoft YaHei">电<span>&nbsp; </span>话：<span>02584531102</span></span></p><p><span style="font-size: 16px;font-family:微软雅黑, Microsoft YaHei">手<span>&nbsp; </span>机：<span>13770913660</span></span></p>', 1435320697153, NULL, 1436542135280, 2, '金融服务', '金融服务', 'extraservice', 'jrfw'),
(9, '物流服务', '<p style="text-align: center;"><span style="color: rgb(84, 141, 212); font-size: 32px;font-family:微软雅黑, Microsoft YaHei">中海环球货运有限公司</span></p><p style="text-align: left;"><img src="http://yfdocument.qiniudn.com/FpNt_5IiokMk9LP5KCWxlJXXZMYn" _src="http://yfdocument.qiniudn.com/FpNt_5IiokMk9LP5KCWxlJXXZMYn"/></p><p style="text-align: left;"><span style="color: rgb(84, 141, 212); font-size: 32px;"><span style="color: rgb(0, 0, 0);"><span style="font-size: 16px;"><span style="font-size: 12px;font-family:微软雅黑, Microsoft YaHei"><br/></span></span></span></span></p><p style="text-align: left;"><span style="color: rgb(84, 141, 212); font-size: 32px;"><span style="color: rgb(0, 0, 0);"><span style="font-size: 16px;font-family:微软雅黑, Microsoft YaHei"><span style="font-size: 12px;"></span></span></span></span></p><p><span style="font-family:微软雅黑, Microsoft YaHei"><span style="font-size: 20px; color: rgb(84, 141, 212);">中海环球货运有限公司</span><span style="font-size: 16px;">（简称中海环球货运）是由国家商务部、国家民航总局和海关总署批准的大型一类国际货运代理企</span></span></p><p><span style="font-size: 16px;font-family:微软雅黑, Microsoft YaHei">业，注 册资本为一亿元。其前身为中国海运集团直属的一级企业，成立于<span>2001</span>年<span>6</span>月的中海环球空运有限公司。目前，中海环球货</span></p><p><span style="font-size: 16px;font-family:微软雅黑, Microsoft YaHei">运有限公司由中海船务代理有限公司 全资控股，并于<span>2013</span>年<span>8</span>月，正式由中海环球空运有限公司更名为中海环球货运有限公司。</span></p><p><span style="font-family:微软雅黑, Microsoft YaHei">中海环球货运经过多年的发展，始终秉持客户体验至上的服务理念，为客户提供空运代理、海运代理、陆路运输、综合仓储、报关</span></p><p><span style="font-family:微软雅黑, Microsoft YaHei">报检、多式联运、电商物流等全方位一站式服务，使客户享受“安全、准时、定制、快乐”的服务体验。&nbsp;&nbsp;&nbsp; 我们为您设计更低成</span></p><p><span style="font-family:微软雅黑, Microsoft YaHei">本、更合理、更高效的海运运输路线，让您获得一流的服务。<br/></span></p><p><span style="font-size: 16px;font-family:微软雅黑, Microsoft YaHei"><br/></span></p><p><span style="font-family:微软雅黑, Microsoft YaHei"><span style="font-size: 16px;">&nbsp;&nbsp;&nbsp; </span><span style="font-size: 16px;">于此我们中海环球货运进江煤炭长期与太仓港，南通如皋港，靖江盈利港务，张家港，江阴港，泰州港，常州港，镇江港，南京</span></span></p><p><span style="font-size: 16px;font-family:微软雅黑, Microsoft YaHei">港，马鞍山港，芜湖港，铜陵港等等，沿线几十个大型港口保持良好的运输合作关系。</span></p><p><span style="font-size: 16px;font-family:微软雅黑, Microsoft YaHei"><br/></span></p><p><span style="font-family:微软雅黑, Microsoft YaHei"><span style="font-size: 16px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span style="font-size: 16px; color: black;">联系人：黄靖</span><span style="font-size: 16px;color:black">18912677788&nbsp;&nbsp;&nbsp; </span></span></p><p><span style="font-size: 16px; color: black;font-family:微软雅黑, Microsoft YaHei"><br/></span></p><p><span style="font-family:微软雅黑, Microsoft YaHei"><span style="font-size: 16px; color: black;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span style="font-size: 16px; color: black;">邮</span><span style="font-size: 16px; color: black;">&nbsp; </span><span style="font-size: 16px; color: black;">箱：</span><span style="font-size: 16px; color: black;">huangj@csl.cn&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</span></span></p><p style="text-align: left;"><span style="color: rgb(84, 141, 212); font-size: 32px;"><span style="color: rgb(0, 0, 0);"><span style="font-size: 16px;"><span style="font-size: 12px;font-family:微软雅黑, Microsoft YaHei"><br/></span></span></span></span><br/></p>', 1435320967388, NULL, 1436542119601, 2, '物流', '物流服务', 'extraservice', 'wlfw'),
(10, '质检服务', '<p style="text-align: center;"><span style="font-size: 32px; color: rgb(255, 0, 0);font-family:微软雅黑, Microsoft YaHei">SGS</span></p><p style="text-align: left;"><span style="font-size: 32px; color: rgb(255, 0, 0);font-family:微软雅黑, Microsoft YaHei"><img src="http://yfdocument.qiniudn.com/Fo2cYPBZ3HpCqp_MxA43yb6MnB7i" _src="http://yfdocument.qiniudn.com/Fo2cYPBZ3HpCqp_MxA43yb6MnB7i"/></span></p><p style="text-align: left;"><span style="font-size: 32px; color: rgb(255, 0, 0);"><span style="color: rgb(0, 0, 0);font-family:微软雅黑, Microsoft YaHei"><span style="font-size: 12px;"></span></span></span></p><p><span style="font-family:微软雅黑, Microsoft YaHei"><span style="font-size: 16px;">SGS</span><span style="font-size: 16px;">是全球领先的检验、鉴定、测试和认证机构，是全球公<span style="color:#000000">认的质量和诚信基准。SGS通标标准技术服务有限公司是瑞士SGS集团和隶属于原国家质量技术监督局的中国标准技术开发公司共同建成于1991年的合资公司，取&quot;通用公证行&quot;和&quot;标准计量局&quot;首字之意，在中国设立了50多个分支机构和几十间实验室，拥有12000多名训练有素的专业人员。</span></span></span></p><p style="margin-top:12px;margin-bottom:12px;text-autospace:ideograph-other"><span style="font-family:微软雅黑, Microsoft YaHeicolor:#000000"><strong><span style="font-size: 16px;">1</span></strong><strong><span style="font-size: 16px;">通用公证行</span></strong></span></p><p style="margin-top:12px;margin-bottom:12px;text-autospace:ideograph-other"><span style="font-family:微软雅黑, Microsoft YaHei"><span style="font-size: 16px;color:#000000">SGS </span><span style="font-size: 16px;"><span style="color:#000000">是一个综合性的检验机构，可进行各种物理、化学和冶金分析，包括进行破坏性和非破坏性试验，向委托人提供一套完整的数量和质量检验以及有关的技术服务，提 供装运前的检验服务，提供各种与国际贸易有关的诸如商品技术、运输、仓储等方面的服务，监督跟购销、贸易、原材料、工业设备、消费品迁移有关联的全部或任 何一部分的商业贸易暨操作过程。在SGS内部，按照商品分类，设立了农业服务部，矿物化工和冶金服务部，非破坏性试验科，国家政府合同服务部，运输和仓库部，工业工程产品服务科，风险和保险服务部等部门。</span><a name="2" href="http://"></a><a name="sub103858_2" href="http://"></a><a name="核心服务" href="http://"></a></span></span></p><p style="margin-top:12px;margin-bottom:12px;text-autospace:ideograph-other"><span style="font-family:微软雅黑, Microsoft YaHei"><strong><span style="font-size: 16px;">2</span></strong><strong><span style="font-size: 16px;">核心服务</span></strong></span></p><p><span style="font-family:微软雅黑, Microsoft YaHei"><strong><span style="font-size: 16px;">检验：</span></strong><span style="font-size: 16px;">我们提供世界领先的全方位检测和验证服务，例如转运时检查贸易商品的状况和重量，帮助控制数量和质量，满足不同地区和市场的所有相关监管要求。</span></span></p><p><span style="font-family:微软雅黑, Microsoft YaHei"><strong><span style="font-size: 16px;">测试：</span></strong><span style="font-size: 16px;">我们的全球测试设施网络配备知识渊博、经验丰富的人员，能够帮助您降低风险、缩短上市时间并根据相关的健康、安全和规范标准对您产品的质量、安全和性能进行测试。</span></span></p><p><span style="font-family:微软雅黑, Microsoft YaHei"><strong><span style="font-size: 16px;">认证：</span></strong><span style="font-size: 16px;">通过认证，我们能够向您证明您的产品、流程、系统或服务是否符合国内和国际标准及规范或客户定义的标准。</span></span></p><p><span style="font-family:微软雅黑, Microsoft YaHei"><strong><span style="font-size: 16px;">鉴定</span></strong><span style="font-size: 16px;">：我们确保产品与服务遵守全球标准与当地法规。 通过将全球覆盖率与几乎包括各个行业的当地知识、无与伦比的经验和专业知识相结合，<span>SGS </span>涵盖了从原材料到最终消费的整条供应链。</span></span></p><p><span style="font-size: 16px;font-family:微软雅黑, Microsoft YaHei">联系人：黄晓钢<span>&nbsp;&nbsp; </span></span></p><p><span style="font-size: 16px;font-family:微软雅黑, Microsoft YaHei">电<span>&nbsp; </span>话：<span>18622595693</span></span></p><p><span style="font-size: 16px;font-family:微软雅黑, Microsoft YaHei">邮<span>&nbsp; </span>箱：<span style="undefinedcolor:#000000"><span style="color:#000000">Norman.huang@sgs.com</span>&nbsp; &nbsp; &nbsp;</span></span></p><p style="text-align: left;"><span style="font-size: 32px; color: rgb(255, 0, 0);"><span style="color: rgb(0, 0, 0);"><span style="font-size: 12px;font-family:微软雅黑, Microsoft YaHei"><br/></span></span></span><br/></p>', 1435321392588, NULL, 1436542093291, 2, '质检', '质检服务', 'extraservice', 'zjfw');

-- --------------------------------------------------------

--
-- Table structure for table `sys_behavior`
--

CREATE TABLE IF NOT EXISTS `sys_behavior` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createtime` bigint(20) NOT NULL,
  `updatetime` bigint(20) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `title` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  `enable` int(11) NOT NULL,
  `webhook` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `sys_behavior`
--

INSERT INTO `sys_behavior` (`id`, `createtime`, `updatetime`, `status`, `title`, `name`, `enable`, `webhook`) VALUES
(1, 1427338077246, 1427338077246, 0, '1231', '123', 1, '321321'),
(2, 1427338111451, 1427338111451, 0, '2121', '2121', 1, '31313'),
(3, 1427338815155, 1427338815155, 1, 'yonghu logout', '/admin/logout', 1, 'http://localhost:8009/rest/test/foo2'),
(4, 1427339775054, 1427339775054, 1, 'DoLogin To System', '/admin/doLogin', 1, 'http://localhost:8009/rest/test/foo2'),
(5, 1427342149336, 1428137132903, 1, 'Setting', '/admin/setting', 1, 'http://localhost:8009/admin/notify/emailNotify'),
(6, 1434021681456, 1434022475444, 1, '支付的回调', '/admin/notify/payNotify', 1, 'http://localhost:8080/trade/main/buyers/payNotify');

-- --------------------------------------------------------

--
-- Table structure for table `sys_document`
--

CREATE TABLE IF NOT EXISTS `sys_document` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(100) NOT NULL,
  `author` int(11) NOT NULL COMMENT '用户id',
  `status` int(4) NOT NULL DEFAULT '0',
  `createtime` bigint(20) NOT NULL DEFAULT '0',
  `updatetime` bigint(20) NOT NULL DEFAULT '0',
  `content` text COMMENT '内容',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sys_file`
--

CREATE TABLE IF NOT EXISTS `sys_file` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `hash` varchar(100) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `path` varchar(200) DEFAULT NULL,
  `uuid` varchar(50) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `createtime` bigint(20) NOT NULL,
  `updatetime` bigint(20) NOT NULL,
  `mode` varchar(20) NOT NULL DEFAULT 'LOCAL',
  `size` bigint(40) NOT NULL DEFAULT '0',
  `type` varchar(20) DEFAULT NULL,
  `folder` varchar(50) NOT NULL DEFAULT 'ROOT',
  `creater` int(11) DEFAULT NULL,
  `token` varchar(500) DEFAULT NULL,
  `url` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='系统文件' AUTO_INCREMENT=87 ;

--
-- Dumping data for table `sys_file`
--

INSERT INTO `sys_file` (`id`, `hash`, `name`, `path`, `uuid`, `status`, `createtime`, `updatetime`, `mode`, `size`, `type`, `folder`, `creater`, `token`, `url`) VALUES
(1, 'FneVj4_YSE16yUcyyTpt58mwWbS5', 'fast_20150711103759.bak', '/tmp/fast_20150711103759.bak', '59a4c952ec7448faa4fbd00e63466930', 1, 1436582280058, 1436582320298, 'QINIU', 0, 'bak', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:J0XcAe9GaKKZEpbAEJErtcHSO0c=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NTg3OX0=', 'http://yfdocument.qiniudn.com/FneVj4_YSE16yUcyyTpt58mwWbS5'),
(2, 'FkqGUWkcpUEsaQ5qVKiCC4X_1cP7', 'yyzz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/6051ca7c7d2a4332bcd1af60d13fb7ee.jpg', '911572cd30a14d7c8bdca753fa3b767a', 1, 1436582655262, 1436582670430, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:TmNdjUoQK42pd3Kzr7TLXg7AT4Q=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NjI1NX0=', 'http://yfdocument.qiniudn.com/FkqGUWkcpUEsaQ5qVKiCC4X_1cP7'),
(3, 'FpGPRIuiYUhicHcR2e8x6TpCgKTO', 'jyxkz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/f45777f9d0444280a0bbef44c41c41c9.jpg', 'edfdd69ddd944ff2b8a1390ad5e78e4a', 1, 1436582670433, 1436582670495, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:8pRPtNfpdgqqRVgPaOProaoBhMs=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NjI3MH0=', 'http://yfdocument.qiniudn.com/FpGPRIuiYUhicHcR2e8x6TpCgKTO'),
(4, 'FibHrfbO1dasrkav2aa9QQSbpH0W', 'swdjz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/54a02a3d892548f5bb360a42565ec48e.jpg', '0f27c0a20cbc45cfbe1bef2bd6269526', 1, 1436582670497, 1436582670567, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:8pRPtNfpdgqqRVgPaOProaoBhMs=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NjI3MH0=', 'http://yfdocument.qiniudn.com/FibHrfbO1dasrkav2aa9QQSbpH0W'),
(5, 'FmvoBkCjiOerjFyhZ1dc_aucNqvy', 'khxkz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/bba1e9622a1d4a8281c603b102fa7477.jpg', 'ea673655975f4925be715acd3aee1a93', 1, 1436582670568, 1436582670629, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:8pRPtNfpdgqqRVgPaOProaoBhMs=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NjI3MH0=', 'http://yfdocument.qiniudn.com/FmvoBkCjiOerjFyhZ1dc_aucNqvy'),
(6, 'FvJRHikM8FnD-E14wBTWeAvtdCVV', '合作方3（225.92）.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/84f4fb52c18d4bf9822b237d7b20bcaa.jpg', 'e801c7ee77264240b3c366336721e2c9', 1, 1436582670630, 1436582670788, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:8pRPtNfpdgqqRVgPaOProaoBhMs=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NjI3MH0=', 'http://yfdocument.qiniudn.com/FvJRHikM8FnD-E14wBTWeAvtdCVV'),
(7, 'FiKKq92BDSGTrYE88uNtmp8NlxO0', 'zzjgdmz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/c26577b1333d42a7b18bf9bcfaf9e3e0.jpg', 'e13dbe429cba4ceda47cb31fbde5b3f6', 1, 1436582670791, 1436582670852, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:8pRPtNfpdgqqRVgPaOProaoBhMs=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NjI3MH0=', 'http://yfdocument.qiniudn.com/FiKKq92BDSGTrYE88uNtmp8NlxO0'),
(8, 'FkqGUWkcpUEsaQ5qVKiCC4X_1cP7', 'yyzz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/53df6e54a6bd44eeb888b441c9e856fd.jpg', 'a3294fa78b304ec097ea4e262018bd4e', 1, 1436582866789, 1436582886883, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:jTYTsvic57kZqKeeeYB_43lBXhA=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NjQ2Nn0=', 'http://yfdocument.qiniudn.com/FkqGUWkcpUEsaQ5qVKiCC4X_1cP7'),
(9, 'FpGPRIuiYUhicHcR2e8x6TpCgKTO', 'jyxkz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/9998e7c62a3d45aca13a5598d2ceea4e.jpg', 'b9eadc02c9bb45559bbe0ca1c9849233', 1, 1436582886888, 1436582886949, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:GhLhSBoMmzmfVy79Hw5EQeLFIkQ=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NjQ4Nn0=', 'http://yfdocument.qiniudn.com/FpGPRIuiYUhicHcR2e8x6TpCgKTO'),
(10, 'FibHrfbO1dasrkav2aa9QQSbpH0W', 'swdjz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/e0e66761ed4c4794ae635d5e6d9c4b22.jpg', '4ca9c78c5e9b463e9cb118079bb655fb', 1, 1436582886951, 1436582887035, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:GhLhSBoMmzmfVy79Hw5EQeLFIkQ=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NjQ4Nn0=', 'http://yfdocument.qiniudn.com/FibHrfbO1dasrkav2aa9QQSbpH0W'),
(11, 'FmvoBkCjiOerjFyhZ1dc_aucNqvy', 'khxkz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/25d90adf47f64037a0a311dd98360d44.jpg', '3ee8271a53584783a2758c429204e847', 1, 1436582887038, 1436582887101, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:riwTR-IrD5AGNVrIZGgd1MJc4qI=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NjQ4N30=', 'http://yfdocument.qiniudn.com/FmvoBkCjiOerjFyhZ1dc_aucNqvy'),
(12, 'Fo2cYPBZ3HpCqp_MxA43yb6MnB7i', '合作方2（225.92）.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/1c90f5beacaa4062897e80dcab96cfb6.jpg', 'd5ca67c456064f5d966154bc0af2fbd6', 1, 1436582887103, 1436582887214, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:riwTR-IrD5AGNVrIZGgd1MJc4qI=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NjQ4N30=', 'http://yfdocument.qiniudn.com/Fo2cYPBZ3HpCqp_MxA43yb6MnB7i'),
(13, 'FiKKq92BDSGTrYE88uNtmp8NlxO0', 'zzjgdmz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/b4e838ab8c764eb88be83c3d72fe0705.jpg', '0bb45e1785e64b0198a9c3a7cc21e79a', 1, 1436582887216, 1436582887277, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:riwTR-IrD5AGNVrIZGgd1MJc4qI=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NjQ4N30=', 'http://yfdocument.qiniudn.com/FiKKq92BDSGTrYE88uNtmp8NlxO0'),
(14, 'FkqGUWkcpUEsaQ5qVKiCC4X_1cP7', 'yyzz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/97aac0cfb68146b9955bef28c9be09dd.jpg', '6a250ac9dfa24e66a5f1b2bc49bc35a4', 1, 1436583056876, 1436583071946, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:_P3KjxHPGmNqrl9vka9CH0K_eqw=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NjY1Nn0=', 'http://yfdocument.qiniudn.com/FkqGUWkcpUEsaQ5qVKiCC4X_1cP7'),
(15, 'FpGPRIuiYUhicHcR2e8x6TpCgKTO', 'jyxkz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/47117fe5ee9b4aacbd0cec351c554445.jpg', 'a48f630e51c547d68255e12b00290325', 1, 1436583071948, 1436583072001, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:CzWu9e79EO9qvczkpTWNS6gvL8c=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NjY3MX0=', 'http://yfdocument.qiniudn.com/FpGPRIuiYUhicHcR2e8x6TpCgKTO'),
(16, 'FibHrfbO1dasrkav2aa9QQSbpH0W', 'swdjz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/2d54acc2c2d9453d92b147bf5b4b7ae7.jpg', 'b209ac47d6154c81854310e975d6cf56', 1, 1436583072003, 1436583072053, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:mk32dPtqasLEHTEpoPqGg-MRIU0=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NjY3Mn0=', 'http://yfdocument.qiniudn.com/FibHrfbO1dasrkav2aa9QQSbpH0W'),
(17, 'FmvoBkCjiOerjFyhZ1dc_aucNqvy', 'khxkz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/7c54f2d7977147c9ab67033a210ef4e8.jpg', '01eca1eb500445d9ac88bbdaa8b21f68', 1, 1436583072055, 1436583072104, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:mk32dPtqasLEHTEpoPqGg-MRIU0=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NjY3Mn0=', 'http://yfdocument.qiniudn.com/FmvoBkCjiOerjFyhZ1dc_aucNqvy'),
(18, 'FpNt_5IiokMk9LP5KCWxlJXXZMYn', '合作方4（225.92）.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/27b1ef364a77496c93fd9155e7b2e0eb.jpg', 'bf41eca33a764359a8fdb3ef2625b346', 1, 1436583072105, 1436583072176, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:mk32dPtqasLEHTEpoPqGg-MRIU0=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NjY3Mn0=', 'http://yfdocument.qiniudn.com/FpNt_5IiokMk9LP5KCWxlJXXZMYn'),
(19, 'FiKKq92BDSGTrYE88uNtmp8NlxO0', 'zzjgdmz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/451c2bb5cdc94d0caf3e1df553f7f064.jpg', '2d3aab7ec8d14ed3af50e904ffb2f3db', 1, 1436583072179, 1436583072229, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:mk32dPtqasLEHTEpoPqGg-MRIU0=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NjY3Mn0=', 'http://yfdocument.qiniudn.com/FiKKq92BDSGTrYE88uNtmp8NlxO0'),
(21, 'Fl48zK-IcSxSq2st6zKfZUjaPhIR', 'bat_20150711105752.bat', '/tmp/bat_20150711105752.bat', 'e5c7ee8ea803454eb621032d078d33df', 1, 1436583472210, 1436583512387, 'QINIU', 0, 'bat', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:fZ4qrqBln2Cqk4PcfvZF3YbE-HI=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzA3Mn0=', 'http://yfdocument.qiniudn.com/Fl48zK-IcSxSq2st6zKfZUjaPhIR'),
(22, 'FkqGUWkcpUEsaQ5qVKiCC4X_1cP7', 'yyzz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/434bc82f892d49cb9af190e34addd2d6.jpg', '71170da7ab384c539cb38e8c280f1321', 1, 1436583561270, 1436583576429, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:7O77MytElwmLSIgYfNmxTSIJqW4=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzE2MX0=', 'http://yfdocument.qiniudn.com/FkqGUWkcpUEsaQ5qVKiCC4X_1cP7'),
(23, 'FpGPRIuiYUhicHcR2e8x6TpCgKTO', 'jyxkz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/103ef927684648c1b95af732aed23cc0.jpg', 'ee2df8442aea4f15a6ca09834337fe6a', 1, 1436583576432, 1436583576501, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:fzz0RFeHVOo7VWEdV1-_fyw0Alw=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzE3Nn0=', 'http://yfdocument.qiniudn.com/FpGPRIuiYUhicHcR2e8x6TpCgKTO'),
(24, 'FibHrfbO1dasrkav2aa9QQSbpH0W', 'swdjz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/e2b3b374e6444018a69e3c5ea7351abd.jpg', '3caa536650cc4d5bb261b37fb34972b6', 1, 1436583576503, 1436583576568, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:fzz0RFeHVOo7VWEdV1-_fyw0Alw=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzE3Nn0=', 'http://yfdocument.qiniudn.com/FibHrfbO1dasrkav2aa9QQSbpH0W'),
(25, 'FmvoBkCjiOerjFyhZ1dc_aucNqvy', 'khxkz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/3dc0ed1f22f54e658b691ff26f34d169.jpg', 'bad726ed37344fe2bc2ce35eb5001375', 1, 1436583576573, 1436583576630, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:fzz0RFeHVOo7VWEdV1-_fyw0Alw=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzE3Nn0=', 'http://yfdocument.qiniudn.com/FmvoBkCjiOerjFyhZ1dc_aucNqvy'),
(26, 'FoQRhITUFOG8A4eNd-GLbMM-016u', '1.JPG', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/3d6a902dc4fa4a8a8a0f3b013e0fdbd0.JPG', '73d36f9777d249c78eb64556edb38c4d', 1, 1436583576631, 1436583577311, 'QINIU', 0, 'JPG', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:fzz0RFeHVOo7VWEdV1-_fyw0Alw=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzE3Nn0=', 'http://yfdocument.qiniudn.com/FoQRhITUFOG8A4eNd-GLbMM-016u'),
(27, 'FiKKq92BDSGTrYE88uNtmp8NlxO0', 'zzjgdmz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/7cf9de1e09e3401eb3dc8f1b0c572308.jpg', '9a9c09beac24455cbac3e21167c1e1bc', 1, 1436583577313, 1436583577359, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:LfthUoIOeoKk0SCwS2nsD8CJHDU=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzE3N30=', 'http://yfdocument.qiniudn.com/FiKKq92BDSGTrYE88uNtmp8NlxO0'),
(28, 'FiKKq92BDSGTrYE88uNtmp8NlxO0', 'zzjgdmz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/663cd3ab8fad4c60bae122dcafac02ef.jpg', 'b0a61b643bd84f36ba47a5b0957eb6c1', 1, 1436583629676, 1436583629838, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:eGnb8adDjxr2-IVVP5DVBq1aMuI=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzIyOX0=', 'http://yfdocument.qiniudn.com/FiKKq92BDSGTrYE88uNtmp8NlxO0'),
(29, 'FibHrfbO1dasrkav2aa9QQSbpH0W', 'swdjz1.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/afa773fe64e34b6da9fe0bc7da5e7537.jpg', 'bf9d40f5564a48888c9b52b06129d9ce', 1, 1436583629841, 1436583629886, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:eGnb8adDjxr2-IVVP5DVBq1aMuI=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzIyOX0=', 'http://yfdocument.qiniudn.com/FibHrfbO1dasrkav2aa9QQSbpH0W'),
(30, 'FpGPRIuiYUhicHcR2e8x6TpCgKTO', 'jyxkz1.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/5b48c90f44e54ed6b3fed55c32c88921.jpg', 'f00d96ca1aa942c09da0b6ac6ddcdc76', 1, 1436583629888, 1436583629931, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:eGnb8adDjxr2-IVVP5DVBq1aMuI=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzIyOX0=', 'http://yfdocument.qiniudn.com/FpGPRIuiYUhicHcR2e8x6TpCgKTO'),
(31, 'FpGPRIuiYUhicHcR2e8x6TpCgKTO', 'jyxkz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/68466604d6fa4ea2a820c572f831bc23.jpg', 'a8135bcbd62f4dd1a3216cb464d3c1af', 1, 1436583629933, 1436583629978, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:eGnb8adDjxr2-IVVP5DVBq1aMuI=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzIyOX0=', 'http://yfdocument.qiniudn.com/FpGPRIuiYUhicHcR2e8x6TpCgKTO'),
(32, 'FmvoBkCjiOerjFyhZ1dc_aucNqvy', 'khxkz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/d24faff338844281b30bb981bf4eebc4.jpg', '6e956080373d4951b4f34d3bcf94eaa4', 1, 1436583629980, 1436583630029, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:eGnb8adDjxr2-IVVP5DVBq1aMuI=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzIyOX0=', 'http://yfdocument.qiniudn.com/FmvoBkCjiOerjFyhZ1dc_aucNqvy'),
(33, 'FibHrfbO1dasrkav2aa9QQSbpH0W', 'swdjz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/ddbaa4e66e0e443f88d8d0cf9c84923e.jpg', '4eb6c1e4c698475a9916417ab26b6934', 1, 1436583630031, 1436583630078, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:fYtyp-xOBPm_BD-XMpcOTppB4w0=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzIzMH0=', 'http://yfdocument.qiniudn.com/FibHrfbO1dasrkav2aa9QQSbpH0W'),
(34, 'FmvoBkCjiOerjFyhZ1dc_aucNqvy', 'khxkz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/97736a19e4154abc93f5b47ab071e1ed.jpg', 'f2c522edf0ed4d95b8c61d68f89680b3', 1, 1436583782311, 1436583797397, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:Hkip9RVTLDQx_1qL0kCjMku-E0Y=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzM4Mn0=', 'http://yfdocument.qiniudn.com/FmvoBkCjiOerjFyhZ1dc_aucNqvy'),
(35, 'FpGPRIuiYUhicHcR2e8x6TpCgKTO', 'jyxkz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/f7847897349442dcb1bb25c3618d3cf0.jpg', '7ccb4252c89f42648124ee2078c3a892', 1, 1436583797400, 1436583797455, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:VoRyRGWTqtbez2T5kKPZI5CjWVY=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzM5N30=', 'http://yfdocument.qiniudn.com/FpGPRIuiYUhicHcR2e8x6TpCgKTO'),
(36, 'FibHrfbO1dasrkav2aa9QQSbpH0W', 'swdjz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/74cbb6386cb949018cef6e75c12a7679.jpg', '9572c2c00dbf4162901a75d279dcc517', 1, 1436583797456, 1436583797513, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:VoRyRGWTqtbez2T5kKPZI5CjWVY=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzM5N30=', 'http://yfdocument.qiniudn.com/FibHrfbO1dasrkav2aa9QQSbpH0W'),
(37, 'FkqGUWkcpUEsaQ5qVKiCC4X_1cP7', 'yyzz1.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/8cd90627d0944477b820d95b16b95be2.jpg', 'c3350f56391c41389cc894132957d100', 1, 1436583797515, 1436583797568, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:VoRyRGWTqtbez2T5kKPZI5CjWVY=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzM5N30=', 'http://yfdocument.qiniudn.com/FkqGUWkcpUEsaQ5qVKiCC4X_1cP7'),
(38, 'Fp0DOgkv_ODDaAPzWUKkVYFu5sgx', '9.JPG', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/ad300e643a25489ebf896e00246c0bf6.JPG', 'a75de577117a48b8a963e33ec8810620', 1, 1436583797569, 1436583808581, 'QINIU', 0, 'JPG', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:VoRyRGWTqtbez2T5kKPZI5CjWVY=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzM5N30=', 'http://yfdocument.qiniudn.com/Fp0DOgkv_ODDaAPzWUKkVYFu5sgx'),
(39, 'FkqGUWkcpUEsaQ5qVKiCC4X_1cP7', 'yyzz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/19cb13c8a3d64f8584d9ccb5e8b8eaf4.jpg', 'dde877dc68e94f83a2475cd59e61d00a', 1, 1436583808585, 1436583808639, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:uodSKcmBGRIurMzch-JTjZ6ty5U=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzQwOH0=', 'http://yfdocument.qiniudn.com/FkqGUWkcpUEsaQ5qVKiCC4X_1cP7'),
(40, 'FkqGUWkcpUEsaQ5qVKiCC4X_1cP7', 'yyzz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/69e624e6d02e4923b84fc7f08604b764.jpg', '18b8735819ab43fd953412f12f4fd067', 1, 1436583857876, 1436583857936, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:Q-2yU027Ar8tcwu1MVjiR0kPl_8=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzQ1N30=', 'http://yfdocument.qiniudn.com/FkqGUWkcpUEsaQ5qVKiCC4X_1cP7'),
(41, 'FiKKq92BDSGTrYE88uNtmp8NlxO0', 'zzjgdmz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/a6b3feeef8b646569b0627ccee470c81.jpg', 'afa871a30395485f894ba0c6db989d1c', 1, 1436583857939, 1436583857994, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:Q-2yU027Ar8tcwu1MVjiR0kPl_8=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzQ1N30=', 'http://yfdocument.qiniudn.com/FiKKq92BDSGTrYE88uNtmp8NlxO0'),
(42, 'FibHrfbO1dasrkav2aa9QQSbpH0W', 'swdjz1.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/e5a72b133fb446a58e55d18a5ebfe463.jpg', '499b23fbc40f47f787e09a6ccbdda491', 1, 1436583857996, 1436583858048, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:Q-2yU027Ar8tcwu1MVjiR0kPl_8=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzQ1N30=', 'http://yfdocument.qiniudn.com/FibHrfbO1dasrkav2aa9QQSbpH0W'),
(43, 'FpGPRIuiYUhicHcR2e8x6TpCgKTO', 'jyxkz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/5718c721768b40bbad07e2bcfe90c35b.jpg', '53643cf69c6c467c8e4bf49dbcbca5e5', 1, 1436583858049, 1436583858124, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:nvo6zOb3TZ-jykXFNiCQ9nETtMA=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzQ1OH0=', 'http://yfdocument.qiniudn.com/FpGPRIuiYUhicHcR2e8x6TpCgKTO'),
(44, 'FibHrfbO1dasrkav2aa9QQSbpH0W', 'swdjz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/2d443dd82f324476ab47f33123e43771.jpg', '6fa8f56f13ea48958dc23ae8aa7e5278', 1, 1436583858126, 1436583858177, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:nvo6zOb3TZ-jykXFNiCQ9nETtMA=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzQ1OH0=', 'http://yfdocument.qiniudn.com/FibHrfbO1dasrkav2aa9QQSbpH0W'),
(45, 'FmvoBkCjiOerjFyhZ1dc_aucNqvy', 'khxkz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/ad7d7f657b7f415ab0a44398135b8521.jpg', 'b70c7090cb2b40af8e2a2217ea562fd7', 1, 1436583858178, 1436583858235, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:nvo6zOb3TZ-jykXFNiCQ9nETtMA=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzQ1OH0=', 'http://yfdocument.qiniudn.com/FmvoBkCjiOerjFyhZ1dc_aucNqvy'),
(46, 'FiKKq92BDSGTrYE88uNtmp8NlxO0', 'zzjgdmz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/1211f8b87acd4292a1721f0b82ec725c.jpg', '8ac29597c1e54be5ae2d6e51ac54425e', 1, 1436583891284, 1436583891339, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:ZpdtqxODI9M_76VpF2wGLFBf3WQ=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzQ5MX0=', 'http://yfdocument.qiniudn.com/FiKKq92BDSGTrYE88uNtmp8NlxO0'),
(47, 'FmvoBkCjiOerjFyhZ1dc_aucNqvy', 'khxkz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/9728a37627eb4250a6cf55836e6c133b.jpg', 'faa828948f034b7d8fe8df4e8d3bdbe2', 1, 1436583891341, 1436583891396, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:ZpdtqxODI9M_76VpF2wGLFBf3WQ=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzQ5MX0=', 'http://yfdocument.qiniudn.com/FmvoBkCjiOerjFyhZ1dc_aucNqvy'),
(48, 'FpGPRIuiYUhicHcR2e8x6TpCgKTO', 'jyxkz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/35577baf33424209a4c0e39d1f271ede.jpg', '5e531777d473410ea4db9db16efbf383', 1, 1436583891398, 1436583891455, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:ZpdtqxODI9M_76VpF2wGLFBf3WQ=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzQ5MX0=', 'http://yfdocument.qiniudn.com/FpGPRIuiYUhicHcR2e8x6TpCgKTO'),
(49, 'FibHrfbO1dasrkav2aa9QQSbpH0W', 'swdjz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/d7b34f4660194eff9ce47ea1c992f9e7.jpg', 'b8dffc0554304d60ba19bade0de27e2c', 1, 1436583891457, 1436583891514, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:ZpdtqxODI9M_76VpF2wGLFBf3WQ=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzQ5MX0=', 'http://yfdocument.qiniudn.com/FibHrfbO1dasrkav2aa9QQSbpH0W'),
(50, 'FmvoBkCjiOerjFyhZ1dc_aucNqvy', 'khxkz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/27b3fac2e58941449c000316475fa58e.jpg', '834af953652542d5a21abe379a87f2de', 1, 1436584007398, 1436584022727, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:l6oTgZkAH4eIMRN2Y3H4WAJpPjE=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzYwN30=', 'http://yfdocument.qiniudn.com/FmvoBkCjiOerjFyhZ1dc_aucNqvy'),
(51, 'FpGPRIuiYUhicHcR2e8x6TpCgKTO', 'jyxkz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/7cdb757f7154482a8a1727767909b946.jpg', 'e9358e9f38c24d869738555af532d953', 1, 1436584022732, 1436584022793, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:a31vRXIGUz9t5gn2MHHvbMCfKfQ=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzYyMn0=', 'http://yfdocument.qiniudn.com/FpGPRIuiYUhicHcR2e8x6TpCgKTO'),
(52, 'FibHrfbO1dasrkav2aa9QQSbpH0W', 'swdjz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/3de3cf9f9f8444b685e1590a8be4e1e7.jpg', '979cb5980eef41f3a2b0378d569ce000', 1, 1436584022794, 1436584022848, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:a31vRXIGUz9t5gn2MHHvbMCfKfQ=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzYyMn0=', 'http://yfdocument.qiniudn.com/FibHrfbO1dasrkav2aa9QQSbpH0W'),
(53, 'FiKKq92BDSGTrYE88uNtmp8NlxO0', 'zzjgdmz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/c63a39921007407f9723c394ceffdac8.jpg', 'd5041a9e74b645a8b6ba64d9c2033a75', 1, 1436584022849, 1436584022906, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:a31vRXIGUz9t5gn2MHHvbMCfKfQ=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzYyMn0=', 'http://yfdocument.qiniudn.com/FiKKq92BDSGTrYE88uNtmp8NlxO0'),
(54, 'FsKk32pKHKXLkpSv6a_y_2bDCpNZ', 'DSC_0018.JPG', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/4cb48c644b5642d49760213aec1263aa.JPG', '74db4cca188c498f9d5ea100417e2d49', 1, 1436584022909, 1436584027911, 'QINIU', 0, 'JPG', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:a31vRXIGUz9t5gn2MHHvbMCfKfQ=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzYyMn0=', 'http://yfdocument.qiniudn.com/FsKk32pKHKXLkpSv6a_y_2bDCpNZ'),
(55, 'FkqGUWkcpUEsaQ5qVKiCC4X_1cP7', 'yyzz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/855bf6c4b2f04123b094207f74d1830c.jpg', 'b269ac95c2ea45cfbdf9f575e6319348', 1, 1436584027913, 1436584027975, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:TPvMNjRC6lqFKdxWoMNgC42bfuo=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzYyN30=', 'http://yfdocument.qiniudn.com/FkqGUWkcpUEsaQ5qVKiCC4X_1cP7'),
(56, 'FibHrfbO1dasrkav2aa9QQSbpH0W', 'swdjz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/7631b1fd96ae408b812c6f17514333ba.jpg', '108bfbb613284ee1ab22820f7dfab4a6', 1, 1436584070811, 1436584070862, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:lDoqkgyfqWR4bvcG7TYWO0XXqYQ=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzY3MH0=', 'http://yfdocument.qiniudn.com/FibHrfbO1dasrkav2aa9QQSbpH0W'),
(57, 'FiKKq92BDSGTrYE88uNtmp8NlxO0', 'zzjgdmz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/996f883f532840e488c5a7785d71ce62.jpg', '164ac0906a0f4016a5cb4a55b0da73b4', 1, 1436584070863, 1436584070917, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:lDoqkgyfqWR4bvcG7TYWO0XXqYQ=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzY3MH0=', 'http://yfdocument.qiniudn.com/FiKKq92BDSGTrYE88uNtmp8NlxO0'),
(58, 'FpGPRIuiYUhicHcR2e8x6TpCgKTO', 'jyxkz1.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/f6def2aa675246f7bda2bb6cbfa17a6a.jpg', '926c0b556fc547e3912bdd8c99ce54b4', 1, 1436584070919, 1436584070973, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:lDoqkgyfqWR4bvcG7TYWO0XXqYQ=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzY3MH0=', 'http://yfdocument.qiniudn.com/FpGPRIuiYUhicHcR2e8x6TpCgKTO'),
(59, 'FpGPRIuiYUhicHcR2e8x6TpCgKTO', 'jyxkz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/1f1e22d839b143d783a38f5449215750.jpg', 'af45a31b294b4efc89a8c923f577e76b', 1, 1436584070975, 1436584071030, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:lDoqkgyfqWR4bvcG7TYWO0XXqYQ=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzY3MH0=', 'http://yfdocument.qiniudn.com/FpGPRIuiYUhicHcR2e8x6TpCgKTO'),
(60, 'FkqGUWkcpUEsaQ5qVKiCC4X_1cP7', 'yyzz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/18359dbdfcc54628875da71876c08e10.jpg', '7be5f3cb19034297a0131f0f48f3a016', 1, 1436584071032, 1436584071089, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:1QjvNpoL2f281mb0Ce9MtYOeY60=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzY3MX0=', 'http://yfdocument.qiniudn.com/FkqGUWkcpUEsaQ5qVKiCC4X_1cP7'),
(61, 'FmvoBkCjiOerjFyhZ1dc_aucNqvy', 'khxkz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/39d3e5b600c446c98f9538162ebd3f4a.jpg', 'fcc3161c4e5d4f7ea5068d1eb7e80d49', 1, 1436584071090, 1436584071147, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:1QjvNpoL2f281mb0Ce9MtYOeY60=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzY3MX0=', 'http://yfdocument.qiniudn.com/FmvoBkCjiOerjFyhZ1dc_aucNqvy'),
(62, 'FkqGUWkcpUEsaQ5qVKiCC4X_1cP7', 'yyzz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/ceb5b725a8ac4357945663b0d0f0efe0.jpg', '1f0003d0e1dd46fda5be8d4e16ba0ebf', 1, 1436584101032, 1436584101090, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:o-9lTDZorLA0fUGtaEMjjNWptjk=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzcwMX0=', 'http://yfdocument.qiniudn.com/FkqGUWkcpUEsaQ5qVKiCC4X_1cP7'),
(63, 'FmvoBkCjiOerjFyhZ1dc_aucNqvy', 'khxkz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/65e7e679d3da4594a868456cf7fe70c2.jpg', '016d9429b32c44dd83832590b1cea618', 1, 1436584101092, 1436584101168, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:o-9lTDZorLA0fUGtaEMjjNWptjk=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzcwMX0=', 'http://yfdocument.qiniudn.com/FmvoBkCjiOerjFyhZ1dc_aucNqvy'),
(64, 'FpGPRIuiYUhicHcR2e8x6TpCgKTO', 'jyxkz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/ff6f71bc7ab3480180c952c6f048900b.jpg', '8d4876e3c2364e3da8c0748d2569d521', 1, 1436584101171, 1436584101233, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:o-9lTDZorLA0fUGtaEMjjNWptjk=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzcwMX0=', 'http://yfdocument.qiniudn.com/FpGPRIuiYUhicHcR2e8x6TpCgKTO'),
(65, 'FibHrfbO1dasrkav2aa9QQSbpH0W', 'swdjz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/0bb687f9c9e04e2cb0b956feadd22f27.jpg', '8b6b27523ea14afdb6c2067066c2d07b', 1, 1436584101235, 1436584101552, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:o-9lTDZorLA0fUGtaEMjjNWptjk=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzcwMX0=', 'http://yfdocument.qiniudn.com/FibHrfbO1dasrkav2aa9QQSbpH0W'),
(66, 'FmvoBkCjiOerjFyhZ1dc_aucNqvy', 'khxkz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/908c1ee4cd5248cfbadca092a1aca833.jpg', 'eca2b4c83a9641e0be7b75c7d726d2fe', 1, 1436584228584, 1436584233673, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:aW5KQ97Un6nI2TrtriR5yZZJB10=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzgyOH0=', 'http://yfdocument.qiniudn.com/FmvoBkCjiOerjFyhZ1dc_aucNqvy'),
(67, 'FpGPRIuiYUhicHcR2e8x6TpCgKTO', 'jyxkz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/3c3cfd39b26f40278c0c8df0c8d75485.jpg', '6f42ed2140354320b4903899e20e2ae5', 1, 1436584233675, 1436584233734, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:l5IruIUeAIc6ugc1FZWCU_u1yzs=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzgzM30=', 'http://yfdocument.qiniudn.com/FpGPRIuiYUhicHcR2e8x6TpCgKTO'),
(68, 'FibHrfbO1dasrkav2aa9QQSbpH0W', 'swdjz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/268061735732428b8a6c67fb8d5bb334.jpg', '4551d667004e4a919a573cccf5b697dc', 1, 1436584233735, 1436584233794, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:l5IruIUeAIc6ugc1FZWCU_u1yzs=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzgzM30=', 'http://yfdocument.qiniudn.com/FibHrfbO1dasrkav2aa9QQSbpH0W'),
(69, 'FibHrfbO1dasrkav2aa9QQSbpH0W', 'swdjz1.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/0c0556b282964dd7b96ebb78450f649b.jpg', 'ed638758f77f4ebb889e7fdfd19107b1', 1, 1436584233796, 1436584233854, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:l5IruIUeAIc6ugc1FZWCU_u1yzs=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzgzM30=', 'http://yfdocument.qiniudn.com/FibHrfbO1dasrkav2aa9QQSbpH0W'),
(70, 'Ft-Xh_5uE3Tq_4xhlAmaJY2mWPs4', 'IMG_3606.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/0c7c5f7eae414fc58f7485aef51568ed.jpg', 'f9f6cdf8736c405fad4f41889b5331a6', 1, 1436584233857, 1436584239225, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:l5IruIUeAIc6ugc1FZWCU_u1yzs=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzgzM30=', 'http://yfdocument.qiniudn.com/Ft-Xh_5uE3Tq_4xhlAmaJY2mWPs4'),
(71, 'FiKKq92BDSGTrYE88uNtmp8NlxO0', 'zzjgdmz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/d0bf05d95a214cab98873bac9af5ad1f.jpg', '615a1a96d26e4172b2ff9f1953b76214', 1, 1436584239228, 1436584239310, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:ArPIpajo761pwNz0lRIbvWtfN-Y=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzgzOX0=', 'http://yfdocument.qiniudn.com/FiKKq92BDSGTrYE88uNtmp8NlxO0'),
(72, 'FibHrfbO1dasrkav2aa9QQSbpH0W', 'swdjz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/acbbcca2b5db4b0eb0537779cb84a3b6.jpg', 'b1ba89e6258a46b980157d0b9c4df260', 1, 1436584286633, 1436584286708, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:U8lK_6xHunOf3wKqCNE3aiMSEg8=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4Nzg4Nn0=', 'http://yfdocument.qiniudn.com/FibHrfbO1dasrkav2aa9QQSbpH0W'),
(73, 'FibHrfbO1dasrkav2aa9QQSbpH0W', 'swdjz1.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/a3c38eeb8ea94fd89245f876906158b6.jpg', 'b880947c9aae4450bafcc54eb0828ab0', 1, 1436584286709, 1436584286764, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:U8lK_6xHunOf3wKqCNE3aiMSEg8=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4Nzg4Nn0=', 'http://yfdocument.qiniudn.com/FibHrfbO1dasrkav2aa9QQSbpH0W'),
(74, 'FiKKq92BDSGTrYE88uNtmp8NlxO0', 'zzjgdmz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/01b21ab9d943467cb9de097242d65602.jpg', '900f5cb144a240ad860d4156b0b8a8d8', 1, 1436584286765, 1436584286837, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:U8lK_6xHunOf3wKqCNE3aiMSEg8=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4Nzg4Nn0=', 'http://yfdocument.qiniudn.com/FiKKq92BDSGTrYE88uNtmp8NlxO0'),
(75, 'FpGPRIuiYUhicHcR2e8x6TpCgKTO', 'jyxkz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/a4e70efbe26e41b3835f0144fd0e94d7.jpg', '330e5c1c84114ed7bac10442b93086b8', 1, 1436584286838, 1436584286895, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:U8lK_6xHunOf3wKqCNE3aiMSEg8=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4Nzg4Nn0=', 'http://yfdocument.qiniudn.com/FpGPRIuiYUhicHcR2e8x6TpCgKTO'),
(76, 'FmvoBkCjiOerjFyhZ1dc_aucNqvy', 'khxkz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/7f46692dfc8348b48ff0c138fe94da34.jpg', '1e9799a4dda64778af28fb59fd4af581', 1, 1436584286899, 1436584286959, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:U8lK_6xHunOf3wKqCNE3aiMSEg8=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4Nzg4Nn0=', 'http://yfdocument.qiniudn.com/FmvoBkCjiOerjFyhZ1dc_aucNqvy'),
(77, 'FiKKq92BDSGTrYE88uNtmp8NlxO0', 'zzjgdmz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/79cba19459f94d73bb6314b2e78b26da.jpg', 'e21f3e9c66cd46b988dc150e4c1a2ff6', 1, 1436584318361, 1436584318425, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:eLUlKsmoV09Fi8WKCl-fcxiuzOc=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzkxOH0=', 'http://yfdocument.qiniudn.com/FiKKq92BDSGTrYE88uNtmp8NlxO0'),
(78, 'FibHrfbO1dasrkav2aa9QQSbpH0W', 'swdjz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/36506835278d4a3b8c6b00da30e49e02.jpg', 'f67bd8778bb0490f94cc51735a36b52e', 1, 1436584318426, 1436584318487, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:eLUlKsmoV09Fi8WKCl-fcxiuzOc=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzkxOH0=', 'http://yfdocument.qiniudn.com/FibHrfbO1dasrkav2aa9QQSbpH0W'),
(79, 'FmvoBkCjiOerjFyhZ1dc_aucNqvy', 'khxkz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/e2a0b492077c412a97be6ba3ec12e992.jpg', '2517bd1531a846c48f7948cc06c116b3', 1, 1436584318488, 1436584318573, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:eLUlKsmoV09Fi8WKCl-fcxiuzOc=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzkxOH0=', 'http://yfdocument.qiniudn.com/FmvoBkCjiOerjFyhZ1dc_aucNqvy'),
(80, 'FpGPRIuiYUhicHcR2e8x6TpCgKTO', 'jyxkz.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/88cece2cbec1460598250af819fe012d.jpg', 'b027ca8fe77e488aab1194db621f9455', 1, 1436584318574, 1436584318654, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:eLUlKsmoV09Fi8WKCl-fcxiuzOc=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4NzkxOH0=', 'http://yfdocument.qiniudn.com/FpGPRIuiYUhicHcR2e8x6TpCgKTO'),
(81, 'Fijk8iJZ_aOl0NpdrVKbqhI6PhNR', 'IMG_3637.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/16bca6715eba4e32bd4b86224328aefe.jpg', '0f927dce4b56446cabce91662eb4415a', 1, 1436584568397, 1436584594258, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:AwYt8g0LEtE31oKUGM7BvS2tvSY=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4ODE2OH0=', 'http://yfdocument.qiniudn.com/Fijk8iJZ_aOl0NpdrVKbqhI6PhNR'),
(82, 'Fp4nYbfccf9HDP8mp6PMvQnnTuMp', 'IMG_3631.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/7295d0941baf48aabcba29e87f332ae9.jpg', 'd156d790295842c2ad622b28798a0a22', 1, 1436584701175, 1436584722001, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:bzUyMWsdeAqS5QlVVp7Gn4pDmu8=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4ODMwMX0=', 'http://yfdocument.qiniudn.com/Fp4nYbfccf9HDP8mp6PMvQnnTuMp'),
(83, 'FqscUh5S4hcGScGYAnC0uw_EwLLD', 'IMG_3611.jpg', '/home/yfsoft/Workspace/JEE/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/trade/upload/eb2f69501ffa473f950b28802596e705.jpg', '4c0dc7d20d254517b1563bb480bd01cc', 1, 1436584819577, 1436584837745, 'QINIU', 0, 'jpg', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:ovlg6_Lg-2cfsLd4wk6hdm_KNsU=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4ODQxOX0=', 'http://yfdocument.qiniudn.com/FqscUh5S4hcGScGYAnC0uw_EwLLD'),
(86, 'Fs2k3k3mq4E8CzCLLf6Ck1NBo5Pg', 'fast_20150711112506.bak', '/tmp/fast_20150711112506.bak', 'd6cd862c9e2e4689b7255a08afaf69ba', 1, 1436585106207, 1436585141360, 'QINIU', 0, 'bak', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:2ulhgRNIYnF7bO0oY1pD6HfjPq0=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4ODcwNn0=', 'http://yfdocument.qiniudn.com/Fs2k3k3mq4E8CzCLLf6Ck1NBo5Pg'),
(85, 'FrNAMkzmQJDJzKQkD0qAOqhsPbOP', 'bat_20150711112334.bat', '/tmp/bat_20150711112334.bat', 'da99bdbf4caf450faffa427d7bcae4fc', 1, 1436585014586, 1436585034649, 'QINIU', 0, 'bat', 'ROOT', NULL, '65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81:AOpfpOhOzDAHYi0hG4qgjrJxxbY=:eyJzY29wZSI6InlmZG9jdW1lbnQiLCJkZWFkbGluZSI6MTQzNjU4ODYxNH0=', 'http://yfdocument.qiniudn.com/FrNAMkzmQJDJzKQkD0qAOqhsPbOP');

-- --------------------------------------------------------

--
-- Table structure for table `sys_log`
--

CREATE TABLE IF NOT EXISTS `sys_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `action` varchar(100) NOT NULL,
  `remark` varchar(200) DEFAULT NULL,
  `createtime` bigint(20) NOT NULL,
  `ip` varchar(20) NOT NULL,
  `user` int(11) NOT NULL COMMENT 'USERID',
  `updatetime` bigint(20) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `result` int(11) NOT NULL DEFAULT '0',
  `reason` varchar(1000) DEFAULT NULL,
  `args` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sys_permission`
--

CREATE TABLE IF NOT EXISTS `sys_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `title` varchar(200) DEFAULT NULL,
  `status` int(4) NOT NULL DEFAULT '0',
  `createtime` bigint(20) NOT NULL,
  `updatetime` bigint(20) NOT NULL,
  `scope` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `sys_permission`
--

INSERT INTO `sys_permission` (`id`, `name`, `title`, `status`, `createtime`, `updatetime`, `scope`) VALUES
(1, '/admin/user/role/security', '角色授权', 1, 1428976041205, 1428976041205, 'SYS'),
(2, '/admin/user/role/add', '角色新增', 1, 1428976041205, 1428976041205, 'SYS'),
(3, '/admin/user/save', '用户新增编辑', 1, 1428976086371, 1428976086371, 'SYS'),
(4, '/admin/user/role/ajaxList', '用户角色列表', 1, 1428976041205, 1428976041205, 'SYS'),
(5, '/admin/index', '首页', 1, 1428976041205, 1428976041205, 'SYS'),
(6, '/admin/permission', '用户权限', 1, 1428976041205, 1428976041205, 'SYS'),
(7, '/admin/user/role', '用户角色', 1, 1428976067614, 1428976067614, 'SYS'),
(8, '/admin/user', '用户列表', 1, 1428976086371, 1428976086371, 'SYS'),
(9, '/admin', '用户验证', 1, 1428976041205, 1428976041205, 'SYS'),
(10, '/admin/user/ajaxList', '用户列表查询', 1, 1428976041205, 1428976041205, 'SYS'),
(11, '/admin/judge/page', '我的代办', 1, 1428976041205, 1428976041205, 'SYS'),
(12, '/admin/judge/list', '代办任务列表', 1, 1428976041205, 1428976041205, 'SYS'),
(13, '/admin/process/list', '工作流列表', 1, 1428976041205, 1428976041205, 'SYS'),
(14, '/admin/product/page', '我的商品', 1, 1428976041205, 1428976041205, 'SYS'),
(15, '/admin/product/task', '新增商品', 1, 1428976041205, 1428976041205, 'SYS'),
(16, '/admin/product/list', '查看商品列表', 1, 1428976041205, 1428976041205, 'SYS'),
(17, '/admin/judge/task', '审核', 1, 1428976041205, 1428976041205, 'SYS'),
(18, '/admin/judge/form', '代办表单', 1, 1428976041205, 1428976041205, 'SYS'),
(19, '/admin/product/form', '商品新增表单', 1, 0, 0, 'SYS'),
(20, '/admin/judge/agency', '代办的页面信息', 1, 0, 0, 'SYS'),
(21, '/admin/login', '登陆', 1, 1433905444637, 1433905544829, 'USER'),
(22, '/admin/auth', '授权登录', 1, 1433906116468, 1433906116468, 'USER'),
(23, '/admin/document', '文档管理', 1, 1433908155266, 1433908155266, 'USER'),
(24, '/admin/logout', '注销操作', 1, 1433908199886, 1433908199886, 'USER'),
(25, '/admin/user/changePassword', '修改密码', 1, 1433916098414, 1433916098414, 'USER');

-- --------------------------------------------------------

--
-- Table structure for table `sys_plugin`
--

CREATE TABLE IF NOT EXISTS `sys_plugin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `title` varchar(200) NOT NULL,
  `class` varchar(200) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `createtime` bigint(20) NOT NULL,
  `updatetime` bigint(20) NOT NULL,
  `args` varchar(1000) DEFAULT NULL,
  `autoload` int(4) NOT NULL DEFAULT '1',
  `filepath` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `sys_plugin`
--

INSERT INTO `sys_plugin` (`id`, `name`, `title`, `class`, `status`, `createtime`, `updatetime`, `args`, `autoload`, `filepath`) VALUES
(1, 'MongodbPlugin', '操作MongodbPlugin数据库的插件', 'biz.yfsoft.app.fastframework.plugin.mongodb.MongodbPlugin', 1, 1434014660008, 1434705603224, '{"host":"192.168.1.5","port":"27017","name":"app","bin":"/usr/bin/"}', 1, ''),
(2, 'QiniuPlugin', '七牛云存储的插件', 'biz.yfsoft.app.fastframework.plugin.qiniu.QiniuPlugin', 1, 1434014780668, 1434592754298, '{"DOMAIN":"http://yfdocument.qiniudn.com","BUCKET_NAME":"yfdocument","ACCESS_KEY":"65nep44MNB8Ft1v_L1f7jaSnP8P07buuMMN4kI81","SECRET_KEY":"kZxy-i93_B98yg4lNn7XmSujeZh_JWRxQOJX3E_m"}', 1, ''),
(3, 'IcbcPayPlugin', '工商银行支付插件', 'biz.yfsoft.app.fastframework.plugin.icbc.IcbcPayPlugin', 1, 1434015540789, 1435062316815, '{"password":"123456","notifyUrl":"http://fast.yfsoft.info/fastframework/admin/notify/payNotify","cert":"classpath:hy"}', 1, ''),
(4, 'Quartz', '任务调度插件', 'biz.yfsoft.app.fastframework.plugin.quartz.DefaultQuartzPlugin', 1, 1434016822861, 1434019079139, '', 0, '/quartz/jobs.properties'),
(5, 'Email', '邮箱插件', 'biz.yfsoft.app.fastframework.plugin.mail.MailerPlugin', 1, 1434016932307, 1434018540035, '{"template":"<a hrefr=\\"http://www.baidu.com\\">baidu.com111111</a>","password":"YFsoft123","host":"smtp.126.com","name":"江阴a","user":"yfsoftcom@126.com"}', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `sys_role`
--

CREATE TABLE IF NOT EXISTS `sys_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(200) NOT NULL COMMENT '角色名称',
  `title` varchar(2000) DEFAULT NULL COMMENT '角色描述',
  `status` int(4) NOT NULL DEFAULT '0',
  `createtime` bigint(20) NOT NULL DEFAULT '0',
  `updatetime` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `sys_role`
--

INSERT INTO `sys_role` (`id`, `name`, `title`, `status`, `createtime`, `updatetime`) VALUES
(1, 'localadmin', '本地管理员', 1, 1429098494353, 1429098494353),
(2, 'admin', '普通管理员', 1, 1429098494353, 1429098494353),
(3, 'seller', '卖家', 1, 1429098494353, 1429098494353),
(5, 'operator', '操作员', 1, 1429098494353, 1429098494353),
(8, 'fund', '资金方', 1, 1429098494353, 1429098494353),
(9, 'buyer', '买家', 1, 1429098516856, 1429098516856),
(10, 'judge', '平台审核员', 1, 1429098584142, 1429098584142),
(11, 'check', '质检方', 1, 1434005571675, 1434005571675),
(12, 'logistics', '物流方', 1, 1434005611229, 1434005611229);

-- --------------------------------------------------------

--
-- Table structure for table `sys_role_permission`
--

CREATE TABLE IF NOT EXISTS `sys_role_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roleid` int(11) NOT NULL,
  `permissionid` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `sys_role_permission`
--

INSERT INTO `sys_role_permission` (`id`, `roleid`, `permissionid`) VALUES
(1, 1, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20'),
(3, 9, '21,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20'),
(4, 8, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20'),
(5, 10, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20'),
(6, 2, '21,22,23,24,25,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20');

-- --------------------------------------------------------

--
-- Table structure for table `sys_session`
--

CREATE TABLE IF NOT EXISTS `sys_session` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sessionid` varchar(50) NOT NULL,
  `uid` int(11) NOT NULL,
  `logintime` bigint(20) NOT NULL DEFAULT '0',
  `ip` varchar(20) NOT NULL,
  `errorcount` int(11) NOT NULL DEFAULT '0' COMMENT 'login pass error counter',
  `logouttime` bigint(20) NOT NULL DEFAULT '0',
  `deleteflag` int(11) NOT NULL DEFAULT '0',
  `createtime` bigint(20) NOT NULL DEFAULT '0',
  `updatetime` bigint(20) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `token` varchar(50) DEFAULT NULL,
  `refreshtime` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sys_setting`
--

CREATE TABLE IF NOT EXISTS `sys_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '配置键',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '配置标题',
  `category` varchar(255) NOT NULL DEFAULT '' COMMENT '配置类型',
  `val` varchar(2000) NOT NULL COMMENT '配置值',
  `createtime` bigint(50) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updatetime` bigint(50) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `sys_setting`
--

INSERT INTO `sys_setting` (`id`, `name`, `title`, `category`, `val`, `createtime`, `updatetime`, `status`) VALUES
(1, 'version', '版本号12', 'EXTENDS', '1.0.1.1\n2订单3213', 0, 1425610638878, 1),
(20, 'POST_TAGS', '文章标签列表', 'SYS', '{"greenhand":"新手上路","abouttrade":"关于买卖","news":"行情资讯","extraservice":"增值服务","aboutus":"关于我们"}', 1435238873312, 1435238873312, 1),
(21, 'SYS_DOMAIN', '系统域名', 'SYS', 'http://fast.yfsoft.info/trade/', 1435323560992, 1436268650726, 1),
(22, 'EMAIL_CHECK', '系统域名', 'SYS', 'xx@163.com', 1435683956559, 1435683956559, 1),
(23, 'RESET_EMAIL_URL', '重置密码使用的url地址', 'SYS', 'http://localhost:8001/admin/notify/resetPass/', 1436843075144, 1436843075144, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sys_user`
--

CREATE TABLE IF NOT EXISTS `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `login_name` varchar(50) NOT NULL COMMENT '登录名',
  `login_pass` varchar(50) DEFAULT NULL COMMENT '登录密码',
  `pass_encode` varchar(20) NOT NULL DEFAULT 'MD5' COMMENT '密码的加密方式',
  `email` varchar(100) NOT NULL COMMENT '邮箱',
  `login_ip` varchar(20) DEFAULT NULL COMMENT '上次登录IP',
  `login_time` bigint(50) DEFAULT NULL COMMENT '上次登录时间',
  `active_time` bigint(50) DEFAULT NULL COMMENT '用户激活时间',
  `is_admin` int(1) NOT NULL DEFAULT '0' COMMENT '是否管理员 0：非，1：是',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '用户状态 0：未初始化，1：正常，<0异常',
  `createtime` bigint(50) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updatetime` bigint(50) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `display_name` varchar(20) DEFAULT NULL COMMENT '昵称',
  `emai_validate` int(1) DEFAULT '0' COMMENT '邮箱验证',
  `reset_pass_token` varchar(100) DEFAULT NULL COMMENT '重置密码的token',
  `reset_pass_valid_time` bigint(50) DEFAULT NULL COMMENT '密码重置的有效期',
  `mobile_phone` varchar(50) DEFAULT NULL COMMENT '账户的手机号码',
  `phone_validate` int(1) DEFAULT '0' COMMENT '验证手机号的标示',
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDXU_sys_user_email` (`email`),
  UNIQUE KEY `IDXU_sys_user_login_name` (`login_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='系统用户 系统用户' AUTO_INCREMENT=1176 ;

--
-- Dumping data for table `sys_user`
--

INSERT INTO `sys_user` (`id`, `login_name`, `login_pass`, `pass_encode`, `email`, `login_ip`, `login_time`, `active_time`, `is_admin`, `status`, `createtime`, `updatetime`, `display_name`, `emai_validate`, `reset_pass_token`, `reset_pass_valid_time`, `mobile_phone`, `phone_validate`) VALUES
(1000, 'root', '4297f44b13955235245b2497399d7a93', 'MD5', 'yfsoftcom@126.com', '127.0.0.1', 1436843008604, NULL, 1, 1, 0, 1436843995341, '管理员', 0, NULL, 0, NULL, 0),
(1175, 'nmgmtmj@qq.com', '9e0b249dd4f9d0f7b773e0786b2abea2', 'MD5', 'nmgmtmj@qq.com', NULL, NULL, 1436584239313, 0, 1, 1436584143470, 1436584143470, NULL, 0, NULL, NULL, NULL, 0),
(1174, 'sxmtmj@qq.com', '05b6989d22db9a85607c8459cc15d59b', 'MD5', 'sxmtmj@qq.com', NULL, NULL, 1436584027978, 0, 1, 1436583922774, 1436583922774, NULL, 0, NULL, NULL, NULL, 0),
(1173, 'jymtmj@maijia.com', 'd610d33bc855d73cf11a8c548dc1feb6', 'MD5', 'jymtmj@maijia.com', NULL, NULL, 1436583808642, 0, 1, 1436583692186, 1436583692186, NULL, 0, NULL, NULL, NULL, 0),
(1172, 'yzliangjun@126.com', '973b2e6e57e6a999a70811c337e5f8b8', 'MD5', 'yzliangjun@126.com', NULL, NULL, 1436583577375, 0, 1, 1436583190137, 1436583190137, NULL, 0, NULL, NULL, NULL, 0),
(1171, 'zhy@qq.com', 'acb11fd6cd24deb54973389aa1ba434f', 'MD5', 'zhy@qq.com', NULL, NULL, 1436583072232, 0, 1, 1436582517776, 1436582517776, NULL, 0, NULL, NULL, NULL, 0),
(1170, 'sgs@qq.com', '5fee7097ed292c1dbc654e1384476147', 'MD5', 'sgs@qq.com', NULL, NULL, 1436582887282, 0, 1, 1436582501451, 1436582501451, NULL, 0, NULL, NULL, NULL, 0),
(1169, 'sumeida@qq.com', '15c784c5042485d51abc3c36c420416f', 'MD5', 'sumeida@qq.com', NULL, NULL, 1436583092642, 0, 1, 1436582485530, 1436582485530, NULL, 0, NULL, NULL, NULL, 0),
(1168, 'caozuoyuan@qq.com', '88359a6aee27f3bb09ffd1d21e07c4dd', 'MD5', 'caozuoyuan@qq.com', NULL, NULL, NULL, 0, 0, 1436582415939, 1436582415939, NULL, 0, NULL, NULL, NULL, 0),
(1167, 'guanliyuan@qq.com', '665b2fed1599f29554a9bd6a19c9947f', 'MD5', 'guanliyuan@qq.com', NULL, NULL, NULL, 0, 1, 1436582358194, 1436582358194, 'guanliyuan@qq.com', 0, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sys_user_role`
--

CREATE TABLE IF NOT EXISTS `sys_user_role` (
  `roleid` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sys_user_role`
--

INSERT INTO `sys_user_role` (`roleid`, `userid`) VALUES
(1, 1000),
(1, 1113),
(3, 1175),
(9, 1175),
(3, 1174),
(9, 1174),
(3, 1173),
(9, 1173),
(9, 1172),
(12, 1171),
(11, 1170),
(8, 1169),
(5, 1168),
(2, 1167);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_fast_work`
--
CREATE TABLE IF NOT EXISTS `view_fast_work` (
`taskId` varchar(32)
,`taskName` varchar(100)
,`variable` varchar(2000)
,`displayName` varchar(200)
,`createTime` varchar(50)
,`creator` varchar(50)
,`processId` varchar(32)
,`orderId` varchar(32)
,`actorId` varchar(50)
,`actionUrl` varchar(200)
,`parentTaskId` varchar(32)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `view_hist_fast_work`
--
CREATE TABLE IF NOT EXISTS `view_hist_fast_work` (
`taskId` varchar(32)
,`taskName` varchar(100)
,`displayName` varchar(200)
,`variable` varchar(2000)
,`createTime` varchar(50)
,`creator` varchar(50)
,`processId` varchar(32)
,`orderId` varchar(32)
,`actorId` varchar(50)
,`actionUrl` varchar(200)
,`parentTaskId` varchar(32)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `view_wf_process`
--
CREATE TABLE IF NOT EXISTS `view_wf_process` (
`id` varchar(32)
,`name` varchar(100)
,`displayName` varchar(200)
,`type` varchar(100)
,`instanceUrl` varchar(200)
,`state` tinyint(1)
,`content` longblob
,`createTime` varchar(50)
,`creator` varchar(50)
);
-- --------------------------------------------------------

--
-- Table structure for table `wf_hist_order`
--

CREATE TABLE IF NOT EXISTS `wf_hist_order` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `process_Id` varchar(32) NOT NULL COMMENT '流程定义ID',
  `order_State` tinyint(1) NOT NULL COMMENT '状态',
  `creator` varchar(50) DEFAULT NULL COMMENT '发起人',
  `create_Time` varchar(50) NOT NULL COMMENT '发起时间',
  `end_Time` varchar(50) DEFAULT NULL COMMENT '完成时间',
  `expire_Time` varchar(50) DEFAULT NULL COMMENT '期望完成时间',
  `priority` tinyint(1) DEFAULT NULL COMMENT '优先级',
  `parent_Id` varchar(32) DEFAULT NULL COMMENT '父流程ID',
  `order_No` varchar(50) DEFAULT NULL COMMENT '流程实例编号',
  `variable` varchar(2000) DEFAULT NULL COMMENT '附属变量json存储',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='历史流程实例表';

-- --------------------------------------------------------

--
-- Table structure for table `wf_hist_task`
--

CREATE TABLE IF NOT EXISTS `wf_hist_task` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `order_Id` varchar(32) NOT NULL COMMENT '流程实例ID',
  `task_Name` varchar(100) NOT NULL COMMENT '任务名称',
  `display_Name` varchar(200) NOT NULL COMMENT '任务显示名称',
  `task_Type` tinyint(1) NOT NULL COMMENT '任务类型',
  `perform_Type` tinyint(1) DEFAULT NULL COMMENT '参与类型',
  `task_State` tinyint(1) NOT NULL COMMENT '任务状态',
  `operator` varchar(50) DEFAULT NULL COMMENT '任务处理人',
  `create_Time` varchar(50) NOT NULL COMMENT '任务创建时间',
  `finish_Time` varchar(50) DEFAULT NULL COMMENT '任务完成时间',
  `expire_Time` varchar(50) DEFAULT NULL COMMENT '任务期望完成时间',
  `action_Url` varchar(200) DEFAULT NULL COMMENT '任务处理url',
  `parent_Task_Id` varchar(32) DEFAULT NULL COMMENT '父任务ID',
  `variable` varchar(2000) DEFAULT NULL COMMENT '附属变量json存储',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='历史任务表';

-- --------------------------------------------------------

--
-- Table structure for table `wf_hist_task_actor`
--

CREATE TABLE IF NOT EXISTS `wf_hist_task_actor` (
  `task_Id` varchar(32) NOT NULL COMMENT '任务ID',
  `actor_Id` varchar(50) NOT NULL COMMENT '参与者ID'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='历史任务参与者表';

-- --------------------------------------------------------

--
-- Table structure for table `wf_order`
--

CREATE TABLE IF NOT EXISTS `wf_order` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `parent_Id` varchar(32) DEFAULT NULL COMMENT '父流程ID',
  `process_Id` varchar(32) NOT NULL COMMENT '流程定义ID',
  `creator` varchar(50) DEFAULT NULL COMMENT '发起人',
  `create_Time` varchar(50) NOT NULL COMMENT '发起时间',
  `expire_Time` varchar(50) DEFAULT NULL COMMENT '期望完成时间',
  `last_Update_Time` varchar(50) DEFAULT NULL COMMENT '上次更新时间',
  `last_Updator` varchar(50) DEFAULT NULL COMMENT '上次更新人',
  `priority` tinyint(1) DEFAULT NULL COMMENT '优先级',
  `parent_Node_Name` varchar(100) DEFAULT NULL COMMENT '父流程依赖的节点名称',
  `order_No` varchar(50) DEFAULT NULL COMMENT '流程实例编号',
  `variable` varchar(2000) DEFAULT NULL COMMENT '附属变量json存储',
  `version` int(3) DEFAULT NULL COMMENT '版本',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='流程实例表';

-- --------------------------------------------------------

--
-- Table structure for table `wf_process`
--

CREATE TABLE IF NOT EXISTS `wf_process` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `name` varchar(100) DEFAULT NULL COMMENT '流程名称',
  `display_Name` varchar(200) DEFAULT NULL COMMENT '流程显示名称',
  `type` varchar(100) DEFAULT NULL COMMENT '流程类型',
  `instance_Url` varchar(200) DEFAULT NULL COMMENT '实例url',
  `state` tinyint(1) DEFAULT NULL COMMENT '流程是否可用',
  `content` longblob COMMENT '流程模型定义',
  `version` int(2) DEFAULT NULL COMMENT '版本',
  `create_Time` varchar(50) DEFAULT NULL COMMENT '创建时间',
  `creator` varchar(50) DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='流程定义表';

-- --------------------------------------------------------

--
-- Table structure for table `wf_task`
--

CREATE TABLE IF NOT EXISTS `wf_task` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `order_Id` varchar(32) NOT NULL COMMENT '流程实例ID',
  `task_Name` varchar(100) NOT NULL COMMENT '任务名称',
  `display_Name` varchar(200) NOT NULL COMMENT '任务显示名称',
  `task_Type` tinyint(1) NOT NULL COMMENT '任务类型',
  `perform_Type` tinyint(1) DEFAULT NULL COMMENT '参与类型',
  `operator` varchar(50) DEFAULT NULL COMMENT '任务处理人',
  `create_Time` varchar(50) DEFAULT NULL COMMENT '任务创建时间',
  `finish_Time` varchar(50) DEFAULT NULL COMMENT '任务完成时间',
  `expire_Time` varchar(50) DEFAULT NULL COMMENT '任务期望完成时间',
  `action_Url` varchar(200) DEFAULT NULL COMMENT '任务处理的url',
  `parent_Task_Id` varchar(32) DEFAULT NULL COMMENT '父任务ID',
  `variable` varchar(2000) DEFAULT NULL COMMENT '附属变量json存储',
  `version` tinyint(1) DEFAULT NULL COMMENT '版本',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='任务表';

-- --------------------------------------------------------

--
-- Table structure for table `wf_task_actor`
--

CREATE TABLE IF NOT EXISTS `wf_task_actor` (
  `task_Id` varchar(32) NOT NULL COMMENT '任务ID',
  `actor_Id` varchar(50) NOT NULL COMMENT '参与者ID'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='任务参与者表';

-- --------------------------------------------------------

--
-- Structure for view `view_fast_work`
--
DROP TABLE IF EXISTS `view_fast_work`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_fast_work` AS select `wf_task`.`id` AS `taskId`,`wf_task`.`task_Name` AS `taskName`,`wf_task`.`variable` AS `variable`,`wf_task`.`display_Name` AS `displayName`,`wf_task`.`create_Time` AS `createTime`,`wf_order`.`creator` AS `creator`,`wf_process`.`id` AS `processId`,`wf_order`.`id` AS `orderId`,`wf_task_actor`.`actor_Id` AS `actorId`,`wf_task`.`action_Url` AS `actionUrl`,`wf_task`.`parent_Task_Id` AS `parentTaskId` from (((`wf_task` join `wf_task_actor`) join `wf_order`) join `wf_process`) where ((`wf_process`.`id` = `wf_order`.`process_Id`) and (`wf_order`.`id` = `wf_task`.`order_Id`) and (`wf_task_actor`.`task_Id` = `wf_task`.`id`));

-- --------------------------------------------------------

--
-- Structure for view `view_hist_fast_work`
--
DROP TABLE IF EXISTS `view_hist_fast_work`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_hist_fast_work` AS select `wf_hist_task`.`id` AS `taskId`,`wf_hist_task`.`task_Name` AS `taskName`,`wf_hist_task`.`display_Name` AS `displayName`,`wf_hist_task`.`variable` AS `variable`,`wf_hist_task`.`create_Time` AS `createTime`,`wf_hist_order`.`creator` AS `creator`,`wf_process`.`id` AS `processId`,`wf_hist_order`.`id` AS `orderId`,`wf_hist_task_actor`.`actor_Id` AS `actorId`,`wf_hist_task`.`action_Url` AS `actionUrl`,`wf_hist_task`.`parent_Task_Id` AS `parentTaskId` from (((`wf_hist_task` join `wf_hist_task_actor`) join `wf_hist_order`) join `wf_process`) where ((`wf_process`.`id` = `wf_hist_order`.`process_Id`) and (`wf_hist_order`.`id` = `wf_hist_task`.`order_Id`) and (`wf_hist_task_actor`.`task_Id` = `wf_hist_task`.`id`));

-- --------------------------------------------------------

--
-- Structure for view `view_wf_process`
--
DROP TABLE IF EXISTS `view_wf_process`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_wf_process` AS select `wf_process`.`id` AS `id`,`wf_process`.`name` AS `name`,`wf_process`.`display_Name` AS `displayName`,`wf_process`.`type` AS `type`,`wf_process`.`instance_Url` AS `instanceUrl`,`wf_process`.`state` AS `state`,`wf_process`.`content` AS `content`,`wf_process`.`create_Time` AS `createTime`,`wf_process`.`creator` AS `creator` from `wf_process`;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
