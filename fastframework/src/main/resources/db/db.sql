-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2015 年 03 月 05 日 14:33
-- 服务器版本: 5.6.12-log
-- PHP 版本: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `fast`
--
CREATE DATABASE IF NOT EXISTS `fast` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `fast`;

-- --------------------------------------------------------

--
-- 表的结构 `sys_setting`
--

CREATE TABLE IF NOT EXISTS `sys_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '配置键',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '配置标题',
  `category` varchar(255) NOT NULL DEFAULT '' COMMENT '配置类型',
  `val` varchar(2000) NOT NULL COMMENT '配置值',
  `createtime` bigint(50) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updatetime` bigint(50) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- 转存表中的数据 `sys_setting`
--

INSERT INTO `sys_setting` (`id`, `name`, `title`, `category`, `val`, `createtime`, `updatetime`, `status`) VALUES
(1, 'version', '版本号12', 'EXTENDS', '1.0.1.1\n2', 0, 0, 1),
(2, 'TEST', '1', 'EXTENDS', '111', 1425540937922, 1425540937922, -1),
(3, 'TEST2', '1', 'OTHER', 'fdsa', 1425541265913, 1425541265913, -1),
(6, '2', '新的版本号', 'EXTENDS', '怎么弄？', 1425544819083, 1425544819083, -1),
(7, '2222', '2222', 'SYS', '2222', 1425565589582, 1425565589582, 1);

-- --------------------------------------------------------

--
-- 表的结构 `sys_user`
--

CREATE TABLE IF NOT EXISTS `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `login_name` varchar(50) NOT NULL COMMENT '登录名',
  `login_pass` varchar(50) DEFAULT NULL COMMENT '登录密码',
  `pass_encode` varchar(20) NOT NULL DEFAULT 'MD5' COMMENT '密码的加密方式',
  `email` varchar(100) NOT NULL COMMENT '邮箱',
  `login_ip` varchar(20) DEFAULT NULL COMMENT '上次登录IP',
  `login_time` bigint(50) DEFAULT NULL COMMENT '上次登录时间',
  `active_time` bigint(50) DEFAULT NULL COMMENT '用户激活时间',
  `is_admin` int(1) NOT NULL DEFAULT '0' COMMENT '是否管理员 0：非，1：是',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '用户状态 0：未初始化，1：正常，<0异常',
  `createtime` bigint(50) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updatetime` bigint(50) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `display_name` varchar(20) DEFAULT NULL COMMENT '昵称',
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDXU_sys_user_email` (`email`),
  UNIQUE KEY `IDXU_sys_user_login_name` (`login_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='系统用户 系统用户' AUTO_INCREMENT=1001 ;

--
-- 转存表中的数据 `sys_user`
--

INSERT INTO `sys_user` (`id`, `login_name`, `login_pass`, `pass_encode`, `email`, `login_ip`, `login_time`, `active_time`, `is_admin`, `status`, `createtime`, `updatetime`, `display_name`) VALUES
(1000, 'root', '63a9f0ea7bb98050796b649e85481845', 'MD5', 'yfsoftcom@126.com', '127.0.0.1', NULL, NULL, 1, 1, 0, 0, '管理员');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
